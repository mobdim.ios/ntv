//
//  NTVRecordViewController.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 28/06/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Cocoa
import AVFoundation

class NTVRecordViewController2: NTVViewController, AVCaptureFileOutputDelegate, AVCaptureFileOutputRecordingDelegate {

    @IBOutlet weak var previewView: NSView!
    
    var videoDevices = [AVCaptureDevice]()
    var audioDevices = [AVCaptureDevice]()
    
    var videoDeviceInput: AVCaptureDeviceInput?
    var audioDeviceInput: AVCaptureDeviceInput?

    var movieFileOutput = AVCaptureMovieFileOutput()
    var audioPreviewOutput: AVCaptureAudioPreviewOutput? = nil
    
    // - selectedVideoDevice
    var selectedVideoDevice: AVCaptureDevice? {
        set {
            self.session.beginConfiguration()
            
            if let _ = self.videoDeviceInput {
                self.session.removeInput(self.videoDeviceInput!)
                self.videoDeviceInput = nil
            }
            
            if let _ = self.selectedVideoDevice {
                
                var newVideoDeviceInput: AVCaptureDeviceInput? = nil
                
                do {
                    newVideoDeviceInput = try AVCaptureDeviceInput(device: self.selectedVideoDevice!)
                    
                    if self.selectedVideoDevice?.supportsAVCaptureSessionPreset(self.session.sessionPreset) == false {
                        self.session.sessionPreset = AVCaptureSessionPresetHigh
                    }
                    
                    self.session.addInput(newVideoDeviceInput!)
                    self.videoDeviceInput = newVideoDeviceInput!
                }
                catch {
                    DispatchQueue.main.async(execute: {
                        print("Failed to connect device \(self.selectedVideoDevice!.description)")
                    })
                    return
                }
            }
            
            if self.selectedVideoDeviceProvidesAudio {
                self.selectedAudioDevice = nil
            }
            
            self.session.commitConfiguration()
        }
        
        get {
            return self.videoDeviceInput?.device
        }
    }
    
    // - selectedAudioDevice
    var selectedAudioDevice: AVCaptureDevice? {
        set {
            self.session.beginConfiguration()
            
            if let _ = self.audioDeviceInput {
                self.session.removeInput(self.audioDeviceInput!)
                self.audioDeviceInput = nil
            }
            
            if let _ = self.selectedAudioDevice , self.selectedVideoDeviceProvidesAudio == false {
                var newAudioDeviceInput: AVCaptureDeviceInput? = nil
                
                do {
                    newAudioDeviceInput = try AVCaptureDeviceInput(device: self.selectedAudioDevice)
                    
                    if ((self.selectedAudioDevice?.supportsAVCaptureSessionPreset(self.session.sessionPreset)) != nil) {
                        self.session.sessionPreset = AVCaptureSessionPresetHigh
                    }
                    
                    self.session.addInput(newAudioDeviceInput)
                    self.audioDeviceInput = newAudioDeviceInput
                }
                catch {
                    DispatchQueue.main.async(execute: {
                        print("Failed to connect device \(self.selectedAudioDevice!.description)")
                    })
                }
            }
            
            self.session.commitConfiguration()
        }
        
        get {
            return self.audioDeviceInput?.device
        }
    }
    
    var selectedVideoDeviceProvidesAudio: Bool {
        get {
            guard let vdevice = self.selectedVideoDevice else {
                return false
            }
            return (vdevice.hasMediaType(AVMediaTypeMuxed) || vdevice.hasMediaType(AVMediaTypeAudio))
        }
    }
    
    var videoDeviceFormat: AVCaptureDeviceFormat? {
        get {
            return self.selectedVideoDevice?.activeFormat
        }
        set {
            if let videoDevice = self.selectedVideoDevice {
                do {
                    try videoDevice.lockForConfiguration()
                    videoDevice.activeFormat = newValue
                    videoDevice.unlockForConfiguration()
                }
                catch {
                    DispatchQueue.main.async(execute: {
                        //[self presentError:error];
                    })
                }
            }
        }
    }
    var audioDeviceFormat: AVCaptureDeviceFormat? {
        get {
            return self.selectedAudioDevice?.activeFormat
        }
        set {
            
            if let audioDevice = self.selectedAudioDevice {
                do {
                    try audioDevice.lockForConfiguration()
                    audioDevice.activeFormat = newValue
                    audioDevice.unlockForConfiguration()
                }
                catch {
                    DispatchQueue.main.async(execute: {
                        // [self presentError:error];
                    })
                }
            }
        }
    }
    var frameRateRange: AVFrameRateRange? {
        get {
            guard let selected_vdevice = self.selectedVideoDevice else {
                return nil
            }
            
            var activeFrameRateRange: AVFrameRateRange? = nil
            for rateRange in selected_vdevice.activeFormat.videoSupportedFrameRateRanges {
                if CMTimeCompare((rateRange as AnyObject).minFrameDuration, selected_vdevice.activeVideoMinFrameDuration) == 0 {
                    activeFrameRateRange = rateRange as? AVFrameRateRange
                    break
                }
            }
            
            return activeFrameRateRange
        }
        set {
            guard let selected_vdevice = self.selectedVideoDevice else {
                return
            }
            
            guard let newFrameRate = newValue  else {
                return
            }
            
            if let frameRateRanges = selected_vdevice.activeFormat.videoSupportedFrameRateRanges as? [AVFrameRateRange] {
                if frameRateRanges.contains(newFrameRate) {
                    do {
                        try selected_vdevice.lockForConfiguration()
                        selected_vdevice.activeVideoMinFrameDuration = newFrameRate.minFrameDuration
                        selected_vdevice.unlockForConfiguration()
                    }
                    catch {
                        DispatchQueue.main.async(execute: {
                            //[self presentError:error];
                        })
                    }
                }
            }
        }
    }
    
    var observers = [NSObjectProtocol]()
    
    var session = AVCaptureSession()
    
    lazy var availableSessionPresets: [String] = {
        let allSessionPresets = [AVCaptureSessionPresetLow,
                                 AVCaptureSessionPresetMedium,
                                 AVCaptureSessionPresetHigh,
                                 AVCaptureSessionPreset320x240,
                                 AVCaptureSessionPreset352x288,
                                 AVCaptureSessionPreset640x480,
                                 AVCaptureSessionPreset960x540,
                                 AVCaptureSessionPreset1280x720,
                                 AVCaptureSessionPresetPhoto
                                 ]
        
        var availableSessionPresets = [String]()
        
        for sessionPresset in allSessionPresets {
            if self.session.canSetSessionPreset(sessionPresset) {
                availableSessionPresets.append(sessionPresset)
            }
        }
        
        return availableSessionPresets
    }()
    
    var previewLayer: AVCaptureVideoPreviewLayer?
    
    var audioLevelTimer: Timer?

    var recording: Bool {
        get {
            return movieFileOutput.isRecording
        }
        
        set {
            if newValue {
                
            } else {
                self.movieFileOutput.stopRecording()
            }
        }
    }
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let notificationCenter = NotificationCenter.default
        let runtimeErrorObserver = notificationCenter.addObserver(forName: NSNotification.Name.AVCaptureSessionRuntimeError, object: session, queue: OperationQueue.main) { (note) in
            DispatchQueue.main.async(execute: {
                //[self presentError:[[note userInfo] objectForKey:AVCaptureSessionErrorKey]];
            })
        }
        let didStartRunningObserver = notificationCenter.addObserver(forName: NSNotification.Name.AVCaptureSessionDidStartRunning, object: session, queue: OperationQueue.main) { (note) in
            DispatchQueue.main.async(execute: {
                print("did start running")
            })
        }
        let didStopRunningObserver = notificationCenter.addObserver(forName: NSNotification.Name.AVCaptureSessionDidStopRunning, object: session, queue: OperationQueue.main) { (note) in
            DispatchQueue.main.async(execute: {
                print("did stop running")
            })
        }
        let deviceWasConnectedObserver = notificationCenter.addObserver(forName: NSNotification.Name.AVCaptureDeviceWasConnected, object: nil, queue: OperationQueue.main) { (note) in
            DispatchQueue.main.async(execute: {
                self.refreshDevices()
            })
        }
        let deviceWasDisconnectedObserver = notificationCenter.addObserver(forName: NSNotification.Name.AVCaptureDeviceWasDisconnected, object: nil, queue: OperationQueue.main) { (note) in
            DispatchQueue.main.async(execute: {
                self.refreshDevices()
            })
        }
        
        observers = [runtimeErrorObserver, didStartRunningObserver, didStopRunningObserver, deviceWasConnectedObserver, deviceWasDisconnectedObserver]
        
        // Attach outputs to session
        movieFileOutput.delegate = self
        session.addOutput(movieFileOutput)
        
        audioPreviewOutput = AVCaptureAudioPreviewOutput()
        audioPreviewOutput!.volume = 0
        session.addOutput(audioPreviewOutput)
        
        // Select devices if any exist
        let videoDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
//        if (videoDevice) {
        self.selectedVideoDevice = videoDevice
        self.selectedAudioDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeAudio)
//        } else {
//            [self setSelectedVideoDevice:[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeMuxed]];
//        }
        
        // Initial refresh of device list
        refreshDevices()
        
        
        previewView.wantsLayer = true
        
        // Attach preview to session
        let previewViewLayer = previewView.layer
        previewViewLayer?.backgroundColor = CGColor.black
        let newPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
        newPreviewLayer?.frame = (previewViewLayer?.bounds)!
        newPreviewLayer?.autoresizingMask = [.layerWidthSizable, .layerHeightSizable]
        previewViewLayer?.addSublayer(newPreviewLayer!)
        previewLayer = newPreviewLayer
        
        // Start the session
        session.startRunning()
        
        // Start updating the audio level meter
        audioLevelTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateAudioLevels), userInfo: nil, repeats: true)
        
//        @weakify(self);
//        [RACObserve(self, movieFileOutput.recording) subscribeNext:^(NSNumber *value) {
//        @strongify(self);
//        self.buttonClose.enabled = !value.boolValue;
//        }];
//        self.buttonClose.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
//        @strongify(self);
//        [self dismissController:nil];
//        return [RACSignal empty];
//        }];
    }
    
    // MARK: - Device selection
    fileprivate func refreshDevices() {
        
        let vdevices = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) + AVCaptureDevice.devices(withMediaType: AVMediaTypeMuxed)
        videoDevices = vdevices as! [AVCaptureDevice]
        
        audioDevices = AVCaptureDevice.devices(withMediaType: AVMediaTypeAudio) as! [AVCaptureDevice]

        session.beginConfiguration()
        
        if videoDevices.contains(selectedVideoDevice!) {
            selectedVideoDevice = nil
        }
        
        if audioDevices.contains(selectedAudioDevice!) {
            selectedAudioDevice = nil
        }
        
        session.commitConfiguration()
    }
    
    func updateAudioLevels(_ timer: Timer) {
        
    }
    
    // MARK: - Device Properties
    
    static func keyPathsForValuesAffectingSelectedVideoDeviceProvidesAudio() -> NSSet {
        return NSSet(object: "selectedVideoDevice")
    }
    
    static func keyPathsForValuesAffectingVideoDeviceFormat() -> NSSet {
        return NSSet(object: "selectedVideoDevice.activeFormat")
    }
    
    static func keyPathsForValuesAffectingAudioDeviceFormat() -> NSSet {
        return NSSet(object: "selectedAudioDevice.activeFormat")
    }
    
    static func keyPathsForValuesAffectingFrameRateRange() -> NSSet {
        return NSSet(objects: "selectedVideoDevice.activeFormat.videoSupportedFrameRateRanges", "selectedVideoDevice.activeVideoMinFrameDuration")
    }

    // MARK: - Recording
    
    static func keyPathsForValuesAffectingHasRecordingDevice() -> NSSet {
        return NSSet(objects: "selectedVideoDevice", "selectedAudioDevice")
    }
    
    func hasRecordingDevice() -> Bool {
        return videoDeviceInput != nil || audioDeviceInput != nil
    }
    
    static func keyPathsForValuesAffectingRecording() -> NSSet {
        return NSSet(object: "movieFileOutput.recording")
    }

    static func keyPathsForValuesAffectingAvailableSessionPresets() -> NSSet {
        return NSSet(objects: "selectedVideoDevice", "selectedAudioDevice")
    }
    
    // MARK: - Delegate methods
    
    func captureOutputShouldProvideSampleAccurateRecordingStart(_ captureOutput: AVCaptureOutput!) -> Bool {
        return true
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
        
    }
}
