//
//  NTVMovieTableCellView.h
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 01/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class NTVMovieModel;
@interface NTVMovieTableCellView : NSTableCellView

- (void)configureWithModel:(NTVMovieModel *)model;

@end
