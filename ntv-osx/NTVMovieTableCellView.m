//
//  NTVMovieTableCellView.m
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 01/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

#import "NTVMovieTableCellView.h"
#import "NTVRecordViewController.h"
#import "ntv_osx-Swift.h"

@interface NTVMovieTableCellView ()

@property (strong, nonatomic) NTVMovieModel *model;
@property (weak) IBOutlet NSImageView *imageView;
@property (weak) IBOutlet NSTextField *labelName;
@property (weak) IBOutlet NSTextField *labelStatus;

@end


@implementation NTVMovieTableCellView

- (void)configureWithModel:(NTVMovieModel *)model {
    self.model = model;
    
    if (model.name.length) {
        self.labelName.stringValue = model.name;
        self.labelName.textColor = [NSColor whiteColor];
    } else {
        self.labelName.stringValue = @"(пусто)";
        self.labelName.textColor = [[NSColor whiteColor] colorWithAlphaComponent:0.5];
    }
    if (model.status.length) {
        self.labelStatus.stringValue = model.status;
        self.labelStatus.textColor = [NSColor whiteColor];
    } else {
        self.labelStatus.stringValue = @"(нет статуса)";
        self.labelStatus.textColor = [[NSColor whiteColor] colorWithAlphaComponent:0.5];
    }
    
    self.imageView.wantsLayer = YES;
    self.imageView.layer.backgroundColor = [NSColor bg].CGColor;
    
    
    __weak typeof(self) weakSelf = self;
    [self thumbnailImageWithCompletion:^(NSImage *image, double durationSeconds) {
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf.imageView.image = image;
    }];
}

- (void)thumbnailImageWithCompletion:(void (^)(NSImage* image, double durationSeconds))completion {
    
    if (self.model.fullfilename == nil) {
        NSLog(@"thumbnailImageWithCompletion: m_fullfilename == nil!!!");
        return;
    }
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:self.model.fullfilename options:nil];
    CMTime videoDuration = asset.duration;
    double videoDurationSeconds = CMTimeGetSeconds(videoDuration);
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = TRUE;
    CMTime thumbTime = CMTimeMakeWithSeconds(0,30);
    
    AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error){
        if (result == AVAssetImageGeneratorSucceeded) { //CGImageGetWidth(cgImage),
            NSImage *thumbnail= [[NSImage alloc] initWithCGImage:im size:NSZeroSize];
            dispatch_async(dispatch_get_main_queue(),  ^{ completion(thumbnail, videoDurationSeconds); });
        }
        else {
            dispatch_async(dispatch_get_main_queue(),  ^{
                NSLog(@"<thumbnailImageWithCompletion> result != AVAssetImageGeneratorSucceeded (%ld)", result); completion(nil, -1); });
        }
    };
    
    CGSize maxSize = CGSizeMake(320, 180);
    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:thumbTime]] completionHandler:handler];
}

@end
