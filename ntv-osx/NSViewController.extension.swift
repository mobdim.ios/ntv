//
//  NSViewController.extension.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 08/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Foundation
import Cocoa

enum ErrorText: String {
    case title_error = "Ошибка"
    case button_ok = "OK"
    case title_critical_error = "Критическая ошибка"
}

extension NSViewController {
    func showError(_ error: Error) {
        showError(nserror: error as NSError)
    }
    
    func showError(nserror error: NSError) {
        showErrorMessage(error.localizedDescription)
    }
    
    func showErrorMessage(_ message: String) {
        let myPopup: NSAlert = NSAlert()
        myPopup.messageText = ErrorText.title_error.rawValue
        myPopup.informativeText = message
        myPopup.alertStyle = NSAlertStyle.critical
        myPopup.addButton(withTitle: ErrorText.button_ok.rawValue)
        myPopup.runModal()
    }
    
    func showAlert(_ message: String) {
        let myPopup: NSAlert = NSAlert()
        myPopup.messageText = message
        myPopup.alertStyle = NSAlertStyle.informational
        myPopup.addButton(withTitle: ErrorText.button_ok.rawValue)
        myPopup.runModal()
    }
}
