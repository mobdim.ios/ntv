//
//  NSTextField+Placeholder.h
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 12/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSTextField (Placeholder)

- (void)objc_initPlaceholder:(NSString *)placeholder;

@end
