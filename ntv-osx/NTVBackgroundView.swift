//
//  NTVBackgroundView.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 12/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Cocoa

@IBDesignable
class NTVBackgroundView: NSView {

    @IBInspectable var backgroundColor: NSColor? = nil
//        NSColor(deviceRed: 40/255, green:48/255, blue:53/255, alpha:1) {
//        didSet {
////            #if !TARGET_INTERFACE_BUILDER
////            if let color = self.backgroundColor {
////                self.wantsLayer = true
////                self.layer!.backgroundColor = color.CGColor
////                self.layer!.setNeedsDisplay()
////            }
////            #endif
//            
//        }
//    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
    
}
