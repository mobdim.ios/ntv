//
//  NSStoryboard.extension.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 31/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Foundation
import Cocoa

extension NSStoryboard {
    static func main() -> NSStoryboard {
        return NSStoryboard(name: "Main", bundle: nil)
    }
}