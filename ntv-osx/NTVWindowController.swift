//
//  NTVWindowController.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 24/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Cocoa
import RxCocoa
import RxSwift

let notificationNavigationUpdate = "notificationNavigationUpdate"
let notificationNavigationName = "notificationNavigationName"
let notificationLoginSuccess = "notificationLoginSuccess"
let notificationLogoutSuccess = "notificationLogoutSuccess"
let notificationAnketa = "notificationAnketa"

class NTVWindowController: NSWindowController {

    fileprivate let disposeBag = DisposeBag()
    
    @IBOutlet weak var segmentedNavigationControl: NSSegmentedControl!
    @IBOutlet weak var buttonAnketa: NSButton!
    
    override func windowDidLoad() {
        super.windowDidLoad()
        
        self.window?.titleVisibility = .hidden
        
        segmentedNavigationControl.action = #selector(clickNavigation)
        buttonAnketa.isHidden = true
        segmentedNavigationControl.isEnabled = false
        
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: notificationLoginSuccess), object: nil).subscribeNext { [unowned self] _ in
            
            NTVWebServiceManager.sharedManager.readAnketa().subscribe(
                onNext: { [unowned self] result in
                    let anketa = result as! NTVAnketa
                    self.buttonAnketa.title = anketa.toTitleButton()
                    self.buttonAnketa.isHidden = false
                    self.segmentedNavigationControl.isEnabled = true
                }, onError: { error in
                    print("\(error)")
                    
                }
            ).addDisposableTo(self.disposeBag)

        }.addDisposableTo(self.disposeBag)
        
        buttonAnketa.rx.tap.subscribeNext {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationAnketa), object: nil)
        }.addDisposableTo(disposeBag)
        
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: notificationNavigationUpdate), object: nil).subscribeNext { [unowned self] notification in
            if let titleButton = notification.object as? String {
                self.buttonAnketa.title = titleButton
            }
            self.clickNavigation()
        }.addDisposableTo(self.disposeBag)
        
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: kNotificationSetNavigation), object: nil).subscribeNext { [unowned self] notification in
            if let index = notification.object as? NSNumber {
                self.segmentedNavigationControl.setSelected(true, forSegment: index.intValue)
                self.clickNavigation()
            }
        }.addDisposableTo(self.disposeBag)
        
//        let storyboard = NSStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateControllerWithIdentifier("NTVRecordViewController") as? NTVRecordViewController
        
        //        if let titlebarController = storyboard.instantiateControllerWithIdentifier("NTVTitlebarStatusViewController") as? NTVTitlebarStatusViewController {
        //            NSApplication.sharedApplication().mainWindow?.addTitlebarAccessoryViewController(titlebarController)
        //            //NSApplication.sharedApplication().mainWindow?.titlebarAppearsTransparent = true
        //        }

    }

    func clickNavigation() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: notificationNavigationName), object: segmentedNavigationControl.selectedSegment)
        //print("\(segmentedNavigationControl.selectedSegment)")
    }
    
}
