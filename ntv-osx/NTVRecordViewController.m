//
//  NTVRecordViewController.m
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 30/06/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

#import "NTVRecordViewController.h"

#import <AVFoundation/AVFoundation.h>

#import "NSTextField+Placeholder.h"
#import "KBButton.h"
#import "ntv_osx-Swift.h"

NSString * const kNotificationRefreshLocalFiles = @"kNotificationRefreshLocalFiles";
NSString * const kNotificationDidStartRecord = @"kNotificationDidStartRecord";
NSString * const kNotificationDidStopRecord = @"kNotificationDidStopRecord";
NSString * const kNotificationSignOut = @"kNotificationSignOut";
NSString * const kNotificationAnketaSent = @"kNotificationAnketaSent";
NSString * const kNotificationSetNavigation = @"kNotificationSetNavigation";

//#ifdef DEBUG
//DDLogLevel const ddLogLevel = DDLogLevelVerbose;
//#else
//DDLogLevel const ddLogLevel = DDLogLevelWarning;
//#endif

static NSInteger maxRecordDuration = 60;

@interface NTVRecordViewController () <AVCaptureFileOutputDelegate, AVCaptureFileOutputRecordingDelegate>

@property (weak) IBOutlet NSTextField *textName;
@property (weak) IBOutlet NSTextField *textDescription;

@property (weak) IBOutlet NSButton *buttonRecord;
@property (weak) IBOutlet KBButton *buttonSave;
@property (weak) IBOutlet KBButton *buttonUpload;
@property (weak) IBOutlet KBButton *buttonDelete;

@property (weak) IBOutlet NSView *bgCameraView;
@property (weak) IBOutlet NSView *previewView;

@property (retain) AVCaptureDeviceInput *videoDeviceInput;
@property (retain) AVCaptureDeviceInput *audioDeviceInput;
@property (readonly) BOOL selectedVideoDeviceProvidesAudio;
@property (retain) AVCaptureAudioPreviewOutput *audioPreviewOutput;
@property (retain) AVCaptureMovieFileOutput *movieFileOutput;
@property (retain) AVCaptureVideoPreviewLayer *previewLayer;
@property (assign) NSTimer *audioLevelTimer;
@property (retain) NSArray *observers;

@property (copy) NSURL *recordedFileURL;
@property (strong) NSDate *timerDate;
@property (assign) NSTimer *timer;

@property (strong, nonatomic) NTVMovieViewModel *movieViewModel;
@property (strong, nonatomic) NTVMovieModel *movieModel;

@end

@implementation NTVRecordViewController {
    IBOutlet NSView *viewbg;
    IBOutlet NSView *viewbg2;
    IBOutlet NSTextField *labelTimer;
}

- (void)awakeFromNib {
    NSColor *color = [NSColor whiteColor];
    NSMutableAttributedString *colorTitle = [[NSMutableAttributedString alloc] initWithAttributedString:[self.buttonRecord attributedTitle]];
    NSRange titleRange = NSMakeRange(0, colorTitle.length);
    [colorTitle addAttribute:NSForegroundColorAttributeName value:color range:titleRange];
    [self.buttonRecord setAttributedTitle:colorTitle];
    colorTitle = [[NSMutableAttributedString alloc] initWithAttributedString:[self.buttonRecord attributedAlternateTitle]];
    titleRange = NSMakeRange(0, colorTitle.length);
    [colorTitle addAttribute:NSForegroundColorAttributeName value:color range:titleRange];
    [self.buttonRecord setAttributedAlternateTitle:colorTitle];
    labelTimer.stringValue = @"";
}

- (void)viewDidDisappear {
    [[self session] stopRunning];
    [super viewDidDisappear];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.textName objc_initPlaceholder:@"Имя видео"];
    [self.textDescription objc_initPlaceholder:@"Описание видео"];

    
    [self.buttonSave setKBButtonType:BButtonTypeNTV];
    [self.buttonUpload setKBButtonType:BButtonTypeNTV];
    [self.buttonDelete setKBButtonType:BButtonTypeNTV];
    
    self.view.wantsLayer = YES;
    self.view.layer.backgroundColor = [NSColor bg].CGColor;
    
    viewbg.wantsLayer = YES;
    viewbg.layer.backgroundColor =  [NSColor bgDark].CGColor;
    
    viewbg2.wantsLayer = YES;
    viewbg2.layer.backgroundColor = [NSColor bgDark].CGColor;

    
    //[self _timerStart];
    
//    self.bgCameraView.backgroundColor = [NSColor colorWithRed:85./255. green:85./255. blue:85./255. alpha:1.];
    
    self.session = [[AVCaptureSession alloc] init];
    
    // Capture Notification Observers
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    id runtimeErrorObserver = [notificationCenter addObserverForName:AVCaptureSessionRuntimeErrorNotification
                                                              object:self.session
                                                               queue:[NSOperationQueue mainQueue]
                                                          usingBlock:^(NSNotification *note) {
                                                              dispatch_async(dispatch_get_main_queue(), ^(void) {
                                                                  [self presentError:[[note userInfo] objectForKey:AVCaptureSessionErrorKey]];
                                                              });
                                                          }];
    id didStartRunningObserver = [notificationCenter addObserverForName:AVCaptureSessionDidStartRunningNotification
                                                                 object:self.session
                                                                  queue:[NSOperationQueue mainQueue]
                                                             usingBlock:^(NSNotification *note) {
                                                                 NSLog(@"did start running");
                                                             }];
    id didStopRunningObserver = [notificationCenter addObserverForName:AVCaptureSessionDidStopRunningNotification
                                                                object:self.session
                                                                 queue:[NSOperationQueue mainQueue]
                                                            usingBlock:^(NSNotification *note) {
                                                                NSLog(@"did stop running");
                                                            }];
    id deviceWasConnectedObserver = [notificationCenter addObserverForName:AVCaptureDeviceWasConnectedNotification
                                                                    object:nil
                                                                     queue:[NSOperationQueue mainQueue]
                                                                usingBlock:^(NSNotification *note) {
                                                                    [self refreshDevices];
                                                                }];
    id deviceWasDisconnectedObserver = [notificationCenter addObserverForName:AVCaptureDeviceWasDisconnectedNotification
                                                                       object:nil
                                                                        queue:[NSOperationQueue mainQueue]
                                                                   usingBlock:^(NSNotification *note) {
                                                                       [self refreshDevices];
                                                                   }];
    self.observers = [[NSArray alloc] initWithObjects:runtimeErrorObserver, didStartRunningObserver, didStopRunningObserver, deviceWasConnectedObserver, deviceWasDisconnectedObserver, nil];
    
    // Attach outputs to session
    self.movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
    [self.movieFileOutput setDelegate:self];
    [self.session addOutput:self.movieFileOutput];
    
    self.audioPreviewOutput = [[AVCaptureAudioPreviewOutput alloc] init];
    [self.audioPreviewOutput setVolume:0.f];
    [self.session addOutput:self.audioPreviewOutput];
    
    // Select devices if any exist
    AVCaptureDevice *videoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (videoDevice) {
        [self setSelectedVideoDevice:videoDevice];
        [self setSelectedAudioDevice:[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio]];
    } else {
        [self setSelectedVideoDevice:[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeMuxed]];
    }
    
    // Initial refresh of device list
    [self refreshDevices];
    
    
    self.previewView.wantsLayer = YES;
    
    // Attach preview to session
    CALayer *previewViewLayer = [[self previewView] layer];
    [previewViewLayer setBackgroundColor:CGColorGetConstantColor(kCGColorBlack)];
    AVCaptureVideoPreviewLayer *newPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:[self session]];
    [newPreviewLayer setFrame:[previewViewLayer bounds]];
    [newPreviewLayer setAutoresizingMask:kCALayerWidthSizable | kCALayerHeightSizable];
    [previewViewLayer addSublayer:newPreviewLayer];
    [self setPreviewLayer:newPreviewLayer];
    
    // Start the session
    [[self session] startRunning];
    
    // Start updating the audio level meter
    [self setAudioLevelTimer:[NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(updateAudioLevels:) userInfo:nil repeats:YES]];
    
    self.buttonRecord.target = self;
    self.buttonRecord.action = @selector(clickRecord:);
    
    self.buttonUpload.target = self;
    self.buttonUpload.action = @selector(clickUpload:);
    
    self.buttonSave.target = self;
    self.buttonSave.action = @selector(clickSave:);
    
    self.buttonDelete.target = self;
    self.buttonDelete.action = @selector(clickDelete:);
    
//    self.buttonSave.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(NSButton *sender) {
//        @strongify(self);
//        [self save];
//        return [RACSignal empty];
//    }];
//    self.buttonUpload.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(NSButton *sender) {
//        @strongify(self);
//        [self upload];
//        return [RACSignal empty];
//    }];
//    self.buttonDelete.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(NSButton *sender) {
//        @strongify(self);
//        [self delete];
//        return [RACSignal empty];
//    }];
//    
//    [RACObserve(self, movieModel) subscribeNext:^(id value) {
//        @strongify(self);
//        self.buttonSave.enabled = self.buttonDelete.enabled = self.buttonUpload.enabled = (value != nil);
//    }];
    
    //    RACSignal *buttonsEnable = [RACSignal combineLatest:@[self.textName.rac_textSignal, self.textDescription.rac_textSignal] reduce:^id(NSString *name, NSString *description) {
    //        return @(name.length && description.length);
    //    }];
    
    //    RAC(self, buttonSave.enabled) = buttonsEnable;
    //    RAC(self, buttonUpload.enabled) = buttonsEnable;
    
}

- (void)clickRecord:(NSButton *)sender {
    switch (sender.state) {
        case NSOnState:
            [self setRecording:YES];
            break;
        case NSOffState:
            [self setRecording:NO];
            break;
            
        default:
            break;
    }
}

#pragma mark - Device selection
- (void)refreshDevices
{
    [self setVideoDevices:[[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] arrayByAddingObjectsFromArray:[AVCaptureDevice devicesWithMediaType:AVMediaTypeMuxed]]];
    [self setAudioDevices:[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio]];
    
    [[self session] beginConfiguration];
    
    if (![[self videoDevices] containsObject:[self selectedVideoDevice]])
        [self setSelectedVideoDevice:nil];
    
    if (![[self audioDevices] containsObject:[self selectedAudioDevice]])
        [self setSelectedAudioDevice:nil];
    
    [[self session] commitConfiguration];
}

- (AVCaptureDevice *)selectedVideoDevice
{
    return [self.videoDeviceInput device];
}

- (void)setSelectedVideoDevice:(AVCaptureDevice *)selectedVideoDevice
{
    [[self session] beginConfiguration];
    
    if ([self videoDeviceInput]) {
        // Remove the old device input from the session
        [self.session removeInput:[self videoDeviceInput]];
        [self setVideoDeviceInput:nil];
    }
    
    if (selectedVideoDevice) {
        NSError *error = nil;
        
        // Create a device input for the device and add it to the session
        AVCaptureDeviceInput *newVideoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:selectedVideoDevice error:&error];
        if (newVideoDeviceInput == nil) {
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self presentError:error];
            });
        } else {
            if (![selectedVideoDevice supportsAVCaptureSessionPreset:[self.session sessionPreset]])
                [[self session] setSessionPreset:AVCaptureSessionPresetHigh];
            
            [[self session] addInput:newVideoDeviceInput];
            [self setVideoDeviceInput:newVideoDeviceInput];
        }
    }
    
    // If this video device also provides audio, don't use another audio device
    if ([self selectedVideoDeviceProvidesAudio])
        [self setSelectedAudioDevice:nil];
    
    [[self session] commitConfiguration];
}

- (AVCaptureDevice *)selectedAudioDevice
{
    return [self.audioDeviceInput device];
}

- (void)setSelectedAudioDevice:(AVCaptureDevice *)selectedAudioDevice
{
    [[self session] beginConfiguration];
    
    if ([self audioDeviceInput]) {
        // Remove the old device input from the session
        [self.session removeInput:[self audioDeviceInput]];
        [self setAudioDeviceInput:nil];
    }
    
    if (selectedAudioDevice && ![self selectedVideoDeviceProvidesAudio]) {
        NSError *error = nil;
        
        // Create a device input for the device and add it to the session
        AVCaptureDeviceInput *newAudioDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:selectedAudioDevice error:&error];
        if (newAudioDeviceInput == nil) {
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self presentError:error];
            });
        } else {
            if (![selectedAudioDevice supportsAVCaptureSessionPreset:[self.session sessionPreset]])
                [[self session] setSessionPreset:AVCaptureSessionPresetHigh];
            
            [[self session] addInput:newAudioDeviceInput];
            [self setAudioDeviceInput:newAudioDeviceInput];
        }
    }
    
    [[self session] commitConfiguration];
}


#pragma mark - Device Properties

+ (NSSet *)keyPathsForValuesAffectingSelectedVideoDeviceProvidesAudio
{
    return [NSSet setWithObjects:@"selectedVideoDevice", nil];
}

- (BOOL)selectedVideoDeviceProvidesAudio
{
    return ([[self selectedVideoDevice] hasMediaType:AVMediaTypeMuxed] || [[self selectedVideoDevice] hasMediaType:AVMediaTypeAudio]);
}

+ (NSSet *)keyPathsForValuesAffectingVideoDeviceFormat
{
    return [NSSet setWithObjects:@"selectedVideoDevice.activeFormat", nil];
}

- (AVCaptureDeviceFormat *)videoDeviceFormat
{
    return [[self selectedVideoDevice] activeFormat];
}

- (void)setVideoDeviceFormat:(AVCaptureDeviceFormat *)deviceFormat
{
    NSError *error = nil;
    AVCaptureDevice *videoDevice = [self selectedVideoDevice];
    if ([videoDevice lockForConfiguration:&error]) {
        [videoDevice setActiveFormat:deviceFormat];
        [videoDevice unlockForConfiguration];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self presentError:error];
        });
    }
}

+ (NSSet *)keyPathsForValuesAffectingAudioDeviceFormat
{
    return [NSSet setWithObjects:@"selectedAudioDevice.activeFormat", nil];
}

- (AVCaptureDeviceFormat *)audioDeviceFormat
{
    return [[self selectedAudioDevice] activeFormat];
}

- (void)setAudioDeviceFormat:(AVCaptureDeviceFormat *)deviceFormat
{
    NSError *error = nil;
    AVCaptureDevice *audioDevice = [self selectedAudioDevice];
    if ([audioDevice lockForConfiguration:&error]) {
        [audioDevice setActiveFormat:deviceFormat];
        [audioDevice unlockForConfiguration];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self presentError:error];
        });
    }
}

+ (NSSet *)keyPathsForValuesAffectingFrameRateRange
{
    return [NSSet setWithObjects:@"selectedVideoDevice.activeFormat.videoSupportedFrameRateRanges", @"selectedVideoDevice.activeVideoMinFrameDuration", nil];
}

- (AVFrameRateRange *)frameRateRange
{
    AVFrameRateRange *activeFrameRateRange = nil;
    for (AVFrameRateRange *frameRateRange in [[[self selectedVideoDevice] activeFormat] videoSupportedFrameRateRanges])
    {
        if (CMTIME_COMPARE_INLINE([frameRateRange minFrameDuration], ==, [[self selectedVideoDevice] activeVideoMinFrameDuration]))
        {
            activeFrameRateRange = frameRateRange;
            break;
        }
    }
    
    return activeFrameRateRange;
}

- (void)setFrameRateRange:(AVFrameRateRange *)frameRateRange
{
    NSError *error = nil;
    if ([[[[self selectedVideoDevice] activeFormat] videoSupportedFrameRateRanges] containsObject:frameRateRange])
    {
        if ([[self selectedVideoDevice] lockForConfiguration:&error]) {
            [[self selectedVideoDevice] setActiveVideoMinFrameDuration:[frameRateRange minFrameDuration]];
            [[self selectedVideoDevice] unlockForConfiguration];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self presentError:error];
            });
        }
    }
}

- (IBAction)lockVideoDeviceForConfiguration:(id)sender
{
    if ([(NSButton *)sender state] == NSOnState)
    {
        [[self selectedVideoDevice] lockForConfiguration:nil];
    }
    else
    {
        [[self selectedVideoDevice] unlockForConfiguration];
    }
}

#pragma mark - Recording

+ (NSSet *)keyPathsForValuesAffectingHasRecordingDevice
{
    return [NSSet setWithObjects:@"selectedVideoDevice", @"selectedAudioDevice", nil];
}

- (BOOL)hasRecordingDevice
{
    return ((self.videoDeviceInput != nil) || (self.audioDeviceInput != nil));
}

+ (NSSet *)keyPathsForValuesAffectingRecording
{
    return [NSSet setWithObject:@"movieFileOutput.recording"];
}

- (BOOL)isRecording
{
    return [[self movieFileOutput] isRecording];
}

- (void)setRecording:(BOOL)record
{
    if (record) {
        self.buttonRecord.enabled = NO;
        self.buttonSave.enabled = NO;
        self.buttonDelete.enabled = NO;
        self.buttonUpload.enabled = NO;
        self.textName.editable = NO;
        self.textDescription.editable = NO;
        // Record to a temporary file, which the user will relocate when recording is finished
        char *tempNameBytes = tempnam([NSTemporaryDirectory() fileSystemRepresentation], "AVRecorder_");
        NSString *tempName = [[NSString alloc] initWithBytesNoCopy:tempNameBytes length:strlen(tempNameBytes) encoding:NSUTF8StringEncoding freeWhenDone:YES];
        
        [[self movieFileOutput] startRecordingToOutputFileURL:[NSURL fileURLWithPath:[tempName stringByAppendingPathExtension:@"mov"]]
                                            recordingDelegate:self];
    } else {
        [[self movieFileOutput] stopRecording];
        self.buttonSave.enabled = YES;
        self.buttonDelete.enabled = YES;
        self.buttonUpload.enabled = YES;
        self.textName.editable = YES;
        self.textDescription.editable = YES;
    }
}

+ (NSSet *)keyPathsForValuesAffectingAvailableSessionPresets
{
    return [NSSet setWithObjects:@"selectedVideoDevice", @"selectedAudioDevice", nil];
}

- (NSArray *)availableSessionPresets
{
    NSArray *allSessionPresets = [NSArray arrayWithObjects:
                                  AVCaptureSessionPresetLow,
                                  AVCaptureSessionPresetMedium,
                                  AVCaptureSessionPresetHigh,
                                  AVCaptureSessionPreset320x240,
                                  AVCaptureSessionPreset352x288,
                                  AVCaptureSessionPreset640x480,
                                  AVCaptureSessionPreset960x540,
                                  AVCaptureSessionPreset1280x720,
                                  AVCaptureSessionPresetPhoto,
                                  nil];
    
    NSMutableArray *availableSessionPresets = [NSMutableArray arrayWithCapacity:9];
    for (NSString *sessionPreset in allSessionPresets) {
        if ([[self session] canSetSessionPreset:sessionPreset])
            [availableSessionPresets addObject:sessionPreset];
    }
    
    return availableSessionPresets;
}

#pragma mark - Audio Preview

- (float)previewVolume
{
    return [[self audioPreviewOutput] volume];
}

- (void)setPreviewVolume:(float)newPreviewVolume
{
    [[self audioPreviewOutput] setVolume:newPreviewVolume];
}

- (void)updateAudioLevels:(NSTimer *)timer
{
    NSInteger channelCount = 0;
    float decibels = 0.f;
    
    // Sum all of the average power levels and divide by the number of channels
    for (AVCaptureConnection *connection in [[self movieFileOutput] connections]) {
        for (AVCaptureAudioChannel *audioChannel in [connection audioChannels]) {
            decibels += [audioChannel averagePowerLevel];
            channelCount += 1;
        }
    }
    
    decibels /= channelCount;
    
//    [[self audioLevelMeter] setFloatValue:(pow(10.f, 0.05f * decibels) * 20.0f)];
}

#pragma mark - Transport Controls

- (IBAction)stop:(id)sender
{
    [self setTransportMode:AVCaptureDeviceTransportControlsNotPlayingMode speed:0.f forDevice:[self selectedVideoDevice]];
}

+ (NSSet *)keyPathsForValuesAffectingPlaying
{
    return [NSSet setWithObjects:@"selectedVideoDevice.transportControlsPlaybackMode", @"selectedVideoDevice.transportControlsSpeed",nil];
}

- (BOOL)isPlaying
{
    AVCaptureDevice *device = [self selectedVideoDevice];
    return ([device transportControlsSupported] &&
            [device transportControlsPlaybackMode] == AVCaptureDeviceTransportControlsPlayingMode &&
            [device transportControlsSpeed] == 1.f);
}

- (void)setPlaying:(BOOL)play
{
    AVCaptureDevice *device = [self selectedVideoDevice];
    [self setTransportMode:AVCaptureDeviceTransportControlsPlayingMode speed:play ? 1.f : 0.f forDevice:device];
}

+ (NSSet *)keyPathsForValuesAffectingRewinding
{
    return [NSSet setWithObjects:@"selectedVideoDevice.transportControlsPlaybackMode", @"selectedVideoDevice.transportControlsSpeed",nil];
}

- (BOOL)isRewinding
{
    AVCaptureDevice *device = [self selectedVideoDevice];
    return [device transportControlsSupported] && ([device transportControlsSpeed] < -1.f);
}

- (void)setRewinding:(BOOL)rewind
{
    AVCaptureDevice *device = [self selectedVideoDevice];
    [self setTransportMode:[device transportControlsPlaybackMode] speed:rewind ? -2.f : 0.f forDevice:device];
}

+ (NSSet *)keyPathsForValuesAffectingFastForwarding
{
    return [NSSet setWithObjects:@"selectedVideoDevice.transportControlsPlaybackMode", @"selectedVideoDevice.transportControlsSpeed",nil];
}

- (BOOL)isFastForwarding
{
    AVCaptureDevice *device = [self selectedVideoDevice];
    return [device transportControlsSupported] && ([device transportControlsSpeed] > 1.f);
}

- (void)setFastForwarding:(BOOL)fastforward
{
    AVCaptureDevice *device = [self selectedVideoDevice];
    [self setTransportMode:[device transportControlsPlaybackMode] speed:fastforward ? 2.f : 0.f forDevice:device];
}

- (void)setTransportMode:(AVCaptureDeviceTransportControlsPlaybackMode)playbackMode speed:(AVCaptureDeviceTransportControlsSpeed)speed forDevice:(AVCaptureDevice *)device
{
    NSError *error = nil;
    if ([device transportControlsSupported]) {
        if ([device lockForConfiguration:&error]) {
            [device setTransportControlsPlaybackMode:playbackMode speed:speed];
            [device unlockForConfiguration];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self presentError:error];
            });
        }
    }
}

#pragma mark - Delegate methods

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didStartRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections
{
    NSLog(@"Did start recording to %@", [fileURL description]);
    [self performSelectorOnMainThread:@selector(_timerStart) withObject:nil waitUntilDone:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDidStartRecord object:nil];
    self.buttonRecord.enabled = YES;
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didPauseRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections
{
    NSLog(@"Did pause recording to %@", [fileURL description]);
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didResumeRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections
{
    NSLog(@"Did resume recording to %@", [fileURL description]);
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput willFinishRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections dueToError:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self presentError:error];
    });
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDidStopRecord object:nil];
    [self _timerStop];
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)recordError
{
    if (recordError != nil && [[[recordError userInfo] objectForKey:AVErrorRecordingSuccessfullyFinishedKey] boolValue] == NO) {
        [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self presentError:recordError];
        });
    } else {
        self.recordedFileURL = outputFileURL;
        NSLog(@"Файл записан:\r%@", self.recordedFileURL);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDidStopRecord object:nil];
    [self _timerStop];
}

- (BOOL)captureOutputShouldProvideSampleAccurateRecordingStart:(AVCaptureOutput *)captureOutput
{
    // We don't require frame accurate start when we start a recording. If we answer YES, the capture output
    // applies outputSettings immediately when the session starts previewing, resulting in higher CPU usage
    // and shorter battery life.
    return NO;
}

#pragma mark - Operations

- (void)clickSave:(NSButton *)sender {
    [self save: YES];
}

- (void)clickUpload:(NSButton *)sender {
    [self upload];
}

- (void)clickDelete:(NSButton *)delete {
    [self delete];
}

- (void)save:(BOOL)showSavedMessage {
    NSLog(@"Сохранение временного файла:\r%@", self.recordedFileURL);
    
    NSURL *movieFile = [[NTVLocalFilesManager sharedManager] newMovieFile];
    NSString *filename = [movieFile lastPathComponent];
    self.movieModel = [NTVMovieModel new];
    self.movieModel.filename = filename;
    self.movieModel.fullfilename = movieFile;
    self.movieModel.fullFileNameStorage = movieFile.path;
    self.movieModel.name = self.textName.stringValue;
    self.movieModel.descript = self.textDescription.stringValue;
    self.movieModel.date = self.timerDate;
    
    NSError *error = nil;
    
    if (![[NSFileManager defaultManager] moveItemAtURL:self.recordedFileURL toURL:self.movieModel.fullfilename error:&error]) {
        NSLog(@"%@", error);
        [self showErrorMessage:@"Ошибка сохранения файла!"];
    }
    
    NSLog(@"Временный файл сохранен:\r%@", self.movieModel.fullfilename);
    
    if (!error) {
        [[NTVLocalFilesManager sharedManager] addLocalMovie:self.movieModel];
        if (showSavedMessage) {
            [self showAlert:@"Успешное сохранение файла!"];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRefreshLocalFiles object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSetNavigation object:[NSNumber numberWithInteger:1]];
}

- (void)upload {
    [self save: NO];
    [DJProgressHUD showStatus:@"" FromView:self.view];
    self.movieViewModel = [[NTVMovieViewModel alloc] initWithModel:self.movieModel];
    self.movieViewModel.selectedIndex = [NTVLocalFilesManager sharedManager].movies.count - 1;
    [self.movieViewModel upload_objc:^{
        NSLog(@"upload success");
        self.movieModel.uploaded = YES;
        [[NTVLocalFilesManager sharedManager] updateLocalMove:self.movieModel atIndex:self.movieViewModel.selectedIndex];
        [DJProgressHUD dismiss];
        [self showAlert:@"Успешная загрузка файла!"];
    } failed:^(NSError * _Nonnull error) {
        [DJProgressHUD dismiss];
        NSLog(@"%@", error);
        [self showError:error];
    }];
//    @weakify(self);
//    [self.movieViewModel.commandUpload.executing subscribeNext:^(NSNumber *executing) {
//        @strongify(self);
//        if (executing.boolValue) {
//            [DJProgressHUD showStatus:@"" FromView:self.view];
//        } else {
//            [DJProgressHUD dismiss];
//        }
//    }];
//    [self.movieViewModel.commandUpload execute:nil];
}

- (void)delete {
    NSLog(@"Удаление временного файла:\r%@", self.recordedFileURL);
    if (!self.recordedFileURL) {
        return;
    }
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtURL:self.recordedFileURL error:&error];
    if (!error) {
        [self showAlert:@"Файл удален!"];
        self.recordedFileURL = nil;
    } else {
        [self showError:error];
    }
    
}

#pragma mark - Timers

- (void) _timerStart {
    NSLog(@"Запуск таймера");
    self.timerDate = [NSDate date];
    [self setTimer:[NSTimer scheduledTimerWithTimeInterval:1. target:self selector:@selector(timertick:) userInfo:nil repeats:YES]];
}

- (void) _timerStop {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)timertick:(NSTimer *)timer {
    NSLog(@"Таймер тикнул");
    NSTimeInterval elapsedTime = ABS([self.timerDate timeIntervalSinceNow]);
    div_t h = div(elapsedTime, 3600);
    int hours = h.quot;
    div_t m = div(h.rem, 60);
    int minutes = m.quot;
    int seconds = m.rem;
    
    labelTimer.stringValue = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
}

@end
