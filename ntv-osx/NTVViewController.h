//
//  NTVViewController.h
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 30/06/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NTVViewController : NSViewController

+ (instancetype)loadViewController:(NSString *)storyboardId;

@end
