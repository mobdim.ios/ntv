//
//  NSTextFiled.extension.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 12/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Foundation

extension NSTextField {
    func initPlaceholder(_ placeholder: String) {
        let color = NSColor.lightGray
        let attrs = [NSForegroundColorAttributeName: color]
        let placeHolderStr = NSAttributedString(string: placeholder, attributes: attrs)
        
        placeholderAttributedString = placeHolderStr
    }
}
