//
//  NTVAccountManager.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 28/06/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Foundation

class NTVAccountManager {
    
    fileprivate var authorize = false
    
    static let sharedInstance = NTVAccountManager()
    
    static var isAuthorized: Bool {
        get {
            return NTVAccountManager.sharedInstance.authorize
        }
    }
    
    static func signOut() {
        
    }
}
