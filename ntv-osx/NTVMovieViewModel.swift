//
//  NTVMovieViewModel.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 04/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Cocoa
import RxCocoa
import RxSwift

class NTVMovieViewModel: NSObject {
    fileprivate let disposeBag = DisposeBag()
    
    var selectedIndex: NSInteger = -1
    
    fileprivate let model: NTVMovieModel
    
    init(model: NTVMovieModel) {
        self.model = model
        super.init()
    }
    
    func upload_objc(_ success: @escaping (Void) -> Void, failed: @escaping (NSError) -> Void) {
        upload().subscribe(
            onNext: { _ in
                success()
            }, onError: { error in
                failed(error as NSError)
            }
        ).addDisposableTo(disposeBag)
    }
    
    func upload() -> Observable<Any> {
        return Observable.create { [unowned self] observer in
        
            NTVWebServiceManager.sharedManager.canUploadMovie(self.model.name!, description: self.model.descript!)
                .subscribe(
                    onNext: { [unowned self] response in
                        let result = response as! NTVNetworkResult
                        self.model.id = Int32(result.id!.intValue)
                        
                        //MRProgressOverlayView.showOverlayAddedTo(self.navigationController?.view, animated: true)
                        NTVWebServiceManager.sharedManager.uploadMovie(self.model).subscribe(
                            onNext: { result in
                                print("upload success:\n\(result)")
                                //let r = result as! [NSString: AnyObject?]
                                observer.onNext(result)
                                observer.onCompleted()
                                //MRProgressOverlayView.dismissOverlayForView(self.navigationController?.view, animated: true)
                            }, onError: { error in
                                //MRProgressOverlayView.dismissOverlayForView(self.navigationController?.view, animated: true)
                                print("\(error)")
                                observer.onError(error)
                            }
                        ).addDisposableTo(self.disposeBag)
                    }, onError: { error in
                        print("\(error)")
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            return NopDisposable.instance
        }
    }
}
