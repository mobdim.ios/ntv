//
//  NTVCameraViewController.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 24/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Cocoa
import AVFoundation

class NTVCameraViewController: NSViewController, AVCaptureFileOutputDelegate, AVCaptureFileOutputRecordingDelegate {
    
    @IBOutlet weak var previewView: NSView!
    
    var videoDeviceInput: AVCaptureDeviceInput?
    var audioDeviceInput: AVCaptureDeviceInput?
    var selectedVideoDeviceProvidesAudio: Bool = false // readonly!
    var audioPreviewOutput: AVCaptureAudioPreviewOutput?
    var movieFileOutput: AVCaptureMovieFileOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    var audioLevelTimer: Timer?
    var observers: NSArray?
    @IBOutlet weak var buttonClose: NSButton!
    
    var videoDevices: NSArray?
    var audioDevices: NSArray?
    var selectedVideoDevice: AVCaptureDevice?
    var selectedAudioDevice: AVCaptureDevice?
    
    var videoDeviceFormat: AVCaptureDeviceFormat?
    var audioDeviceFormat: AVCaptureDeviceFormat?
    var frameRateRange: AVFrameRateRange?

    var session: AVCaptureSession?
    var availableSessionPresets: NSArray?
    var hasRecordingDevice: Bool = false // readonly!
    var recording: Bool = false
    
    var previewVolume: Float = 0
    
    var playing = false
    var rewinding = false
    let fastForwarding = false
    
    override init?(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.session = AVCaptureSession()
        
        let notificationCenter = NotificationCenter.default
        
        
    }
    
    // MARK: - Camera Delegate methods
    
    func captureOutputShouldProvideSampleAccurateRecordingStart(_ captureOutput: AVCaptureOutput!) -> Bool {
        return false
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAt fileURL: URL!, fromConnections connections: [Any]!) {
        
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didPauseRecordingToOutputFileAt fileURL: URL!, fromConnections connections: [Any]!) {
        
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didResumeRecordingToOutputFileAt fileURL: URL!, fromConnections connections: [Any]!) {
        
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, willFinishRecordingToOutputFileAt fileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
        
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
        
    }
    
}
