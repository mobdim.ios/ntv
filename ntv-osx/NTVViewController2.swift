//
//  NTVViewController.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 28/06/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Cocoa

class NTVViewController2: NSViewController {

    static func loadViewController(_ storyboardId: String) -> AnyObject {
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        
        return storyboard.instantiateController(withIdentifier: storyboardId) as AnyObject
    }
    
}
