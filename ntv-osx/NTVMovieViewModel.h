//
//  NTVMovieViewModel.h
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 03/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NTVMovieModel;
@interface NTVMovieViewModel : NSObject

- (instancetype)initWithModel:(NTVMovieModel *)model;

- (void)upload;

@property (nonatomic, assign) NSInteger selectedIndex;


@end
