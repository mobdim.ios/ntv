//
//  ViewController.swift
//  ntv-osx
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 15/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Cocoa
import SnapKit

class ViewController: NSViewController {

    let tabView = NSTabView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.frame.size = CGSize(width: 700, height: 800)
        
        
        view.addSubview(tabView)
        tabView.snp.makeConstraints { make in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(0)
            make.height.equalTo(800)
        }
        let tabCamera = NSTabViewItem(viewController: NTVCameraViewController())
        tabView.addTabViewItem(tabCamera)
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

