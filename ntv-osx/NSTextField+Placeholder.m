//
//  NSTextField+Placeholder.m
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 12/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

#import "NSTextField+Placeholder.h"

@implementation NSTextField (Placeholder)

- (void)objc_initPlaceholder:(NSString *)placeholder {
    NSColor *color = [NSColor lightGrayColor];
    NSAttributedString *attrs = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: color}];

    self.placeholderAttributedString = attrs;
}
@end
