//
//  KBButton.m
//  KBButton
//
//  Created by Kyle Bock on 11/3/12.
//  Copyright (c) 2012 Kyle Bock. All rights reserved.
//

#import "KBButton.h"
#import "KBButtonCell.h"

@implementation KBButton

- (void) setKBButtonType:(BButtonType)type {
    if ((type == BButtonTypeNTV)
     || (type == BButtonTypeNTVRed)) {
        [self enableTrackingMouse];
        [self setOnlyBorder:YES];
    }
    
    [self.cell setKBButtonType:type];
}

- (void)setOnlyBorder:(BOOL)onlyBorder {
    ((KBButtonCell *)self.cell).onlyBorder = onlyBorder;
}

- initWithCoder: (NSCoder *)origCoder
{
	BOOL sub = YES;
	
	sub = sub && [origCoder isKindOfClass: [NSKeyedUnarchiver class]]; // no support for 10.1 nibs
	sub = sub && ![self isMemberOfClass: [NSControl class]]; // no raw NSControls
	sub = sub && [[self superclass] cellClass] != nil; // need to have something to substitute
	sub = sub && [[self superclass] cellClass] != [[self class] cellClass]; // pointless if same
	
	if( !sub )
	{
		self = [super initWithCoder: origCoder];
	}
	else
	{
		NSKeyedUnarchiver *coder = (id)origCoder;
		
		// gather info about the superclass's cell and save the archiver's old mapping
		Class superCell = [[self superclass] cellClass];
		NSString *oldClassName = NSStringFromClass( superCell );
		Class oldClass = [coder classForClassName: oldClassName];
		if( !oldClass )
			oldClass = superCell;
		
		// override what comes out of the unarchiver
		[coder setClass: [[self class] cellClass] forClassName: oldClassName];
		
		// unarchive
		self = [super initWithCoder: coder];
		
		// set it back
		[coder setClass: oldClass forClassName: oldClassName];
	}
	
	return self;
}

- (void)enableTrackingMouse {
    NSTrackingArea *trackingArea = [[NSTrackingArea alloc] initWithRect:self.bounds options:NSTrackingMouseEnteredAndExited | NSTrackingActiveAlways owner:self userInfo:nil];
    [self addTrackingArea:trackingArea];
    
    ((KBButtonCell *)self.cell).tracking = YES;
}

- (void)mouseEntered:(NSEvent *)theEvent {
    ((KBButtonCell *)self.cell).hover = YES;
    [self setNeedsDisplay];
}

- (void)mouseExited:(NSEvent *)theEvent {
    ((KBButtonCell *)self.cell).hover = NO;
    [self setNeedsDisplay];
}

+ (Class)cellClass
{
    return [KBButtonCell class];
}

@end
