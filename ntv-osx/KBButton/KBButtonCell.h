//
//  KBButtonCell.h
//  KBButton
//
//  Created by Kyle Bock on 11/2/12.
//  Copyright (c) 2012 Kyle Bock. All rights reserved.
//

#import <Cocoa/Cocoa.h>

typedef enum {
    BButtonTypeDefault = 0,
    BButtonTypePrimary,
    BButtonTypeInfo,
    BButtonTypeSuccess,
    BButtonTypeWarning,
    BButtonTypeDanger,
    BButtonTypeInverse,
    BButtonTypeDark,
    BButtonTypeCustom,
    BButtonTypeNTV,
    BButtonTypeNTVRed
} BButtonType;

@interface KBButtonCell : NSButtonCell {
    NSColor *_color;
    BButtonType kbButtonType;
}

- (void)setKBButtonType:(BButtonType)type;

@property (strong) NSColor *backgroundColor;
@property (assign, nonatomic) BOOL onlyBorder;
@property (assign, nonatomic) BOOL tracking;

@property (strong, nonatomic) NSColor *colorNormal;
@property (strong, nonatomic) NSColor *colorTracking;
@property (assign, nonatomic) BOOL hover;

@end

