//
//  NTVGalleryViewController.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 29/06/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Cocoa

import AVFoundation
import AVKit
import RxCocoa
import RxSwift

class NTVGalleryViewController: NTVViewController, NSTableViewDataSource, NSTableViewDelegate {

    fileprivate let disposeBag = DisposeBag()
    
    @IBOutlet var playerView: AVPlayerView!
    
    @IBOutlet var buttonUpload: KBButton!
    @IBOutlet var buttonSave: KBButton!
    @IBOutlet var buttonDelete: KBButton!
    
    @IBOutlet var textName: NSTextField!
    @IBOutlet var textDescription: NSTextField!
    
    @IBOutlet var tableView: NSTableView!
    
    @IBOutlet weak var viewTextName: NSView!
    @IBOutlet weak var viewDescriptionName: NSView!
    
    var statuses = [Int: String]()
    
    var playerWithModel: NTVMovieModel? {
        didSet {
            guard let model = playerWithModel else {
                self.playerView.player = AVPlayer()
                return
            }
            
            if let filename = model.fullfilename {
                self.playerView.player = AVPlayer(url: filename)
            } else {
                self.playerView.player = AVPlayer()
            }
        }
    }
    
    var movieViewModel: NTVMovieViewModel? = nil
    var selectedModel: NTVMovieModel? = nil
    
    var selectedIndex: NSInteger = -1 {
        didSet {
            if selectedIndex != -1 {
                let model = NTVLocalFilesManager.sharedManager.movies[selectedIndex]
                
                self.buttonUpload.title = model.uploaded ? "Сохранить" : "Отправить"
                
                self.selectedModel = model
                self.playerWithModel = model
                self.textName.stringValue = model.name == nil ? "" : model.name!
                self.textDescription.stringValue = model.descript == nil ? "" : model.descript!
//                if (model.uploaded) {
//                    self.imageIndicator.image = [NSImage imageNamed:@"indicator_uploaded"];
//                    self.labelIdicatior.stringValue = NSLocalizedString(@"indicator_uploaded", nil);
//                } else {
//                    self.imageIndicator.image = [NSImage imageNamed:@"indicator_not_uploaded"];
//                    self.labelIdicatior.stringValue = NSLocalizedString(@"indicator_not_uploaded", nil);
//                }
                self.textName.isEnabled = true
                self.textDescription.isEditable = true
                self.textDescription.isSelectable = true
                self.buttonSave.isEnabled = true
                self.buttonUpload.isEnabled = true
                self.buttonDelete.isEnabled = true
            } else {
                self.playerWithModel = nil
                self.textName.stringValue = ""
                self.textDescription.stringValue = ""
//                self.imageIndicator.image = nil;
//                self.labelIdicatior.stringValue = @"";
                
                self.textName.isEnabled = false
                self.textDescription.isSelectable = false
                self.textDescription.isEditable = false
                self.buttonSave.isEnabled = false
                self.buttonUpload.isEnabled = false
                self.buttonDelete.isEnabled = false
            }
        }
    }
    
    override init?(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        let sid = UserDefaults.standard.object(forKey: "sid") as? String
        NTVWebServiceManager.sharedManager.getFiles(sid!).subscribeNext { statuses in
            self.statuses = statuses as! [Int: String]
            
            for movie in NTVLocalFilesManager.sharedManager.movies {
                if let status = self.statuses[Int(movie.id)] {
                    movie.status = status;
                }
            }
            self.tableView.reloadData();
        }.addDisposableTo(self.disposeBag)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textName.initPlaceholder("Имя видео")
        textDescription.initPlaceholder("Описание видео")
        
//        tableView.wantsLayer = true
//        tableView.layer!.backgroundColor = NSColor.bg().cgColor
//        
        view.wantsLayer = true
        view.layer!.backgroundColor = NSColor.bg().cgColor
        
        viewTextName.wantsLayer = true
        viewTextName.layer!.backgroundColor = NSColor.bgDark().cgColor
        
        viewDescriptionName.wantsLayer = true
        viewDescriptionName.layer!.backgroundColor = NSColor.bgDark().cgColor
        
        buttonUpload.setKBButtonType(BButtonTypeNTV)
        buttonSave.setKBButtonType(BButtonTypeNTV)
        buttonDelete.setKBButtonType(BButtonTypeNTV)
        
        buttonUpload.target = self
        buttonUpload.action = #selector(upload)
        
        buttonSave.target = self
        buttonSave.action = #selector(save)
        
        buttonDelete.target = self
        buttonDelete.action = #selector(delete)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        NotificationCenter.default
            .rx.notification(Notification.Name(rawValue: kNotificationRefreshLocalFiles))
            .subscribeNext { [unowned self] notification in
                NTVLocalFilesManager.sharedManager.refreshLocal()
                self.tableView.reloadData()
                let index = self.selectedIndex
                self.selectedIndex = index        
        }.addDisposableTo(self.disposeBag)
        
//        NSNotificationCenter.defaultCenter().rx.notification(notificationNavigationName, object: nil).subscribeNext { [unowned self] _ in
//            NTVLocalFilesManager.sharedManager.refreshLocal()
//            self.tableView.reloadData()
//        }.addDisposableTo(self.disposeBag)
        
        NTVLocalFilesManager.sharedManager.refreshLocal()
        tableView.reloadData()
    }
    
    // MARK: - Tables
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return NTVLocalFilesManager.sharedManager.movies.count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let cellId = "movieCellId"
        
        let cellView = tableView.make(withIdentifier: cellId, owner: self) as! NTVMovieTableCellView
        
        cellView.configure(with: NTVLocalFilesManager.sharedManager.movies[row])
        
        return cellView;
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        let tableView = notification.object as! NSTableView
        
        let rowIndex = tableView.selectedRow
        
        if rowIndex == -1 {
            return
        }
        
        selectedIndex = rowIndex
    }
    
    // MARK: - Buttons
    
    func save(_ showSavedMessage: Bool = true) {
        if let model = selectedModel {
            model.name = textName.stringValue
            model.descript = textDescription.stringValue
            NTVLocalFilesManager.sharedManager.updateLocalMove(model, atIndex: selectedIndex)
            NotificationCenter.default.post(name: Notification.Name(rawValue: kNotificationRefreshLocalFiles), object: nil)
            if showSavedMessage {
                self.showAlert("Успешное сохранение файла!")
            }
        }
    }
    
    func upload() {
        if let model = selectedModel {
        
            if model.uploaded {
                save()
            } else {
                save(false)
                DJProgressHUD.showStatus("", from: self.view)
                movieViewModel = NTVMovieViewModel(model: model)
                movieViewModel!.selectedIndex = selectedIndex
                movieViewModel!.upload().subscribe(
                    onNext: { [unowned self] result in
                        self.selectedModel!.id = Int32((result as! NTVNetworkResult).id!.intValue)
                        self.selectedModel!.uploaded = true
                        self.save(false)
                        print("success upload")
                        self.buttonUpload.title = "Сохранить"
                        DJProgressHUD.dismiss()
                        self.showAlert("Успешная загрузка файла!")
                    }, onError: { [unowned self] error in
                        print("\(error)")
                        DJProgressHUD.dismiss()
                        self.showError(error)
                    }
                ).addDisposableTo(disposeBag)
            }
        }
    }
    
    func delete() {
        NTVLocalFilesManager.sharedManager.removeLocalMovieAtIndex(selectedIndex)
        selectedIndex = -1
        textName.stringValue = ""
        textDescription.stringValue = ""
        playerWithModel = nil
        let indexSet = IndexSet(integer: -1)
        tableView.selectRowIndexes(indexSet, byExtendingSelection: false)
        NotificationCenter.default.post(name: Notification.Name(rawValue: kNotificationRefreshLocalFiles), object: nil)
        self.showAlert("Успешное удаление файла!")
    }
}
