//
//  NTVMainViewController.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 28/06/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Cocoa
import RxCocoa
import RxSwift

enum ViewControllerType: String {
    case Login = "NTVLoginViewController"
    case Record = "NTVRecordViewController"
    case Gallery = "NTVGalleryViewController"
    case Anketa = "NTVAnketaViewController"
}

class NTVMainViewController: NSViewController {

    fileprivate let disposeBag = DisposeBag()
    
    @IBOutlet weak var containerView: NSView!
    
    fileprivate var currentController: NSViewController?
        
    var controllerType: ViewControllerType = .Login {
        didSet {
            
            if let current = currentController {
                current.view.removeFromSuperview()
            }
            
            switch controllerType {
            case .Login:
                currentController = NTVLoginViewController.load("NTVLoginViewController")
            case .Record:
                currentController = NTVRecordViewController.load("NTVRecordViewController")
            case .Gallery:
                currentController = NTVGalleryViewController.load("NTVGalleryViewController")
            case .Anketa:
                currentController = NTVAnketaViewController.load("NTVAnketaViewController")
            }
            
            containerView.addSubview(currentController!.view)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if NTVAccountManager.isAuthorized {
            controllerType = .Record
        } else {
            controllerType = .Login
        }
        
        NotificationCenter.default
            .rx.notification(Notification.Name(rawValue: notificationNavigationName), object: nil)
            .subscribeNext { [unowned self] (notification) in
                if let index = notification.object {
                    switch index as! Int {
                    case 0:
                        self.controllerType = .Record
                    case 1:
                        self.controllerType = .Gallery
                    default: ()
                    }
                }
        }.addDisposableTo(disposeBag)
        
        NotificationCenter.default
            .rx.notification(Notification.Name(rawValue: notificationLoginSuccess), object: nil)
            .subscribeNext { [unowned self] (notification) in
                self.controllerType = .Record
        }.addDisposableTo(disposeBag)
        
        NotificationCenter.default
            .rx.notification(Notification.Name(rawValue: notificationLogoutSuccess), object: nil)
            .subscribeNext { [unowned self] (notification) in
                let storage = HTTPCookieStorage.shared
                if let cookies = storage.cookies {
                    for cookie in cookies {
                        storage.deleteCookie(cookie)
                    }
                    UserDefaults.standard.synchronize()
                }
                
                self.controllerType = .Login
        }.addDisposableTo(disposeBag)
        
        
        NotificationCenter.default
            .rx.notification(Notification.Name(rawValue: notificationAnketa), object: nil)
            .subscribeNext { [unowned self] (notification) in
                self.controllerType = .Anketa
        }.addDisposableTo(disposeBag)

    }
    
    
}
