//
//  NTVAnketaViewController.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 05/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Cocoa
import RxCocoa
import RxSwift
import CSwiftV

//extension NSResponder {
//    public var rx.firstResponder: AnyObserver<Bool> {
//        return UIBindingObserver(UIElement: self) { control, shouldRespond in
//            shouldRespond ? control.becomeFirstResponder() : control.resignFirstResponder()
//        }.asObserver()
//    }
//}

class NSDateTextField: NSTextField {
    var firstResponder = Variable(false)
    
    override func resignFirstResponder() -> Bool {
        let r = super.resignFirstResponder()
        
        firstResponder.value = r
        
        return r
    }
    
    override func becomeFirstResponder() -> Bool {
        let b = super.becomeFirstResponder()
        
        firstResponder.value = b
        
        return b
    }
}

class NTVAnketaViewController: NTVViewController, MLCalendarViewDelegate, NSTextFieldDelegate, NSComboBoxDataSource, NSComboBoxDelegate {

    fileprivate let disposeBag = DisposeBag()
    
    @IBOutlet weak var buttonSave: KBButton!
    @IBOutlet weak var buttonSignOut: KBButton!
    @IBOutlet weak var view_lastname: NSView!
    @IBOutlet weak var text_lastname: NSTextField!
    
    @IBOutlet weak var view_firstname: NSView!
    @IBOutlet weak var text_firstname: NSTextField!
    
    @IBOutlet weak var view_middlename: NSView!
    @IBOutlet weak var text_middlename: NSTextField!
    
    @IBOutlet weak var view_birthday: NSView!
    @IBOutlet weak var text_birthday: NSDateTextField!
    
    @IBOutlet weak var view_region: NSView!
    @IBOutlet weak var combobox_region: NSComboBox!
    
    @IBOutlet weak var view_phone: NSView!
    @IBOutlet weak var text_phone: NSTextField!
    
    @IBOutlet weak var view_email: NSView!
    @IBOutlet weak var text_email: NSTextField!
    
    var anketa = NTVAnketa()
    var regions: [[String]]?
    var filteredRegions = [[String]]()
    
    var calendarPopover: NSPopover? = nil
    var calendarView: MLCalendarView? = nil
    var dateFormatter: DateFormatter? = nil
    
    override func viewWillAppear() {
        super.viewWillAppear()
        
        NTVWebServiceManager.sharedManager.readAnketa().subscribe(
            onNext: { [unowned self] result in
                self.anketa = result as! NTVAnketa
                
                self.text_firstname.stringValue = self.anketa.firstName ?? ""
                self.text_lastname.stringValue = self.anketa.lastName ?? ""
                self.text_middlename.stringValue = self.anketa.middleName ?? ""
                self.text_email.stringValue = self.anketa.email ?? ""
                self.text_phone.stringValue = self.anketa.phone ?? ""
                self.text_birthday.stringValue = self.anketa.birthDay ?? ""
                self.updateBirthday(self.text_birthday.stringValue)
                
                if let region_id = self.anketa.region_id {
                    let value = self.regions!.filter { $0[0] == region_id.stringValue }[0][1]
                    
                    self.combobox_region.stringValue = value
                }
            }, onError: { [unowned self] error in
                self.showError(error)
            }
        ).addDisposableTo(self.disposeBag)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createCalendarPopover()
        
        view.wantsLayer = true
        view.layer!.backgroundColor = NSColor(deviceRed: 45/255, green:54/255, blue:61/255, alpha:1).cgColor
        buttonSave.setKBButtonType(BButtonTypeNTV)
        buttonSignOut.setKBButtonType(BButtonTypeNTVRed)
        
        view_lastname.wantsLayer = true
        view_lastname.layer!.backgroundColor = NSColor(deviceRed: 40/255, green:48/255, blue:53/255, alpha:1).cgColor
        
        view_firstname.wantsLayer = true
        view_firstname.layer!.backgroundColor = NSColor(deviceRed: 40/255, green:48/255, blue:53/255, alpha:1).cgColor
        
        view_middlename.wantsLayer = true
        view_middlename.layer!.backgroundColor = NSColor(deviceRed: 40/255, green:48/255, blue:53/255, alpha:1).cgColor
        
        view_birthday.wantsLayer = true
        view_birthday.layer!.backgroundColor = NSColor(deviceRed: 40/255, green:48/255, blue:53/255, alpha:1).cgColor
        
        view_region.wantsLayer = true
        view_region.layer!.backgroundColor = NSColor(deviceRed: 40/255, green:48/255, blue:53/255, alpha:1).cgColor
        
        view_phone.wantsLayer = true
        view_phone.layer!.backgroundColor = NSColor(deviceRed: 40/255, green:48/255, blue:53/255, alpha:1).cgColor
        
        view_email.wantsLayer = true
        view_email.layer!.backgroundColor = NSColor(deviceRed: 40/255, green:48/255, blue:53/255, alpha:1).cgColor
        
        combobox_region.completes = true
        combobox_region.usesDataSource = true
        combobox_region.dataSource = self
        combobox_region.delegate = self
        
        do {
            let filename = Bundle.main.path(forResource: "regions", ofType: "csv")
            let content = try String(contentsOfFile:filename!, encoding: String.Encoding.utf8)
            let csv = CSwiftV(with: content, separator: ",", headers: nil)
            regions = csv.rows
        } catch {
            print("error")
        }
        
        buttonSave.rx.tap.subscribeNext { [unowned self] _ in
            
            self.anketa.firstName = self.text_firstname.stringValue
            self.anketa.lastName = self.text_lastname.stringValue
            self.anketa.middleName = self.text_middlename.stringValue
            self.anketa.phone = self.text_phone.stringValue
            self.anketa.email = self.text_email.stringValue
            self.anketa.birthDay = self.text_birthday.stringValue
            
            DJProgressHUD.showStatus("", from: self.view)
            NTVWebServiceManager.sharedManager.writeAnketa(self.anketa).subscribe(
                onNext: { [unowned self] result in
                    print(result)
                    DJProgressHUD.dismiss()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationShowInfo), object: "Успешное сохранение анкеты!")
                    var empty = false
                    repeat {
                        if (self.anketa.firstName == nil || self.anketa.firstName!.isEmpty)
                            && (self.anketa.middleName == nil || self.anketa.middleName!.isEmpty)
                            && (self.anketa.lastName == nil || self.anketa.lastName!.isEmpty) {
                            empty = true
                            break
                        }
                        if self.anketa.phone == nil || self.anketa.phone!.isEmpty {
                            empty = true
                            break
                        }
                    } while (false)
                    
                    let appdelegate = NSApplication.shared().delegate as? AppDelegate
                    if empty {
                        UserDefaults.set_need_fmessage()
                        appdelegate?.startDelayed()
                    } else {
                        UserDefaults.set_need_fmessage(false)
                        appdelegate?.stopDelayed()
                    }
                    
                    if !UserDefaults.not_fmessage() {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationShowFirstMessage), object: nil)
                        UserDefaults.set_not_fmessage()
                    }

                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationNavigationUpdate), object: self.anketa.toTitleButton())

                }, onError: { error in
                    DJProgressHUD.dismiss()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationShowInfo), object: (error as NSError).localizedDescription)
                }
            ).addDisposableTo(self.disposeBag)
        }.addDisposableTo(self.disposeBag)
        
        buttonSignOut.rx.tap.subscribeNext {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationLogoutSuccess), object: nil)
        }.addDisposableTo(disposeBag)

        text_birthday.firstResponder.asObservable().subscribeNext { [unowned self] first in
            if first {
                self.showCalendar(self.text_birthday)
            }
        }.addDisposableTo(self.disposeBag)
        
        text_birthday.rx.text.subscribeNext { [unowned self] value in
            self.updateBirthday(value)
        }.addDisposableTo(self.disposeBag)
    }
    
    fileprivate func updateBirthday(_ string: String) {
        let date = string.toDate()
        self.calendarView?.date = date ?? Date()
        if !string.isEmpty {
            self.anketa.birthDay = string
        }
    }
    
    // MARK: - 
    
    func resultsInComboFor(_ string: String) -> String {
        
        return ""
//        filteredRegions.removeAll()
//        
//        if string.characters.count == 0 || string.isEmpty || string == " " {
//            filteredRegions.appendContentsOf(regions!)
//        } else {
//            for (i, region) in regions!.enumerate() {
//                let searchName = region
//            }
//        }
//    
//        [self.filteredRegions removeAllObjects];
//        
//        if (string.length == 0 || [string isEqualToString:@""] || [string isEqualToString:@" "]) {
//            [self.filteredRegions addObjectsFromArray:self.regions];
//        }
//        else{
//            for (int i = 0; i < self.regions.count; i++) {
//            
//                NSRange searchName  = [self.regions[i] rangeOfString:string options:NSCaseInsensitiveSearch];
//                if (searchName.location != NSNotFound) { [self.filteredRegions addObject:self.regions[i]]; }
//                
//            }
//        }
//        
//        [self.comboBoxRegion reloadData];
//        
//        return self.filteredRegions;
    }
    
    func createCalendarPopover() {
        var myPopover = calendarPopover
        
        if myPopover == nil {
            myPopover = NSPopover()
            calendarView = MLCalendarView()
            calendarView?.delegate = self
            myPopover!.contentViewController = calendarView
            myPopover!.appearance = NSAppearance(named: NSAppearanceNameAqua)
            myPopover!.animates = true
            myPopover!.behavior = .transient
        }
        
        calendarPopover = myPopover
    }

    func showCalendar(_ sender: NSView) {
        let cellRect = sender.bounds
        
        if calendarPopover!.isShown == false {
            calendarPopover!.show(relativeTo: cellRect, of: sender, preferredEdge: NSRectEdge.maxY)
        }
    }
    
    func didSelect(_ selectedDate: Date!) {
        text_birthday.stringValue = selectedDate.toString("dd.MM.yyyy")
    }
    
    // MARK: - NSComboBoxDataSource
    
    func numberOfItems(in aComboBox: NSComboBox) -> Int {
        return regions?.count ?? 0
    }
    
    func comboBox(_ aComboBox: NSComboBox, objectValueForItemAt index: Int) -> Any? {
        return regions![index][1]
    }
    
    func comboBox(_ aComboBox: NSComboBox, indexOfItemWithStringValue string: String) -> Int {
        return 0//regions!.indexOf( {$0 == string })
    }
    
    func comboBoxWillPopUp(_ notification: Notification) {
//        if let searchStr = (notification.object? as String).string {
//            //self.filterDataArray(searchStr)
//        }
    }
    
    func comboBoxSelectionDidChange(_ notification: Notification) {
        let indexOfSelectedIndex = combobox_region.indexOfSelectedItem
        let region = Int(regions![indexOfSelectedIndex][0])
        
        anketa.region_id = NSNumber(value: region! as Int)
    }
    
    func control(_ control: NSControl, textView: NSTextView, doCommandBy commandSelector: Selector) -> Bool {
        if control == text_birthday {
            showCalendar(text_birthday)
            return false
        }
        
        let combobox = control as! NSComboBox
        
        if combobox == combobox_region && (commandSelector == Selector("insertNewLine:"))
            || commandSelector == #selector(NSResponder.insertBacktab(_:)) || commandSelector == #selector(NSResponder.insertTab(_:)) {
            //if resultsInComboFor(combobox.stringValue).characters .characters.coun
        }
        
        return false
    }
    
    func comboBox(_ aComboBox: NSComboBox, completedString string: String) -> String? {
        return ""
    }
}
