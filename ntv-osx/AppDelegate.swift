//
//  AppDelegate.swift
//  ntv-osx
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 15/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Cocoa
import RxCocoa
import RxSwift

let notificationShowError = "notificationShowError"
let notificationShowWarning = "notificationShowWarning"
let notificationShowInfo = "notificationShowInfo"
let notificationShowFirstMessage = "notificationShowFirstMessage"

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    fileprivate let disposeBag = DisposeBag()
    var task: Task? = nil
    fileprivate var messageShown = false
    
    @IBAction func removeAllFiles(_ sender: AnyObject?) {
        NTVLocalFilesManager.sharedManager.removeAllFiles()
    }
    
    func applicationDidBecomeActive(_ notification: Notification) {
//        let storyboard = NSStoryboard(name: "Main", bundle: nil)
//        if let titlebarController = storyboard.instantiateControllerWithIdentifier("NTVTitlebarStatusViewController") as? NTVTitlebarStatusViewController {
//            NSApplication.sharedApplication().mainWindow?.addTitlebarAccessoryViewController(titlebarController)
//            //NSApplication.sharedApplication().mainWindow?.titlebarAppearsTransparent = true
//        }
        
        //DDLog.initLog()
        
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: notificationShowError)).subscribeNext { (notification) in
            if let message = notification.object as? String {
                self.showError(message)
            }
        }.addDisposableTo(disposeBag)
        
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: notificationShowInfo)).subscribeNext { (notification) in
            if let message = notification.object as? String {
                self.showInfo(message)
            }
        }.addDisposableTo(disposeBag)
        
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: notificationShowWarning)).subscribeNext { (notification) in
            if let message = notification.object as? String {
                self.showWarning(message)
            }
        }.addDisposableTo(disposeBag)
        
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: notificationShowFirstMessage)).subscribeNext { (notification) in
            self.showFirstMessage()
        }.addDisposableTo(disposeBag)
    }
    
    func showInfo(_ message: String) {
        showAlert(message, style: .informational)
    }
    
    func showWarning(_ message: String) {
        showAlert(message, style: .warning)
    }
    
    func showError(_ message: String) {
        showAlert(message)
    }
    
    func showAlert(_ message: String, style: NSAlertStyle = .critical) {
        
        if messageShown {
            return
        }
        
        self.messageShown = true
        
        let alert = NSAlert()
        
        var title: String
        
        switch style {
        case .critical:
            title = "Ошибка"
        case .warning:
            title = "Предупреждение"
        case .informational:
            title = "Информация"
        }
        
        alert.messageText = title
        alert.informativeText = message
        alert.alertStyle = style
        
        alert.beginSheetModal(for: NSApplication.shared().windows.first!, completionHandler: { result in
            self.messageShown = false
        })
    }
    
    // MARK: -
    
    func showFirstMessage() {
        let message = "Уважаемый пользователь, для выплаты вам вознаграждения, в случае нашей заинтересованности в вашем видео, пожалуйста заполните правильно анкету о себе, чтобы мы могли связаться с вами."
        
        let alert = NSAlert()
        alert.messageText = "Важная информация!"
        alert.informativeText = message
        alert.alertStyle = .informational
        alert.beginSheetModal(for: NSApplication.shared().windows.first!, completionHandler: { _ in
            self.task = nil
            if UserDefaults.need_fmessage() {
                self.startDelayed()
            } else {
                if let _ = self.task {
                    self.task = nil
                }
            }
        })
    }

// MARK: -
    
    func startDelayed() {
        if task == nil {
            task = delay(900, task: {
            //task = delay(5, task: {
                NotificationCenter.default
                    .post(name: NSNotification.Name(rawValue: notificationShowFirstMessage), object: nil)
            })
        }
    }
    
    func stopDelayed() {
        if let t = task {
            cancel(t)
        }
        
        task = nil
    }

    // MARK: -
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
    
        
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }

}

