//
//  NTVRecordViewController.h
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 30/06/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "NTVViewController.h"

//@import CocoaLumberjack;
//
//extern DDLogLevel const ddLogLevel;

extern NSString * const kNotificationRefreshLocalFiles;
extern NSString * const kNotificationDidStartRecord;
extern NSString * const kNotificationDidStopRecord;
extern NSString * const kNotificationSignOut;
extern NSString * const kNotificationAnketaSent;
extern NSString * const kNotificationSetNavigation;

@class AVCaptureDevice;
@class AVCaptureDeviceFormat;
@class AVCaptureSession;
@class AVFrameRateRange;
@interface NTVRecordViewController : NTVViewController

#pragma mark Device Selection
@property (retain) NSArray *videoDevices;
@property (retain) NSArray *audioDevices;
@property (assign) AVCaptureDevice *selectedVideoDevice;
@property (assign) AVCaptureDevice *selectedAudioDevice;

#pragma mark - Device Properties
@property (assign) AVCaptureDeviceFormat *videoDeviceFormat;
@property (assign) AVCaptureDeviceFormat *audioDeviceFormat;
@property (assign) AVFrameRateRange *frameRateRange;
- (IBAction)lockVideoDeviceForConfiguration:(id)sender;

#pragma mark - Recording
@property (retain) AVCaptureSession *session;
@property (readonly) NSArray *availableSessionPresets;
@property (readonly) BOOL hasRecordingDevice;
@property (assign,getter=isRecording) BOOL recording;


@end
