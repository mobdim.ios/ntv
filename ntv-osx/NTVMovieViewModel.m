//
//  NTVMovieViewModel.m
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 03/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

#import "NTVMovieViewModel.h"
#import "ntv_osx-Swift.h"

@interface NTVMovieViewModel ()

@property (strong, nonatomic) NTVMovieModel *model;

@end

@implementation NTVMovieViewModel

- (instancetype)initWithModel:(NTVMovieModel *)model {
    self = [super init];
    if (!self) return nil;
 
    self.model = model;
    self.selectedIndex = -1;

    return self;
}

- (void)upload {
//    [[[PLPRESTManager sharedManager] canUploadMovieWithName:self.model.m_name description:self.model.m_description] subscribeNext:^(PLPNetworkResult *result) {
//        @strongify(self);
//        
//        print(@"Можно аплоадить: %@", result);
//        
//        self.model.m_id = result.m_id.integerValue;
//        [[[PLPRESTManager sharedManager] uploadMovie:self.model] subscribeNext:^(id _) {
//            @strongify(self);
//            DDLogDebug(@"upload complete");
//            self.model.m_uploaded = YES;
//            if (self.selectedIndex != -1) {
//                [[PLPLocalFilesManager sharedManager] updateLocalMove:self.model atIndex:self.selectedIndex];
//                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRefreshLocalFiles object:nil];
//            }
//            [subscriber sendNext:nil];
//            [subscriber sendCompleted];
//        } error:^(NSError *error) {
//            [subscriber sendError:error];
//            print(@"%@", error);
//            [appdelegate displayError:error];
//        }];
//    } error:^(NSError *error) {
//        [subscriber sendError:error];
//        print(@"%@", error);
//        [appdelegate displayError:error];
//    }];
}

@end
