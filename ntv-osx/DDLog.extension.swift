//
//  DDLog.extension.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 08/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Foundation

extension DDLog {
    
    /**
     Инициализация логов.
     */
    static func initLog() {
        
        setenv("XcodeColors", "YES", 1);
        
        DDLog.add(DDASLLogger.sharedInstance())
        DDLog.add(DDTTYLogger.sharedInstance())
        
        DDTTYLogger.sharedInstance().setForegroundColor(NSColor.white, backgroundColor: NSColor.red, for: .error)
        DDTTYLogger.sharedInstance().setForegroundColor(NSColor(red: 1, green: 0x85/255, blue: 0x1B/255, alpha: 1), backgroundColor: nil, for: .warning)
        DDTTYLogger.sharedInstance().setForegroundColor(NSColor(red: 0x33/255, green: 0x33/255, blue: 1, alpha: 1), backgroundColor: nil, for: .info)
        DDTTYLogger.sharedInstance().setForegroundColor(NSColor(red: 0x44/255, green: 0x44/255, blue: 0x44/255, alpha: 1), backgroundColor: nil, for: .debug)
        DDTTYLogger.sharedInstance().setForegroundColor(NSColor(red: 0xAA/255, green: 0xAA/255, blue: 0xAA/255, alpha: 1), backgroundColor: nil, for: .verbose)
        DDTTYLogger.sharedInstance().colorsEnabled = true
    }
}
