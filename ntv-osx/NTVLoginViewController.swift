//
//  NTVLoginViewController.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 31/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Cocoa
import RxSwift
import RxCocoa

class NTVLoginViewController: NTVViewController, SocialLoginDelegate {
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var loginView: NSView!
    @IBOutlet weak var codeView: NSView!
    
    @IBOutlet weak var buttonSignIn: KBButton!
    @IBOutlet weak var buttonCode: KBButton!
    
    @IBOutlet weak var textPhoneField: NSTextField!
    @IBOutlet weak var textCodeField: NSTextField!
    @IBOutlet weak var labelCode: NSTextField!
    
    @IBOutlet weak var button_fb: NSButton!
    @IBOutlet weak var button_odk: NSButton!
    @IBOutlet weak var button_vk: NSButton!
    @IBOutlet weak var button_tw: NSButton!
    
    var cameraViewController: NTVCameraViewController?
    
    override func viewDidAppear() {
        super.viewDidAppear()
        if !loginView.isHidden {
            textPhoneField.becomeFirstResponder()
        } else {
            textCodeField.becomeFirstResponder()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        viewLogin()
        buttonSignIn.rx.tap.asObservable().subscribeNext { [unowned self] _ in
            
            DJProgressHUD.showStatus("", from: self.view)
            
            NTVWebServiceManager.sharedManager.requestCode(self.textPhoneField.stringValue).subscribe(
                onNext: { [unowned self] response in
                    DJProgressHUD.dismiss()
                    self.viewCode()
                }, onError: { [unowned self] error in
                    print("\(error)")
                    DJProgressHUD.dismiss()
                    self.showError(error)
                }
            ).addDisposableTo(self.disposeBag)
        }.addDisposableTo(self.disposeBag)
        
        buttonCode.rx.tap.asObservable().subscribeNext { [unowned self] _ in
            
            DJProgressHUD.showStatus("", from: self.view)
            
            NTVWebServiceManager.sharedManager.login(self.textCodeField.stringValue).subscribe(
                onNext: { response in
                    DJProgressHUD.dismiss()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationLoginSuccess), object: nil)
                }, onError: { [unowned self] error in
                    DJProgressHUD.dismiss()
                    print("\(error)")
                    self.showError(error)
                }
            ).addDisposableTo(self.disposeBag)
            
        }.addDisposableTo(self.disposeBag)
        
        let color = NSColor.lightGray
        let attrs = [NSForegroundColorAttributeName: color]
        let placeHolderStr = NSAttributedString(string: "Введите ваш мобильный номер или E-mail", attributes: attrs)
        
        textPhoneField.placeholderAttributedString = placeHolderStr
        
        buttonSignIn.setKBButtonType(BButtonTypeNTV)
        buttonCode.setKBButtonType(BButtonTypeNTV)
        
        #if DEBUG
//            textPhoneField.stringValue = "71978200600"
//            textCodeField.stringValue = "1234"
            textPhoneField.stringValue = "+79037306230"
        #endif
        
        button_odk.rx.tap.subscribeNext { [unowned self] _ in
            SocialLogin.odkLogin(self, clientOnly: true)
        }.addDisposableTo(self.disposeBag)
        
        button_vk.rx.tap.subscribeNext { [unowned self] _ in
            SocialLogin.vkLogin(self, clientOnly: false)
        }.addDisposableTo(self.disposeBag)
        
        button_fb.rx.tap.subscribeNext { [unowned self] _ in
            SocialLogin.fbLogin(self)
        }.addDisposableTo(self.disposeBag)
        
        button_tw.rx.tap.subscribeNext { [unowned self] _ in
            SocialLogin.twLogin(self, clientOnly: false)
        }.addDisposableTo(self.disposeBag)
    }

    
    fileprivate func viewLogin() {
        loginView.isHidden = false
        codeView.isHidden = true
        textPhoneField.becomeFirstResponder()
    }
    
    fileprivate func viewCode() {
        
        if textPhoneField.stringValue.isEmail() {
            labelCode.stringValue = "Дождитесь электронного письма с кодом и введите его в поле ниже:"
        } else {
            labelCode.stringValue = "Дождитесь СМС с кодом и введите его в поле ниже:"
        }
        
        loginView.isHidden = true
        codeView.isHidden = false
        textCodeField.becomeFirstResponder()
    }
    
    // MARK: - SocialLoginDelegate
    
    func socialLoginCompleted(_ successful: Bool, credentials: [AnyHashable: Any]?, loginType: socialLoginType) {

        
        if successful == false {
           
            NotificationCenter.default.post(name: Notification.Name(rawValue: notificationShowError), object: "Ошибка регистрации через соцсеть!")
            
            return
        }
        
        
//        DDLogDebug("socialLoginCompleted: \(credentials)")

        DJProgressHUD.showStatus("", from: self.view)
        
        switch loginType {
        case kLoginFacebook:
            let token = credentials!["token"] as! String
            NTVWebServiceManager.sharedManager.loginViaFacebook(token).subscribe(
                onNext: { sid in
                    DJProgressHUD.dismiss()
                    print("Вошли через fb: \(sid)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationLoginSuccess), object: nil)
                }, onError: { error in
                    DJProgressHUD.dismiss()
                    print("\((error as NSError).localizedDescription)")
                    self.showError(error)
                }
                ).addDisposableTo(self.disposeBag)
        case kLoginODK:
            let token = credentials!["token"] as! String
            NTVWebServiceManager.sharedManager.loginViaOdnoklassniki(token).subscribe(
                onNext: { sid in
                    DJProgressHUD.dismiss()
                    print("Вошли через odk: \(sid)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationLoginSuccess), object: nil)
                }, onError: { error in
                    DJProgressHUD.dismiss()
                    print("\((error as NSError).localizedDescription)")
                    self.showError(error)
                }
            ).addDisposableTo(self.disposeBag)
            
        case kLoginVK:
            let token = credentials!["token"] as! String
            
            NTVWebServiceManager.sharedManager.validateLoginVK(token).subscribe(
                onNext: { [unowned self] access_token in
                    
                    NTVWebServiceManager.sharedManager.loginViaVkontakte(access_token as! String).subscribe(
                        onNext: { sid in
                            DJProgressHUD.dismiss()
                            print("Вошли через vk: \(sid)")
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationLoginSuccess), object: nil)
                        }, onError: { [unowned self] error in
                            DJProgressHUD.dismiss()
                            print("\((error as NSError).localizedDescription)")
                            self.showError(error)
                        }
                    ).addDisposableTo(self.disposeBag)
                    
                }, onError: { [unowned self] error in
                    DJProgressHUD.dismiss()
                    print("\((error as NSError).localizedDescription)")
                    self.showError(error)
                }
            ).addDisposableTo(disposeBag)
            
        case kLoginTwitter:
            let token = credentials!["token"] as! String
            NTVWebServiceManager.sharedManager.loginViaTwitter(token, secret: "").subscribe(
                onNext: { sid in
                    DJProgressHUD.dismiss()
                    print("Вошли через tw: \(sid)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationLoginSuccess), object: nil)
                }, onError: { error in
                    DJProgressHUD.dismiss()
                    print("\((error as NSError).localizedDescription)")
                    self.showError(error)
                }
                ).addDisposableTo(self.disposeBag)
            
        default:
            DJProgressHUD.dismiss()
        }
    }
    
}
