//
//  NTVViewController.m
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 30/06/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

#import "NTVViewController.h"

@interface NTVViewController ()

@end

@implementation NTVViewController

+ (instancetype)loadViewController:(NSString *)storyboardId {
    NSStoryboard *storyboard = [NSStoryboard storyboardWithName:@"Main" bundle:nil];
    
    return [storyboard instantiateControllerWithIdentifier:storyboardId];
}
@end
