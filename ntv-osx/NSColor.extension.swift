//
//  NSColor.extension.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 12/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Foundation

extension NSColor {
    static func bgDark() -> NSColor {
        return NSColor(deviceRed: 40/255, green:48/255, blue:53/255, alpha:1)
    }
    
    static func bg() -> NSColor {
        return NSColor(deviceRed: 45/255, green:54/255, blue:61/255, alpha:1)
    }
}