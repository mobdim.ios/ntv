//
//  NSDate.extension.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 08/04/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Foundation

extension String {
    func toDate(_ formatString: String = "dd.MM.yyyy") -> Date? {
        let format = DateFormatter()
        format.dateFormat = formatString
        let result = format.date(from: self)
        
        return result
    }
}

extension Date {
    
    func toStringDateTime() -> String {
        let format = DateFormatter()
        format.dateFormat = "dd.MM.yyyy HH:mm"
        let result = format.string(from: self)
        
        return result
    }
    
    func toStringTime() -> String {
        let format = DateFormatter()
        format.dateFormat = "HH.mm"
        let result = format.string(from: self)
        
        return result
    }
    
    func toString(_ formatString: String = "dd.MM.yyyy") -> String {
        
        let format = DateFormatter()
        format.dateFormat = formatString
        let result = format.string(from: self)
        
        return result
    }
    
    static func toDate(_ string: String) -> Date? {
        
        let format = DateFormatter()
        format.dateFormat = "dd.MM.yyyy"
        let result = format.date(from: string)
        
        return result
    }
}
