
//
//  AppDelegate.swift
//  ntv-ios
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 15/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import ok_ios_sdk
import VK_ios_sdk
import RxCocoa
import RxSwift

let notification_set_first_message = "notification_set_first_message"
let notification_gpp_token = "notification_gpp_token"

enum SocialLoginType {
    case vk
    case ok
    case gpp
}

//com.ntv.stringer-ios
//ru.imteleport.ntv-ios

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    fileprivate let disposeBag = DisposeBag()

    var socialLogin: SocialLoginType = .vk
    
    var push_id = ""

    var task: Task? = nil
    var timer: Timer? = nil
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: notification_set_first_message)).subscribeNext { _ in
            
            DispatchQueue.main.async(execute: {
                if let _ = self.task {
                    self.stopDelayed()
                }
                let vc = UIApplication.shared.windows.first!.rootViewController
                //let vc = self.window?.rootViewController
                vc?.showFirstMessage { _ in
                    self.task = nil
                    if UserDefaults.need_fmessage() {
                        self.startDelayed()
                    } else {
                        if let _ = self.task {
                            self.task = nil
                        }
                    }
                } 
            })
        }.addDisposableTo(disposeBag)
        
        
        showLoginScroll()
        //showLogin()
        //showCamera()
        
//        let gallery = NTVGalleryViewController()
//        let nav = NTVGalleryNavigationController(rootViewController: gallery)
//        self.window?.rootViewController = nav

//        let save = NTVSaveVideoViewController()
//        let nav = NTVNavigationSaveViewController(rootViewController: save)
//        self.window?.rootViewController = nav
//        save.index = 0

//        let anketa = NTVAnketaViewController()
//        let nav = NTVNavigationSaveViewController(rootViewController: anketa)
//        self.window?.rootViewController = nav        
        
//        showTestTable()

        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)

        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()

        
        
        // Initialize sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        GIDSignIn.sharedInstance().delegate = self
        
        
        
        return true
    }
    
    // MARK: -
    
    func startDelayed() {
//        if timer == nil {
//            timer = NSTimer(timeInterval: 10 * 1000, target: self, selector: @selector, userInfo: <#T##AnyObject?#>, repeats: <#T##Bool#>)
//        }
        
        
        if task == nil {
           task = delay(900, task: {
                NotificationCenter.default
                    .post(name: Notification.Name(rawValue: notification_set_first_message), object: nil)
            })
        } else {
            stopDelayed()
        }
    }
    
    func stopDelayed() {
        if let t = task {
            cancel(t)
        }
        
        task = nil
    }
    
    func timerTick() {
        
    }
    
    func showLoginScroll() {
        let loginViewController = NTVLoginViewControllerScroll()
        let nav = NTVLoginNavigationController(rootViewController: loginViewController)
        nav.isNavigationBarHidden = true
        self.window?.rootViewController = nav
    }
    
    func showLogin() {
        showLoginScroll()
//        let loginViewController = NTVLoginViewController()
//        let nav = NTVLoginNavigationController(rootViewController: loginViewController)
//        nav.navigationBarHidden = true
//        self.window?.rootViewController = nav
    }
        
    func showCamera() {
        let cameraViewController = NTVCameraViewController()
        self.window?.rootViewController = cameraViewController
        if UserDefaults.need_fmessage() {
            NotificationCenter.default.post(name: Notification.Name(rawValue: notification_set_first_message), object: nil)
        }
    }
    
    func showTestTable() {
        let testViewController = TestTableViewController()
        self.window?.rootViewController = testViewController
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // ios8
    func application(_ app: UIApplication, openURL url: URL, sourceApplication: String) -> Bool {
//        [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
        if socialLogin == .vk {
            VKSdk.processOpen(url, fromApplication: sourceApplication)
        }
        if socialLogin == .ok {
            OKSDK.open(url)
        }
        
        return true

    }
    
    // ios9
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
//        [[FBSDKApplicationDelegate sharedInstance] application:app openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:nil];
//        [VKSdk processOpenURL:url fromApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]];
        //VKSdk.processOpenURL(url, fromApplication: options["UIApplicationOpenURLOptionsSourceApplicationKey"] as! String)
        if #available(iOS 9.0, *) {
            if socialLogin == .vk {
                VKSdk.processOpen(url, fromApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String)
            }
        } else {
            // Fallback on earlier versions
        }
        if socialLogin == .ok {
            OKSDK.open(url)
        }
        
        if socialLogin == .gpp {
            if #available(iOS 9.0, *) {
                return GIDSignIn.sharedInstance().handle(url,
                                                            sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                            annotation: options[UIApplicationOpenURLOptionsKey.annotation])
            } else {
                // Fallback on earlier versions
            }
        }
        
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if #available(iOS 9.0, *) {
            if socialLogin == .vk {
                VKSdk.processOpen(url, fromApplication: UIApplicationOpenURLOptionsKey.sourceApplication.rawValue)
            }
        } else {
            if socialLogin == .vk {
                VKSdk.processOpen(url, fromApplication: sourceApplication)
            }
        }
        
        if socialLogin == .gpp {
            return GIDSignIn.sharedInstance().handle(url,
                                                        sourceApplication: sourceApplication,
                                                        annotation: annotation)
        }
        
        return true
    }
    
    // MARK: - Push notification

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("device token: \(deviceToken)")
        push_id = deviceToken.hexString() as String
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("device token: \(error)")
    }

    
}

extension AppDelegate: GIDSignInDelegate {
    @objc(signIn:didSignInForUser:withError:) func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            //            let userId = user.userID                  // For client-side use only!
            //            let idToken = user.authentication.idToken // Safe to send to the server
            //            let fullName = user.profile.name
            //            let givenName = user.profile.givenName
            //            let familyName = user.profile.familyName
            //            let email = user.profile.email
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: notification_gpp_token), object: user.authentication.accessToken);
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
}

extension Data {
    func hexString() -> NSString {
        let str = NSMutableString()
        let bytes = UnsafeBufferPointer<UInt8>(start: (self as NSData).bytes.bindMemory(to: UInt8.self, capacity: self.count), count:self.count)
        for byte in bytes {
            str.appendFormat("%02hhx", byte)
        }
        return str
    }
}
