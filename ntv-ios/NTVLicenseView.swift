//
//  NTVLicenseView.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 04/05/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import UIKit
import SnapKit

class NTVLicenseView: UIView {

    let textView = UITextView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setup() {
        backgroundColor = UIColor.backgroundColor()

        let bundle = Bundle.main
        let path = bundle.path(forResource: "license", ofType: "txt")
        let content = try! String(contentsOfFile: path!, encoding: String.Encoding.utf8)
        
        addSubview(textView)
        textView.text = content
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.backgroundColor = UIColor.clear
        textView.textColor = UIColor.lightGray
        textView.isEditable = false
        textView.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(self)
            make.left.equalTo(self).offset(40)
            make.right.equalTo(self).offset(-20)
        }
        
    }

}
