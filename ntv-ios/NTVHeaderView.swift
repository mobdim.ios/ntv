//
//  NTVHeaderView.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 30/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit

class NTVHeaderTitleView: UIView {
    
    static let height: CGFloat = 160
    
    fileprivate let topTitle: String
    fileprivate let downTitle: String
    fileprivate let countVisible: Bool
    let countLabel = UILabel()
    
    init(frame: CGRect, topTitle: String, downTitle: String, countVisible: Bool = false) {
        self.topTitle = topTitle
        self.downTitle = downTitle
        self.countVisible = countVisible
        super.init(frame: frame)
        
        prepare_view()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func prepare_view() {
        self.backgroundColor = UIColor.clear
        let titleLabel1 = UILabel()
        titleLabel1.textColor = UIColor.foregroundColor()
        titleLabel1.text = topTitle
        titleLabel1.font = UIFont.systemFont(ofSize: 25)
        addSubview(titleLabel1)
        titleLabel1.snp.makeConstraints { make in
            make.top.equalTo(70)
            make.left.equalTo(40)
            make.right.equalTo(-20)
            make.height.equalTo(25)
        }
        
        let titleLabel2 = UILabel()
        titleLabel2.textColor = UIColor.backgroundColor().lighter()
        titleLabel2.text = downTitle
        titleLabel2.font = UIFont.systemFont(ofSize: 25)
        addSubview(titleLabel2)
        titleLabel2.snp.makeConstraints { make in
            make.top.equalTo(titleLabel1.snp.bottom).offset(5)
            make.left.equalTo(40)
            make.right.equalTo(-20)
            make.height.equalTo(26)
        }
        
        if countVisible {
            countLabel.font = UIFont.systemFont(ofSize: 70)
            countLabel.textColor = titleLabel2.textColor
            countLabel.text = "0"
            countLabel.textAlignment = .center
            addSubview(countLabel)
            countLabel.snp.makeConstraints{ make in
                make.top.equalTo(titleLabel1).offset(-10)
                make.right.equalTo(self).offset(-20)
                make.height.equalTo(100)
                make.width.equalTo(100)
            }
        }
    }

}
