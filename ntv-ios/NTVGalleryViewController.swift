//
//  NTVGalleryViewController.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 31/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit
import Eureka
import SnapKit
import RxSwift
import RxCocoa
import MobileCoreServices

extension Int {
    func format(_ f: String) -> String {
        return String(format: "%\(f)d", self)
    }
}

final class VideoCell: Cell<Bool>, CellType {
    
    let disposeBag = DisposeBag()
    
    let thumbImage = UIImageView()
    let buttonUpload = UIButton()
    let labelName = UILabel()
    let labelDuration = UILabel()
    let labelDate = UILabel()
    let labelStatus = UILabel()
    var index = -1
    required internal init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    override func didSelect() {
//        super.didSelect()
//        
//        let selectView = UIView()
//        selectView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3)
//        thumbImage.insertSubview(selectView, atIndex: 0)
//        selectView.snp.makeConstraints { make in
//            make.top.left.right.bottom.equalTo(self.thumbImage)
//        }
//    }
    
    override func update() {
        super.update()
        
//        thumbImage.userInteractionEnabled = true
//        let tapGesture = UITapGestureRecognizer()
//        thumbImage.addGestureRecognizer(tapGesture)
//        tapGesture.rx.event.subscribeNext { gesture in
//            print("tap")
//        }.addDisposableTo(self.disposeBag)
        
        thumbImage.backgroundColor = UIColor.clear
        thumbImage.contentMode = .scaleAspectFit
        addSubview(thumbImage)
        thumbImage.snp.remakeConstraints { make in
            make.top.left.right.equalTo(self)
            make.height.equalTo(self).offset(-60)
        }
        let playImage = UIImageView(image: UIImage(named: "button_play"))
        thumbImage.addSubview(playImage)
        playImage.snp.remakeConstraints { make in
            make.height.width.equalTo(100)
            make.center.equalTo(thumbImage)
        }
        
//        buttonUpload.setImage(UIImage(named: "button_upload"), forState: .Normal)
//        addSubview(buttonUpload)
//        buttonUpload.snp.remakeConstraints { make in
//            make.right.equalTo(self).offset(-20)
//            make.bottom.equalTo(self).offset(-5)
//            make.width.height.equalTo(24)
//        }
        
        labelName.textColor = UIColor.white
        labelName.font = UIFont.systemFont(ofSize: 12)
        addSubview(labelName)
        labelName.snp.remakeConstraints { make in
            make.left.equalTo(20)
            make.top.equalTo(thumbImage.snp.bottom).offset(10)
            make.width.equalTo(160)
            make.height.equalTo(14)
        }
        
        labelStatus.font = UIFont.systemFont(ofSize: 10)
        addSubview(labelStatus)
        labelStatus.textColor = UIColor.white
        labelStatus.snp.remakeConstraints { (make) in
            make.top.equalTo(labelName.snp.bottom).offset(10)
            make.left.equalTo(labelName)
            make.width.equalTo(labelStatus)
            make.height.equalTo(12)
        }
        
        labelDuration.textColor = UIColor.foregroundColor()
        labelDuration.font = UIFont.systemFont(ofSize: 10)
        addSubview(labelDuration)
        labelDuration.snp.remakeConstraints { make in
            make.top.equalTo(labelName.snp.bottom).offset(10)
            make.left.equalTo(labelStatus.snp.right).offset(10)
            make.width.equalTo(100)
            make.height.equalTo(12)
        }
        
        labelDate.textColor = labelDuration.textColor
        labelDate.font = labelDuration.font
        addSubview(labelDate)
        labelDate.snp.remakeConstraints { make in
            make.top.equalTo(labelDuration)
            make.left.equalTo(labelDuration.snp.right).offset(10)
            make.width.equalTo(100)
            make.height.equalTo(labelDuration)
        }
    }
}

class _VideoRow: Row<VideoCell> {
    
    required public init(tag: String?) {
        super.init(tag: tag)
        displayValueFor = nil
    }
}


final class VideoRow:_VideoRow, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
    }
}

class NTVGalleryViewController: FormViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    let disposeBag = DisposeBag()
    var statuses = [Int: String]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        NTVLocalFilesManager.sharedManager.refreshLocal()
        let sid = UserDefaults.standard.object(forKey: "sid") as? String
        NTVWebServiceManager.sharedManager.getFiles(sid!).subscribeNext { statuses in
            self.statuses = statuses as! [Int: String]
            self.fillTable()
        }.addDisposableTo(self.disposeBag)
        fillTable()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        //NTVLocalFilesManager.sharedManager!.refreshLocal()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(clickClose))
        
        tableView?.separatorStyle = .none
        tableView?.backgroundColor = UIColor.backgroundColorDark()
    }
    
    fileprivate func fillTable() {
        
        form.removeAll()
        self.view.subviews.forEach { (subview) in
            if subview is UIButton || subview is UILabel {
                subview.removeFromSuperview()
            }
        }
        
        let section = Section()
        
        let topView = UIView()
        topView.backgroundColor = UIColor.backgroundColorDark(alpha: 0.5)
        view.addSubview(topView)
        topView.snp.makeConstraints { make in
            make.left.right.top.equalTo(self.view)
            make.height.equalTo(64)
        }
        
        let backButton = UIButton.backButton()
        topView.addSubview(backButton)
        backButton.snp.remakeConstraints{ make in
            make.top.equalTo(30)
            make.left.equalTo(10)
            make.width.equalTo(100)
            make.height.equalTo(20)
        }
        backButton.rx.tap.subscribeNext{ _ in
            self.clickClose()
        }.addDisposableTo(self.disposeBag)
        
        let buttonGallery = UIButton(type: .custom)
        topView.addSubview(buttonGallery)
        buttonGallery.snp.remakeConstraints { (make) in
            make.top.height.equalTo(backButton)
            make.width.equalTo(150)
            make.right.equalTo(topView).offset(-30)
        }
        buttonGallery.setTitleColor(UIColor(red: 39/255, green: 201/255, blue: 65/255, alpha: 1), for: UIControlState())
        buttonGallery.setImage(UIImage(named: "plus")!, for: UIControlState())
        buttonGallery.setTitle("   Добавить", for: UIControlState())
        buttonGallery.contentHorizontalAlignment = .right
        buttonGallery.rx.tap.subscribeNext {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
                imagePicker.mediaTypes = [kUTTypeMovie as String]
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }.addDisposableTo(self.disposeBag)
        
        var header = HeaderFooterView<UIView>(HeaderFooterProvider.class)
        header.onSetupView = { (view, _) -> () in
            view.backgroundColor = UIColor.backgroundColor()
            
            let headerView = NTVHeaderTitleView(frame: CGRect.zero, topTitle: "Галерея", downTitle: "видео", countVisible: true)
            view.addSubview(headerView)
            headerView.snp.makeConstraints { make in
                make.left.top.right.equalTo(view)
                make.height.equalTo(NTVHeaderTitleView.height)
            }
            headerView.countLabel.text = "\(NTVLocalFilesManager.sharedManager.movies.count)"
            
        }
        header.height = { NTVHeaderTitleView.height + 20}
        section.header = header
        
        if NTVLocalFilesManager.sharedManager.movies.count == 0 {
            let addButton = UIButton()
            addButton.setImage(UIImage(named: "button_add"), for: UIControlState())
            self.view.addSubview(addButton)
            addButton.snp.remakeConstraints { make in
                make.center.equalTo(self.view)
                make.width.height.equalTo(100)
            }
            addButton.rx.tap.subscribeNext { _ in
                self.clickClose()
            }.addDisposableTo(self.disposeBag)
            
            let labelBottom = UILabel()
            labelBottom.font = UIFont.systemFont(ofSize: 25)
            labelBottom.textColor = UIColor.backgroundColor().lighter()
            labelBottom.numberOfLines = 2
            self.view.addSubview(labelBottom)
            labelBottom.snp.remakeConstraints { make in
                make.left.equalTo(self.view).offset(40)
                make.bottom.equalTo(self.view).offset(-20)
                make.right.equalTo(self.view)
                make.height.equalTo(140)
            }
            labelBottom.text = "Снимите своё\nпервое видео!"
        } else {
            for (index, movie) in NTVLocalFilesManager.sharedManager.movies.enumerated() {
                section <<< VideoRow() {
                    $0.cell.height = { 260 }
                    $0.cell.selectionStyle = .none
                    $0.cell.index = index
                    }.onCellSelection { (cell, row) in
                        let saveViewController = NTVSaveVideoViewController()
                        saveViewController.pushed = true
                        self.navigationController?.pushViewController(saveViewController, animated: true)
                        saveViewController.index = ((row.indexPath as NSIndexPath?)?.row)!
                    }.cellUpdate { (cell, row) in
                        cell.backgroundColor = UIColor.backgroundColorDark()
                        
                        let movie_item = NTVLocalFilesManager.sharedManager.movies[cell.index]
                        
                        if let status = self.statuses[Int(movie_item.id)] {
                            cell.labelStatus.text = status;
                        } else {
                            cell.labelStatus.text = ""
                        }
                        
                        cell.labelStatus.sizeToFit()
                        
                        if let date = movie_item.date {
                            //cell.labelDate.text = date.toString()
                            cell.labelDuration.text = date.toStringDateTime()
                        }
                        cell.labelName.text = movie_item.name
                        movie_item.thumbnailImageWithCompletion { (image, duration) in
                            let time = Int(duration)
                            let (_,m,s) = secondsToHoursMinutesSeconds(time)
                            cell.thumbImage.image = image
                            //cell.labelDuration.text = "\(m.format("02")):\(s.format("02"))"
                        }
                }
            }
        }
        
        form +++ section
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func clickClose() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let filename = info[UIImagePickerControllerMediaURL] {
            let movie = NTVMovieModel()
            let destinationFilename = ((filename as! NSURL).absoluteString)?.replacingOccurrences(of: "file://", with: "")
            
            movie.name = ""
            movie.descript = ""
            movie.filename = NSString(string:destinationFilename!).lastPathComponent
            movie.fullFileNameStorage = NTVLocalFilesManager.sharedManager.fullPathByFile(movie.filename!)
            movie.date = Date()
            
            let fileManager = FileManager.default
            
            do {
                try fileManager.copyItem(atPath: destinationFilename!, toPath: movie.fullFileNameStorage!)
                NTVLocalFilesManager.sharedManager.addLocalMovie(movie)
                NTVLocalFilesManager.sharedManager.refreshLocal()
            }
            catch let error as NSError {
                print("Ошибка при копировании: \(error)")
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
