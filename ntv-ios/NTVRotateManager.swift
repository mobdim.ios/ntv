//
//  NTVRotateManager.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 30/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum NotifyOrientation: String {
    case kNotifyOrientationPortrait = "NotifyOrientationPortrait"
    case kNotifyOrientationUpsideDown = "NotifyOrientationUpsideDown"
    case kNotifyOrientationLandscapeLeft = "NotifyOrientationLandscapeLeft"
    case kNotifyOrientationLandscapeRight = "NotifyOrientationLandscapeRight"
}

class NTVRotateManager: NSObject {
    let disposeBag = DisposeBag()
    
    static let sharedManager = NTVRotateManager()
    
    fileprivate var _lastOrientation: UIDeviceOrientation = .portrait
    
    var currentOrientation: UIDeviceOrientation {
        get {
            let devor = UIDevice.current.orientation
            
            var returnOrientation = UIDeviceOrientation.portrait
            
            switch devor {
            case .portrait, .portraitUpsideDown, .landscapeLeft, .landscapeRight:
                returnOrientation = devor
            default: ()
            }
            
            return returnOrientation
        }
    }
    
    override init() {
        super.init()
        
        _lastOrientation = self.currentOrientation
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
        NotificationCenter.default.rx.notification(NSNotification.Name.UIDeviceOrientationDidChange).subscribeNext { notification in
            
            print("change orientation")
            
            let currentOrientation = self.currentOrientation
            if self._lastOrientation != currentOrientation {
                if currentOrientation == .portrait {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: NotifyOrientation.kNotifyOrientationPortrait.rawValue), object: nil)
                    print("UIDeviceOrientationPortrait")
                }
                if currentOrientation == .portraitUpsideDown {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: NotifyOrientation.kNotifyOrientationUpsideDown.rawValue), object: nil)
                    print("UIDeviceOrientationPortraitUpsideDown")
                }
                if currentOrientation == .landscapeLeft {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: NotifyOrientation.kNotifyOrientationLandscapeLeft.rawValue), object: nil)
                    print("UIDeviceOrientationLandscapeLeft")
                }
                if currentOrientation == .landscapeRight {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: NotifyOrientation.kNotifyOrientationLandscapeRight.rawValue), object: nil)
                    print("UIDeviceOrientationLandscapeRight")
                }

                self._lastOrientation = currentOrientation;
            }
            
        }.addDisposableTo(self.disposeBag)
    }
}
