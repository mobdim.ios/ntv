//
//  ntv-ios-Bridging-Header.h
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 04/04/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

#ifndef ntv_ios_Bridging_Header_h
#define ntv_ios_Bridging_Header_h

#import <SocialLogin/SocialLogin.h>
#import <SocialLogin/OAuth.h>
#import <Google/SignIn.h>

#endif /* ntv_ios_Bridging_Header_h */
