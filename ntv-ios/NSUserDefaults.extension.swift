//
//  NSUserDefaults.extension.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 18/05/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Foundation

let key_license = "license"
let key_anketa = "anketa"
let key_not_fmessage = "not_fmessage"
let key_need_fmessage = "need_fmessage"

extension UserDefaults {
    static func get_bool(_ key: String) -> Bool {
        let udef = UserDefaults.standard
        if let value = udef.object(forKey: key) {
            return (value as AnyObject).boolValue
        }
        return false
    }
    static func set_bool(_ key: String, value: Bool = true) {
        let udef = UserDefaults.standard
        udef.set(value, forKey: key)
        udef.synchronize()
    }

// license
    static func license() -> Bool {
        return get_bool(key_license)
    }
    static func set_license() {
        set_bool(key_license)
    }
// anketa
    static func anketa() -> Bool {
        return get_bool(key_anketa)
    }
    static func set_anketa() {
        set_bool(key_anketa)
    }
// not_fmessage
    static func not_fmessage() -> Bool {
        return get_bool(key_not_fmessage)
    }
    static func set_not_fmessage() {
        set_bool(key_not_fmessage)
    }
// need_fmessage
    static func need_fmessage() -> Bool {
        return get_bool(key_need_fmessage)
    }
    static func set_need_fmessage(_ value: Bool = true) {
        set_bool(key_need_fmessage, value: value)
    }
}
