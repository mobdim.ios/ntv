//
//  UIColor.extension.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 21/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit

extension UIColor {
    static func backgroundColor() -> UIColor {
        return UIColor(red: 0x25/0xFF, green: 0x2C/0xFF, blue: 0x32/0xFF, alpha: 1)
    }
    
    static func backgroundColorDark(alpha: CGFloat = 1) -> UIColor {
        return UIColor(red: 0x23/0xFF, green: 0x29/0xFF, blue: 0x2C/0xFF, alpha: alpha)
    }
    
    static func foregroundColor() -> UIColor {
        return UIColor(red: 0x82/0xFF, green: 0x92/0xFF, blue: 0x9E/0xFF, alpha: 1)
    }
    
    static func buttonTitleColor() -> UIColor {
        return UIColor(red: 0x33/0xFF, green: 0xCC/0xFF, blue: 0x33/0xFF, alpha: 1)
    }
    static func buttonDeleteTitleColor() -> UIColor {
        return UIColor(red: 0xF2/0xFF, green: 0x48/0xFF, blue: 0x48/0xFF, alpha: 1)
    }
}
