//
//  NTVLoginViewController.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 21/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit
import Eureka
import SnapKit
import RxSwift
import RxCocoa
import ok_ios_sdk
import VK_ios_sdk

class NTVLoginViewController: FormViewController, SocialLoginDelegate, VKSdkDelegate, VKSdkUIDelegate {

    let disposeBag = DisposeBag()
    
    var section: Section?
    @IBOutlet weak var textPhoneField: UITextField!
    var socialView: UIView?
    
    lazy var _view: NTVView = {
        self.view as! NTVView
    }()
    
    override func loadView() {
        super.loadView()
        
        view = NTVView(frame: UIScreen.main.bounds)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let udef = UserDefaults.standard
        if let sid = udef.object(forKey: "sid") as? String {
            NTVWebServiceManager.sharedManager.autoLogin(sid).subscribe(
                onNext: { _ in
                    (UIApplication.shared.delegate as! AppDelegate).showCamera()
                }, onError: { error in
                    print("\(error)")
                }
                ).addDisposableTo(self.disposeBag)
            
        }
        
        let settings = OKSDKInitSettings()
        settings.appKey = Bundle.main.infoDictionary!["ODKSecretKey"] as! String
        settings.appId = Bundle.main.infoDictionary!["ODKAppKey"] as! String
        settings.controllerHandler = {
            return self
        }
        OKSDK.initWith(settings)
        
        let vkAppId = Bundle.main.infoDictionary!["VKAppKey"] as! String
        let sdkInstance = VKSdk.initialize(withAppId: vkAppId)
        sdkInstance?.register(self)
        sdkInstance?.uiDelegate = self
        
        self.tableView?.backgroundColor = UIColor.backgroundColor()
        self.tableView?.alwaysBounceVertical = false
        self.tableView?.bounces = false
        
        self.section = Section () {
            var header = HeaderFooterView<UIView>(HeaderFooterProvider.class)
            header.onSetupView = { (view, _) -> () in
                view.backgroundColor = UIColor.green//UIColor.backgroundColor()
                
                let headerView = NTVHeaderTitleView(frame: CGRect.zero, topTitle: "Регистрация", downTitle: "в стрингер")
                view.addSubview(headerView)
                headerView.snp.makeConstraints { make in
                    make.left.top.right.equalTo(view)
                    make.height.equalTo(NTVHeaderTitleView.height)
                }
                
                let textPhoneField = NTVTextField(frame: CGRect(x: 0, y: 0, width: 0, height: 40))
                textPhoneField.textColor = UIColor.white
                textPhoneField.keyboardType = .emailAddress
                textPhoneField.autocapitalizationType = UITextAutocapitalizationType.none
                textPhoneField.attributedPlaceholder = NSAttributedString(string:"E-mail",//string:"Номер телефона или e-mail",
                    attributes:[NSForegroundColorAttributeName: UIColor.foregroundColor().darker()])
                view.addSubview(textPhoneField)
                textPhoneField.snp.makeConstraints { make in
                    make.top.equalTo(headerView.snp.bottom).offset(20)
                    make.left.equalTo(40)
                    make.right.equalTo(-20)
                    make.height.equalTo(40)
                }
                
                let buttonLogin = NTVButton()
                buttonLogin.setTitle("Запросить\nпароль", for: UIControlState())
                view.addSubview(buttonLogin)
                buttonLogin.snp.makeConstraints { make in
                    make.top.equalTo(textPhoneField.snp.bottom).offset(30)
                    make.left.equalTo(40)
                    make.right.equalTo(-40)
                    make.height.equalTo(40)
                }
                buttonLogin.rx.tap.asObservable().subscribeNext {
                    
                    NTVWebServiceManager.sharedManager.requestCode(textPhoneField.text!).subscribe(
                        onNext: { response in
                            let codeController = NTVLoginCodeViewController()
                            self.navigationController!.pushViewController(codeController, animated: true)
                        }, onError: { error in
                            self.showError(error)
                        }
                    ).addDisposableTo(self.disposeBag)
                    
                    
                }.addDisposableTo(self.disposeBag)
                
                let socialContainer = UIView()
                socialContainer.backgroundColor = UIColor.red
                view.addSubview(socialContainer)
                socialContainer.snp.makeConstraints { make in
                    make.left.equalTo(view)
                    make.right.equalTo(view)
                    make.bottom.equalTo(800)
                    make.height.equalTo(UIScreen.main.bounds.width/3 + UIScreen.main.bounds.width/12)
                    //make.height.equalTo(UIScreen.mainScreen().bounds.width - UIScreen.mainScreen().bounds.width/4)
                }
                
                let labelSocial = UILabel()
                labelSocial.text = "Или зайдите через соцсеть:"
                labelSocial.textAlignment = .center
                labelSocial.textColor = UIColor.foregroundColor()
                socialContainer.addSubview(labelSocial)
                labelSocial.snp.makeConstraints { make in
                    make.top.equalTo(0)
                    make.left.equalTo(socialContainer)
                    make.right.equalTo(socialContainer)
                    make.height.equalTo(20)
                }
                
                let buttonOdk = NTVSocialButton(social: .odk)
                socialContainer.addSubview(buttonOdk)
                buttonOdk.snp.makeConstraints { make in
                    make.top.equalTo(labelSocial.snp.bottom).offset(5)
                    make.left.equalTo(0)
                    make.width.equalTo(socialContainer.snp.width).dividedBy(3).offset(-1)
                    //make.width.equalTo(socialContainer.snp.width).dividedBy(2).offset(-1)
                    make.height.equalTo(socialContainer.snp.width).dividedBy(3)
                }
                buttonOdk.tap = {
                    //SocialLogin.odkLogin(self, clientOnly: true)
                    
                    OKSDK.authorize(withPermissions: ["VALUABLE_ACCESS", "LONG_ACCESS_TOKEN", "GET_EMAIL"], success: { data in
                        let token = OKSDK.currentAccessToken()
                        NTVWebServiceManager.sharedManager.loginViaOdnoklassniki(token!).subscribe(
                            onNext: { sid in
                                print("Вошли через odnoklassniki")
                                (UIApplication.shared.delegate as! AppDelegate).showCamera()
                            }, onError: { error in
                                print("\((error as NSError).localizedDescription)")
                            }
                            ).addDisposableTo(self.disposeBag)
                        }, error: { error in
                            print(error)
//                            if (error.code != 8) {
//                                dispatch_async(dispatch_get_main_queue(), ^{
//                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil];
//                                    [alert show];
//                                    });
//                            }
                    })
                }
                
                let buttonFb = NTVSocialButton(social: .fb)
                socialContainer.addSubview(buttonFb)
                buttonFb.snp.makeConstraints { make in
                    make.top.equalTo(buttonOdk)
                    make.centerX.equalTo(socialContainer)
                    //make.right.equalTo(0)
                    make.width.equalTo(buttonOdk)
                    make.height.equalTo(buttonOdk)
                }
                buttonFb.tap = {
                    SocialLogin.fbLogin(self)
                }
                
//                let buttonTw = NTVSocialButton(social: .tw)
//                socialContainer.addSubview(buttonTw)
//                buttonTw.snp.makeConstraints { make in
//                    make.top.equalTo(buttonOdk.snp.bottom).offset(2)
//                    make.left.equalTo(buttonOdk)
//                    make.width.equalTo(buttonOdk)
//                    make.height.equalTo(buttonOdk)
//                }
//                buttonTw.tap = {
//                    SocialLogin.twLogin(self, clientOnly: true)
//                }
                
                let buttonVk = NTVSocialButton(social: .vk)
                socialContainer.addSubview(buttonVk)
                buttonVk.snp.makeConstraints { make in
//                    make.top.equalTo(buttonTw)
//                    make.right.equalTo(buttonFb)
                    make.top.equalTo(buttonOdk)
                    make.right.equalTo(0)
                    make.width.equalTo(buttonOdk)
                    make.height.equalTo(buttonOdk)
                }
                buttonVk.tap = {
                    SocialLogin.vkLogin(self, clientOnly: false)
                    
                    VKSdk.wakeUpSession([VK_PER_EMAIL], complete: { (state, error) in
                        if state == VKAuthorizationState.authorized {
                            self.loginViaVK(VKSdk.accessToken().accessToken)
                        } else if error != nil {
                            print("vk error")
                        } else {
                            VKSdk.authorize([VK_PER_EMAIL])
                        }
                    })
                    
                }
                
                self.socialView = socialContainer
            }
            header.height = {
                let size = UIScreen.main.bounds
                return size.height > 568 ? size.height : 568
            }
            $0.header = header
        }

        if let s = section {
            form +++ s
        }
        
        
        
        _view.didLayout = {
            let table = self.tableView
            print("didlayout")
            if let section = self.section, let header = section.header, let height = header.height {
                let h = height()
//                let v = section.headerView
                print("section")
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Social
    func socialLoginCompleted(_ successful: Bool, credentials: [AnyHashable: Any]!, loginType: socialLoginType) {
        
        switch(loginType) {
        case kLoginFacebook:
            let token = credentials["token"] as! String
            NTVWebServiceManager.sharedManager.loginViaFacebook(token).subscribe(
                onNext: { sid in
                    print("Вошли через facebook")
                    (UIApplication.shared.delegate as! AppDelegate).showCamera()
                }, onError: { error in
                    print("\((error as NSError).localizedDescription)")
                }
            ).addDisposableTo(self.disposeBag)
//        case kLoginVK:
//        let token = credentials["token"] as! String
//        NTVWebServiceManager.sharedManager.loginViaVkontakte(token).subscribe(
//            onNext: { sid in
//                print("Вошли через vk")
//                (UIApplication.sharedApplication().delegate as! AppDelegate).showCamera()
//            }, onError: { error in
//                DDLogError("\((error as NSError).localizedDescription)")
//            }
//            ).addDisposableTo(self.disposeBag)
        case kLoginVK:
            let token = credentials!["token"] as! String
            
            NTVWebServiceManager.sharedManager.validateLoginVK(token).subscribe(
                onNext: { [unowned self] access_token in
                    
                    NTVWebServiceManager.sharedManager.loginViaVkontakte(access_token as! String).subscribe(
                        onNext: { sid in
                            print("Вошли через vk: \(sid)")
                            (UIApplication.shared.delegate as! AppDelegate).showCamera()
                        }, onError: { [unowned self] error in
                            print("\((error as NSError).localizedDescription)")
                            self.showError(error)
                        }
                        ).addDisposableTo(self.disposeBag)
                    
                }, onError: { [unowned self] error in
                    print("\((error as NSError).localizedDescription)")
                    self.showError(error)
                }
                ).addDisposableTo(disposeBag)

        case kLoginODK:
            let token = credentials["token"] as! String
            NTVWebServiceManager.sharedManager.loginViaOdnoklassniki(token).subscribe(
                onNext: { sid in
                    print("Вошли через odnoklassniki")
                    (UIApplication.shared.delegate as! AppDelegate).showCamera()
                }, onError: { error in
                    print("\((error as NSError).localizedDescription)")
                }
                ).addDisposableTo(self.disposeBag)
        default: ()
        }
        
    }
    
    fileprivate func loginViaVK(_ accesToken: String) {
        NTVWebServiceManager.sharedManager.loginViaVkontakte(accesToken).subscribe(
            onNext: { sid in
                print("Вошли через vk")
                (UIApplication.shared.delegate as! AppDelegate).showCamera()
            }, onError: { error in
                print("\((error as NSError).localizedDescription)")
            }
            ).addDisposableTo(self.disposeBag)
    }
    
    internal func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        if result.error != nil {
            print("vk login error")
        } else {
            let token = result.token.accessToken
            loginViaVK(token!)
        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        print("vk login failed")
    }
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        self.present(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        print("vk captcha!")
    }


}
