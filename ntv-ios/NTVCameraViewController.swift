//
//  NTVCameraViewController.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 30/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//


//override func viewDidLoad() {
//    super.viewDidLoad()
//    
//    let audioSession = AVAudioSession.sharedInstance()
//    do {
//        try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
//        try audioRecorder = AVAudioRecorder(URL: self.directoryURL()!,
//                                            settings: recordSettings)
//        audioRecorder.prepareToRecord()
//    } catch {
//    }
//}
//
//func directoryURL() -> NSURL? {
//    let fileManager = NSFileManager.defaultManager()
//    let urls = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
//    let documentDirectory = urls[0] as NSURL
//    let soundURL = documentDirectory.URLByAppendingPathComponent("sound.m4a")
//    return soundURL
//}
//
//@IBAction func doRecordAction(sender: AnyObject) {
//    if !audioRecorder.recording {
//        let audioSession = AVAudioSession.sharedInstance()
//        do {
//            try audioSession.setActive(true)
//            audioRecorder.record()
//        } catch {
//        }
//    }
//}
//
//@IBAction func doStopAction(sender: AnyObject) {
//    audioRecorder.stop()
//    let audioSession = AVAudioSession.sharedInstance()
//    
//    do {
//        try audioSession.setActive(false)
//    } catch {
//    }
//}

import UIKit
import GPUImage
import SnapKit
import RxSwift
import RxCocoa

enum Orientation {
    case portrait
    case landscape
}

class NTVCameraViewController: UIViewController, GPUImageMovieWriterDelegate {

    let disposeBag = DisposeBag()
    
    let rotate = NTVRotateManager.sharedManager
    
    let maxRecordDuration: Double = 60
    
    fileprivate var _isFrontCamera = false
    fileprivate var _isAudio = false
    fileprivate var _isTorchAvailable = false
    fileprivate var _videoCamera: GPUImageVideoCamera?
    
    var movieWriter: GPUImageMovieWriter?
    var filename: String?
    var videoStoragePath: String?
    var timer: Timer?
    var timerDate: Date?
    
    let labelTimeTitle = UILabel()
    let labelTime = UILabel()
    let labelTimeSeconds = UILabel()
    let imageProgress = UIImageView()
    let imageSmileCamera = UIImageView()
    let buttonUserInfo = UIButton()
    let buttonInfo = UIButton()
    let buttonLocal = UIButton()
    let buttonFlash = UIButton()
    let buttonCamera = UIButton()
    let buttonRecord = UIButton()
    
    var quality: String = AVCaptureSessionPresetHigh
    
    fileprivate var lastOrientation = Orientation.portrait
    
    fileprivate var registered = false
    
    lazy var _view: GPUImageView = {
        return self.view as! GPUImageView
    }()
    
    override func loadView() {
        super.loadView()
        
        view = GPUImageView(frame: UIScreen.main.bounds)
        
        buttonRecord.setImage(UIImage(named: "button_record"), for: UIControlState())
        buttonRecord.setImage(UIImage(named: "button_stop"), for: .selected)
        view.addSubview(buttonRecord)
        buttonRecord.snp.makeConstraints { make in
            make.centerY.equalTo(self.view)
            make.right.equalTo(-20)
            make.width.height.equalTo(66)
        }
        buttonRecord.rx.tap.asObservable().subscribeNext {
            self.clickOnRecordButton(self.buttonRecord)
        }.addDisposableTo(self.disposeBag)

        buttonCamera.setImage(UIImage(named: "button_rotate"), for: UIControlState())
        buttonCamera.addTarget(self, action: #selector(clickOnCameraButton), for: .touchUpInside)
        view.addSubview(buttonCamera)
        buttonCamera.snp.makeConstraints { (make) in
            make.bottom.right.equalTo(-20)
            make.width.height.equalTo(40)
        }
        
        buttonFlash.setImage(UIImage(named: "button_flash"), for: UIControlState())
        view.addSubview(buttonFlash)
        buttonFlash.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.right.equalTo(-20)
            make.width.height.equalTo(40)
        }
        buttonFlash.rx.tap.asObservable().subscribeNext {
            self.clickOnFlashbutton(self.buttonFlash)
        }.addDisposableTo(self.disposeBag)
        
        buttonInfo.setImage(UIImage(named: "button_info"), for: UIControlState())
        view.addSubview(buttonInfo)
        buttonInfo.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.left.equalTo(20)
            make.width.height.equalTo(40)
        }
        
        buttonLocal.setImage(UIImage(named: "button_gallery"), for: UIControlState())
        view.addSubview(buttonLocal)
        buttonLocal.snp.makeConstraints { (make) in
            make.bottom.equalTo(-20)
            make.left.equalTo(20)
            make.width.height.equalTo(40)
        }
        buttonLocal.rx.tap.asObservable().subscribeNext {
            let galleryViewController = NTVGalleryViewController()
            let nav = NTVGalleryNavigationController(rootViewController: galleryViewController)
            self.present(nav, animated: true, completion: nil)
        }.addDisposableTo(self.disposeBag)
        
        imageProgress.image = UIImage(named: "progress_rec")
        view.addSubview(imageProgress)
        imageProgress.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view)
            make.top.equalTo(20)
            make.width.height.equalTo(22)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        _view.fillMode = kGPUImageFillModePreserveAspectRatioAndFill
        
        _buttonsApperance()
        
        labelTimeTitle.text = NSLocalizedString("untiltheend", comment: "")
        labelTimeSeconds.text = NSLocalizedString("seconds", comment: "")
        
        buttonCamera.isSelected = false
        clickOnCameraButton(self.buttonCamera)
        
        imageSmileCamera.image = UIImage(named: "reverse")
        
        buttonInfo.rx.tap.asObservable().subscribeNext {
            let anketaViewController = NTVAnketaViewController()
            let nav = NTVAnketaNavigationController(rootViewController: anketaViewController)
            self.present(nav, animated: true, completion: nil)
        }.addDisposableTo(self.disposeBag)
        
//        let orientation_value = UIDevice.currentDevice().orientation.rawValue
//        UIDevice.currentDevice().setValue(orientation_value, forKey: "orientation")
//        
//        UIApplication.sharedApplication().setStatusBarOrientation(.LandscapeRight, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if registered == false {
            registered = true
            _registerRotate()
        }

        if !UserDefaults.anketa() {
            let anketaViewController = NTVAnketaViewController(backButtonVisibled: false)
            let nav = NTVAnketaNavigationController(rootViewController: anketaViewController)
            self.present(nav, animated: false, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    fileprivate func _buttonsApperance() {
        let enabled = !buttonRecord.isSelected
        buttonInfo.isEnabled = enabled
        buttonLocal.isEnabled = enabled
        buttonFlash.isEnabled = enabled
        buttonCamera.isEnabled = enabled
        
        labelTimeTitle.isHidden = true
        labelTime.isHidden = true
        labelTimeSeconds.isHidden = true
        imageProgress.isHidden = true
    }
    
    fileprivate func _cleanCaptureForRecord() {
        print("> cleanCaptureForRecord")
        if let camera = _videoCamera {
            
            print("> stopCameraCapture")
            camera.stopCapture()
            print("< stopCameraCapture")
            print("> removeAllTargets")
            camera.removeAllTargets()
            _videoCamera = nil;
            print("< removeAllTargets")
        }
        
        
        if let _ = movieWriter {
            movieWriter = nil
        }
        print("< cleanCaptureForRecord")
    }
    
    fileprivate func _initCaptureForRecord() {
        print("> initCaptureForRecord")
        
        _cleanCaptureForRecord()
        
        _videoCamera = GPUImageVideoCamera(sessionPreset: self.quality, cameraPosition: self.buttonCamera.isSelected ? .back : .front)
        
        
        guard let camera = _videoCamera else {
            return
        }
        
        _isTorchAvailable = camera.inputCamera.isTorchModeSupported(.on)
        if !_isTorchAvailable {
            self.buttonFlash.isEnabled = false
        }
        
        let currentOrientation = NTVRotateManager.sharedManager.currentOrientation//UIDevice.currentDevice().orientation
        
        if currentOrientation == .landscapeLeft {
            camera.outputImageOrientation = .landscapeRight
        } else if currentOrientation == .landscapeRight {
            camera.outputImageOrientation = .landscapeLeft
        } else if currentOrientation == .portrait {
            camera.outputImageOrientation = .portrait
        } else if currentOrientation == .portraitUpsideDown {
            camera.outputImageOrientation = .portraitUpsideDown
        }
        
        camera.horizontallyMirrorFrontFacingCamera = true
        camera.horizontallyMirrorRearFacingCamera = false
        
        camera.addTarget(_view)

        camera.startCapture()
        print("< initCaptureForRecord")
    }
    
    func clickOnCameraButton(_ sender: UIButton?) {
        guard let button = sender else {
            return
        }
        
        button.isSelected = !button.isSelected
        
        _isFrontCamera = !button.isSelected
        _initCaptureForRecord()
        
        if _isTorchAvailable {
            self.buttonFlash.isEnabled = !_isFrontCamera
        }
    }
    
    func clickOnFlashbutton(_ sender: UIButton?) {
        guard let button = sender else {
            print("button is nil!")
            return
        }
        
        guard let camera = _videoCamera else {
            print("camera is nil!")
            return
        }
        
        button.isSelected = !button.isSelected
        
        do {
            try camera.inputCamera.lockForConfiguration()
        } catch {
            print("camera error lock configuration")
        }
        
        if buttonFlash.isSelected {
            camera.inputCamera.torchMode = .on
        } else {
            camera.inputCamera.torchMode = .off
        }
        
        camera.inputCamera.unlockForConfiguration()
    }
    
    func clickOnRecordButton(_ sender: UIButton?) {
        guard let button = sender else {
            print("button is nil!")
            return
        }
        button.isSelected = !button.isSelected
        button.isEnabled = false
        
        if button.isSelected {
            _recordLocalON()
        } else {
            _recordLocalOFF()
        }
    }
    
    // MARK: - Record
    
    fileprivate func _newFileName() -> String {
        let format = DateFormatter()
        format.dateFormat = "yyyyMMddHHmmss"
        let s = format.string(from: Date())
        
        return s + ".mov"
    }
    
    fileprivate func _prepareToRecFile() {
        filename = _newFileName()
        videoStoragePath = filename
        
        print("\(videoStoragePath)")
    }
    
    fileprivate func _recordLocalON() {
        print("Start record local...")
        
        _buttonsApperance()
        
        _prepareToRecFile()
        
        let pathToMovie = (self.videoStoragePath != nil)
            ? NTVLocalFilesManager.sharedManager.fullPathByFile(self.videoStoragePath!)
            : (NSHomeDirectory() as NSString).appendingPathComponent("Documents/empty.m4v")
        
        guard let _ = pathToMovie else {
            print("pathToMovie is nil!!!")
            return
        }
        
        guard let camera = _videoCamera else {
            print("camera is nil!")
            return
        }
        
        do {
            try FileManager.default.removeItem(atPath: pathToMovie!)
        } catch {
            print("no remove when record!!!")
        }

        let movieURL = URL(fileURLWithPath: pathToMovie!)

//        let size = /*UIScreen.mainScreen().bounds.size*/ self.isLandscape() ? CGSize(width: 666/*640*/, height: 374/*480*/) : CGSize(width: 374/*480*/, height: 666/*640*/)
        
        var size = UIScreen.main.bounds.size
        if size.width.truncatingRemainder(dividingBy: 2) != 0 {
            size.width -= 1
        }
        
        if size.height.truncatingRemainder(dividingBy: 2) != 0 {
            size.height -= 1
        }
        
        print("Размер записи: \(size)")
        
        movieWriter = GPUImageMovieWriter(movieURL: movieURL, size: size)
        
        self.movieWriter!.delegate = self;
        
        camera.addTarget(movieWriter!)
        camera.audioEncodingTarget = movieWriter
        
        movieWriter?.startRecording()
        
        buttonRecord.isEnabled = true
        
        //    NSUserDefaults *udef = [NSUserDefaults standardUserDefaults];
        //    [udef setObject:@{@"long": [NSNumber numberWithDouble:self.currentUserCoordinate.longitude],
        //                      @"lat": [NSNumber numberWithDouble:self.currentUserCoordinate.latitude]}
        //             forKey:self.filename];
        //    [udef synchronize];

        _timerStart()
    }
    
    fileprivate func _recordLocalOFF() {
        guard let camera = _videoCamera else {
            return
        }
        
        guard let mw = movieWriter else {
            return
        }
        
        print("Stop record local...")
        mw.finishRecording()
        camera.audioEncodingTarget = nil;
        print("Movie completed: \(self.videoStoragePath)")
        
        videoRecordedSuccefully(filename!, videoStoragePath: videoStoragePath!)
        
        _initCaptureForRecord()
        _buttonsApperance()
        
        self.buttonRecord.isEnabled = true
        
        _timerStop()
    }
    
    func movieRecordingCompleted() {
        print("movieRecordingCompleted")
    }

    func videoRecordedSuccefully(_ filename: String, videoStoragePath:String) {
    
        let movie = NTVMovieModel()
        movie.name = ""
        movie.descript = ""
        movie.filename = filename
        movie.fullFileNameStorage = videoStoragePath
        movie.date = Date()

        NTVLocalFilesManager.sharedManager.addLocalMovie(movie)
        NTVLocalFilesManager.sharedManager.refreshLocal()
        
        let saveViewController = NTVSaveVideoViewController()
        let saveNavigation = NTVNavigationSaveViewController(rootViewController: saveViewController)
        
        saveViewController.index = NTVLocalFilesManager.sharedManager.movies.count - 1
        
        self.present(saveNavigation, animated: true, completion: nil)
    }
    
    func _timerStart() {
        timerDate = Date().addingTimeInterval(maxRecordDuration + 1)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timertick), userInfo: nil, repeats: true)
        labelTime.text = String(format: "%02d", maxRecordDuration)
    }
    
    func _timerStop() {
        guard let t = timer else {
            return
        }
        t.invalidate()
        timer = nil
        labelTime.text = ""
    }
    
    func timertick(_ timer: Timer) {
        
        guard let td = timerDate else {
            return
        }
        
        let seconds = abs(td.timeIntervalSinceNow)
        
        labelTime.text = String(format: "%02d", seconds)
        
        print("seconds: \(seconds)")
        if (!(seconds > 0)) {
            clickOnRecordButton(self.buttonRecord)
        }
    }
    
    // MARK: - Rotate
    
    fileprivate func _rotateDisable() {
        if !buttonRecord.isSelected {
            let value = false
            imageSmileCamera.isHidden = value
            buttonRecord.isEnabled = value
        }
    }
    
    fileprivate func _rotateEnable() {
        let value = true
        self.imageSmileCamera.isHidden = value
        self.buttonRecord.isEnabled = value
    }
    
    fileprivate func _registerRotate() {
        print("Register rotate")
        
        NotificationCenter.default
            .rx.notification(Notification.Name(rawValue: NotifyOrientation.kNotifyOrientationPortrait.rawValue))
        .subscribeNext { notification in
            //self._rotateDisable()
            self._rotateEnable()
            if !self.buttonRecord.isSelected {
                self._initCaptureForRecord()
            }
        }.addDisposableTo(self.disposeBag)
        
        NotificationCenter.default
            .rx.notification(Notification.Name(rawValue: NotifyOrientation.kNotifyOrientationUpsideDown.rawValue))
        .subscribeNext { notification in
            //self._rotateDisable()
            self._rotateEnable()
            if !self.buttonRecord.isSelected {
                self._initCaptureForRecord()
            }
        }.addDisposableTo(self.disposeBag)
    
        NotificationCenter.default
            .rx.notification(Notification.Name(rawValue: NotifyOrientation.kNotifyOrientationLandscapeLeft.rawValue))
        .subscribeNext { notification in
            self._rotateEnable()
            if !self.buttonRecord.isSelected {
                self._initCaptureForRecord()
            }
        }.addDisposableTo(self.disposeBag)
        
        NotificationCenter.default
            .rx.notification(Notification.Name(rawValue: NotifyOrientation.kNotifyOrientationLandscapeRight.rawValue))
        .subscribeNext { notification in
            self._rotateEnable()
            if !self.buttonRecord.isSelected {
                self._initCaptureForRecord()
            }
        }.addDisposableTo(self.disposeBag)
        
        self._rotateEnable()
        self._initCaptureForRecord()
    }
    
    override var shouldAutorotate : Bool {
        return !self.buttonRecord.isSelected
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIDevice.interfaceOrientation
    }
    
    func isLandscape() -> Bool {
        
        let current_orientation = UIDevice.current.orientation
        
        if current_orientation.isLandscape && !current_orientation.isPortrait {
            lastOrientation = .landscape
        }
        
        if current_orientation.isPortrait && !current_orientation.isLandscape {
            lastOrientation = .portrait
        }
        
        return lastOrientation == .landscape
    }
}
