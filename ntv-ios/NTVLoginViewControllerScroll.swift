//
//  NTVLoginViewControllerScroll.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 21/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit
import Eureka
import SnapKit
import RxSwift
import RxCocoa
import ok_ios_sdk
import VK_ios_sdk
//import GoogleSignIn

class NTVLoginViewControllerScroll: UIViewController, SocialLoginDelegate, GIDSignInUIDelegate {

    let disposeBag = DisposeBag()
    
    var section: Section?
    var socialView: UIView?
    
    let scrollView = UIScrollView()
    let textPhoneField = NTVTextField(frame: CGRect(x: 0, y: 0, width: 0, height: 40))
    
    lazy var _view: NTVView = {
        self.view as! NTVView
    }()
    
    override func loadView() {
        super.loadView()
        
        view = UIView(frame: UIScreen.main.bounds)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let touch = UITapGestureRecognizer()
        touch.rx.event.subscribeNext { recognizer in
            self.textPhoneField.resignFirstResponder()
        }.addDisposableTo(self.disposeBag)
        view.addGestureRecognizer(touch)
        
        let udef = UserDefaults.standard
        if let sid = udef.object(forKey: "sid") as? String {
            NTVWebServiceManager.sharedManager.autoLogin(sid).subscribe(
                onNext: { _ in
                    (UIApplication.shared.delegate as! AppDelegate).showCamera()
                }, onError: { error in
                    print("\(error)")
                }
                ).addDisposableTo(self.disposeBag)
            
        }
        
        let settings = OKSDKInitSettings()
        settings.appKey = Bundle.main.infoDictionary!["ODKSecretKey"] as! String
        settings.appId = Bundle.main.infoDictionary!["ODKAppKey"] as! String
        settings.controllerHandler = {
            return self
        }
        OKSDK.initWith(settings)
        
        let vkAppId = Bundle.main.infoDictionary!["VKAppKey"] as! String
        let sdkInstance = VKSdk.initialize(withAppId: vkAppId)
        sdkInstance?.register(self)
        sdkInstance?.uiDelegate = self

        view.addSubview(scrollView)
        self.scrollView.snp.makeConstraints { make in
            make.left.right.top.bottom.equalTo(self.view)
        }
        
        
        self.view.backgroundColor = UIColor.backgroundColor()

        let headerView = NTVHeaderTitleView(frame: CGRect.zero, topTitle: "Регистрация", downTitle: "в стрингер")
        view.addSubview(headerView)
        headerView.snp.makeConstraints { make in
            make.left.top.right.equalTo(view)
            make.height.equalTo(NTVHeaderTitleView.height)
        }

        
        textPhoneField.textColor = UIColor.white
        textPhoneField.keyboardType = .emailAddress
        textPhoneField.attributedPlaceholder = NSAttributedString(string:"Номер телефона или E-mail",
            attributes:[NSForegroundColorAttributeName: UIColor.foregroundColor().darker()])
        view.addSubview(textPhoneField)
        textPhoneField.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.bottom).offset(20)
            make.left.equalTo(40)
            make.right.equalTo(-20)
            make.height.equalTo(40)
        }
        
        let buttonLogin = NTVButton()
        buttonLogin.setTitle("Запросить\nпароль", for: UIControlState())
        view.addSubview(buttonLogin)
        buttonLogin.snp.makeConstraints { make in
            make.top.equalTo(textPhoneField.snp.bottom).offset(30)
            make.left.equalTo(40)
            make.right.equalTo(-40)
            make.height.equalTo(40)
        }
        buttonLogin.rx.tap.asObservable().subscribeNext {
            
            NTVWebServiceManager.sharedManager.requestCode(self.textPhoneField.text!).subscribe(
                onNext: { response in
                    
                    let codeController = NTVLoginCodeViewController()
                    if self.textPhoneField.text!.isEmail() {
                        codeController.waitEmailCode = true
                    }
                    self.navigationController!.pushViewController(codeController, animated: true)
                }, onError: { error in
                    self.showError(error)
                }
                ).addDisposableTo(self.disposeBag)
            
            
            }.addDisposableTo(self.disposeBag)
        
        
        let socialContainer = UIView()
        view.addSubview(socialContainer)
        socialContainer.snp.makeConstraints { make in
            make.left.equalTo(view)
            make.right.equalTo(view)
            make.bottom.equalTo(self.view)
            make.height.equalTo(UIScreen.main.bounds.width/3 + UIScreen.main.bounds.width/12)
        }
        
        let labelSocial = UILabel()
        labelSocial.text = "Или зайдите через соцсеть:"
        labelSocial.textAlignment = .center
        labelSocial.textColor = UIColor.foregroundColor()
        socialContainer.addSubview(labelSocial)
        labelSocial.snp.makeConstraints { make in
            make.top.equalTo(0)
            make.left.equalTo(socialContainer)
            make.right.equalTo(socialContainer)
            make.height.equalTo(20)
        }
        
        let buttonOdk = NTVSocialButton(social: .odk)
        socialContainer.addSubview(buttonOdk)
        buttonOdk.snp.makeConstraints { make in
            make.top.equalTo(labelSocial.snp.bottom).offset(5)
            make.left.equalTo(0)
            make.width.equalTo(socialContainer.snp.width).dividedBy(5).offset(-1)
            //make.width.equalTo(socialContainer.snp.width).dividedBy(2).offset(-1)
            make.height.equalTo(socialContainer.snp.width).dividedBy(3)
        }
        buttonOdk.tap = {
            //SocialLogin.odkLogin(self, clientOnly: true)
            
            (UIApplication.shared.delegate as! AppDelegate).socialLogin = .ok
            
            OKSDK.authorize(withPermissions: ["VALUABLE_ACCESS", "LONG_ACCESS_TOKEN", "GET_EMAIL"], success: { data in
                let token = OKSDK.currentAccessToken()
                NTVWebServiceManager.sharedManager.loginViaOdnoklassniki(token!).subscribe(
                    onNext: { sid in
                        print("Вошли через odnoklassniki")
                        (UIApplication.shared.delegate as! AppDelegate).showCamera()
                    }, onError: { error in
                        print("\((error as NSError).localizedDescription)")
                    }
                    ).addDisposableTo(self.disposeBag)
                }, error: { error in
                    print(error)
                    //                            if (error.code != 8) {
                    //                                dispatch_async(dispatch_get_main_queue(), ^{
                    //                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil];
                    //                                    [alert show];
                    //                                    });
                    //                            }
            })
        }
        
        let buttonTw = NTVSocialButton(social: .tw)
        socialContainer.addSubview(buttonTw)
        buttonTw.snp.makeConstraints { make in
            make.top.equalTo(buttonOdk)
            make.left.equalTo(buttonOdk.snp.right).offset(1)
            make.width.equalTo(buttonOdk)
            make.height.equalTo(buttonOdk)
        }
        buttonTw.tap = {
            SocialLogin.twLogin(self, clientOnly: true)
        }
        
        let buttonFb = NTVSocialButton(social: .fb)
        socialContainer.addSubview(buttonFb)
        buttonFb.snp.makeConstraints { make in
            make.top.equalTo(buttonOdk)
            make.centerX.equalTo(socialContainer)
            //make.right.equalTo(0)
            make.width.equalTo(buttonOdk)
            make.height.equalTo(buttonOdk)
        }
        buttonFb.tap = {
            SocialLogin.fbLogin(self)
        }
        
        let buttonGp = NTVSocialButton(social: .gp)
        socialContainer.addSubview(buttonGp)
        buttonGp.snp.makeConstraints { make in
            make.top.equalTo(buttonOdk)
            make.left.equalTo(buttonFb.snp.right).offset(1)
            make.width.equalTo(buttonOdk)
            make.height.equalTo(buttonOdk)
        }
        buttonGp.tap = {
            (UIApplication.shared.delegate as! AppDelegate).socialLogin = .gpp
            //SocialLogin.gppLogin(self, clientOnly: true)
            GIDSignIn.sharedInstance().signIn()
        }
        
        let buttonVk = NTVSocialButton(social: .vk)
        socialContainer.addSubview(buttonVk)
        buttonVk.snp.makeConstraints { make in
            //                    make.top.equalTo(buttonTw)
            //                    make.right.equalTo(buttonFb)
            make.top.equalTo(buttonOdk)
            make.right.equalTo(0)
            make.width.equalTo(buttonOdk)
            make.height.equalTo(buttonOdk)
        }
        buttonVk.tap = {
            (UIApplication.shared.delegate as! AppDelegate).socialLogin = .vk
            SocialLogin.vkLogin(self, clientOnly: false)
            
//            VKSdk.wakeUpSession([VK_PER_EMAIL], completeBlock: { (state, error) in
//                if state == VKAuthorizationState.Authorized {
//                    self.loginViaVK(VKSdk.accessToken().accessToken)
//                } else if error != nil {
//                    DDLogError("vk error")
//                } else {
//                    VKSdk.authorize([VK_PER_EMAIL])
////                    if VKSdk.vkAppMayExists() {
////                        VKSdk.authorize([VK_PER_EMAIL])
////                    } else {
////                        VKSdk.authorize([VK_PER_EMAIL])
////                        //VKSdk.authorize([VK_PER_EMAIL], withOptions: .DisableSafariController)
////                        //VKSdk.authorize([VK_PER_EMAIL], revokeAccess: true)
////                        
////                    }
//                }
//            })
            
        }
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
        NotificationCenter.default
            .rx.notification(Notification.Name(rawValue: notification_gpp_token))
            .subscribeNext { (notification) in
                if let token = notification.object as? String {
                    NTVWebServiceManager.sharedManager.loginViaGPP(token).subscribe(
                        onNext: { sid in
                            print("Вошли через g+")
                            (UIApplication.shared.delegate as! AppDelegate).showCamera()
                        }, onError: { error in
                            print("\((error as NSError).localizedDescription)")
                        }
                        ).addDisposableTo(self.disposeBag)
                }
            }.addDisposableTo(disposeBag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Social
    func socialLoginCompleted(_ successful: Bool, credentials: [AnyHashable: Any]!, loginType: socialLoginType) {
        
        if successful == false {
            self.showCriticalError()
        }
        
        switch(loginType) {
        case kLoginFacebook:
            let token = credentials["token"] as! String
            NTVWebServiceManager.sharedManager.loginViaFacebook(token).subscribe(
                onNext: { sid in
                    print("Вошли через facebook")
                    (UIApplication.shared.delegate as! AppDelegate).showCamera()
                }, onError: { error in
                    print("\((error as NSError).localizedDescription)")
                }
            ).addDisposableTo(self.disposeBag)
//        case kLoginVK:
//        let token = credentials["token"] as! String
//        NTVWebServiceManager.sharedManager.loginViaVkontakte(token).subscribe(
//            onNext: { sid in
//                print("Вошли через vk")
//                (UIApplication.sharedApplication().delegate as! AppDelegate).showCamera()
//            }, onError: { error in
//                DDLogError("\((error as NSError).localizedDescription)")
//            }
//            ).addDisposableTo(self.disposeBag)
        case kLoginVK:
            let token = credentials!["token"] as! String
            
            NTVWebServiceManager.sharedManager.validateLoginVK(token).subscribe(
                onNext: { [unowned self] access_token in
                    
                    NTVWebServiceManager.sharedManager.loginViaVkontakte(access_token as! String).subscribe(
                        onNext: { sid in
                            print("Вошли через vk: \(sid)")
                            (UIApplication.shared.delegate as! AppDelegate).showCamera()
                        }, onError: { [unowned self] error in
                            print("\((error as NSError).localizedDescription)")
                            self.showError(error)
                        }
                        ).addDisposableTo(self.disposeBag)
                    
                }, onError: { [unowned self] error in
                    print("\((error as NSError).localizedDescription)")
                    self.showError(error)
                }
                ).addDisposableTo(disposeBag)

        case kLoginODK:
            let token = credentials["token"] as! String
            NTVWebServiceManager.sharedManager.loginViaOdnoklassniki(token).subscribe(
                onNext: { sid in
                    print("Вошли через odnoklassniki")
                    (UIApplication.shared.delegate as! AppDelegate).showCamera()
                }, onError: { error in
                    print("\((error as NSError).localizedDescription)")
                }
                ).addDisposableTo(self.disposeBag)
            
        case kLoginTwitter:
            let token = credentials["token"] as! String
            let secret = credentials["token_secret"] as! String
            NTVWebServiceManager.sharedManager.loginViaTwitter(token, secret: secret).subscribe(
                onNext: { sid in
                    print("Вошли через twitter")
                    (UIApplication.shared.delegate as! AppDelegate).showCamera()
                }, onError: { error in
                    print("\((error as NSError).localizedDescription)")
                }
                ).addDisposableTo(self.disposeBag)
            print("\(token)")
        case kLoginGPP:
            let token = credentials["token"] as! String
            print("\(token)")
        default: ()
        }
        
    }
    
    fileprivate func loginViaVK(_ accesToken: String) {
        NTVWebServiceManager.sharedManager.loginViaVkontakte(accesToken).subscribe(
            onNext: { sid in
                print("Вошли через vk")
                (UIApplication.shared.delegate as! AppDelegate).showCamera()
            }, onError: { error in
                print("\((error as NSError).localizedDescription)")
            }
            ).addDisposableTo(self.disposeBag)
    }
}

extension NTVLoginViewControllerScroll: VKSdkDelegate, VKSdkUIDelegate{
    /**
     Notifies about authorization was completed, and returns authorization result with new token or error.
     
     @param result contains new token or error, retrieved after VK authorization.
     */
    public func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        if result.error != nil {
            print("vk login error")
        } else {
            let token = result.token.accessToken
            loginViaVK(token!)
        }
    }

    /**
     Pass view controller that should be presented to user. Usually, it's an authorization window.
     
     @param controller view controller that must be shown to user
     */
    @available(iOS 2.0, *)
    public func vkSdkShouldPresent(_ controller: UIViewController!) {
        self.navigationController?.topViewController?.present(controller, animated: true, completion: nil)
    }

    
    func vkSdkAccessAuthorizationFinishedWithResult(result: VKAuthorizationResult!) {
        if result.error != nil {
            print("vk login error")
        } else {
            let token = result.token.accessToken
            loginViaVK(token!)
        }
    }
    
    func vkSdkTokenHasExpired(_ expiredToken: VKAccessToken) {
        VKSdk.authorize([VK_PER_EMAIL])
    }
    
    func vkSdkShouldPresentViewController(controller: UIViewController) {
        self.navigationController?.topViewController?.present(controller, animated: true, completion: nil)
//        func vkSdkShouldPresentViewController(controller: UIViewController!) {
//            self.navigationController!.presentViewController(controller, animated: true, completion: nil)
//        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        print("vk login failed")
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        print("vk captcha!")
    }
    
    func vkSdkUserDeniedAccess(_ authorizationError: VKError) {
        
    }
    
    func vkSdkReceivedNewToken(_ newToken: VKAccessToken) {
        
    }
    
    // Implement these methods only if the GIDSignInUIDelegate is not a subclass of
    // UIViewController.
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        //myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}
