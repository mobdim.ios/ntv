//
//  UIButton.extension.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 07/04/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit

extension UIButton {
    static func backButton() -> UIButton {
        let b = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        
        b.setTitle("Назад", for: UIControlState())
        b.setTitleColor(UIColor.foregroundColor(), for: UIControlState())
        
        b.setImage(UIImage(named: "button_back"), for: UIControlState())
        
        return b
    }
}
