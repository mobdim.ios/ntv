//
//  NTVTextField.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 23/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit

class NTVTextField: UITextField {
    
    let bottomBorder = CALayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        bottomBorder.backgroundColor = UIColor.backgroundColor().lighter().cgColor
        self.layer.addSublayer(bottomBorder)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        bottomBorder.frame = CGRect(x: 0.0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1.0);
    }
    
//    override func drawPlaceholderInRect(rect: CGRect) {
//        UIColor.blueColor().set()
//        self.placeholder.draw
//    }
}
