//
//  DDLog.init.extension.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 31/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit
import CocoaLumberjackSwift

extension DDLog {
    static func initLog() {
        
        setenv("XcodeColors", "YES", 1);
        
        DDLog.add(DDASLLogger.sharedInstance())
        DDLog.add(DDTTYLogger.sharedInstance())
        
        DDTTYLogger.sharedInstance().setForegroundColor(UIColor.white, backgroundColor: UIColor.red, for: .error)
        DDTTYLogger.sharedInstance().setForegroundColor(UIColor(red: 1, green: 0x85/255, blue: 0x1B/255, alpha: 1), backgroundColor: nil, for: .warning)
        DDTTYLogger.sharedInstance().setForegroundColor(UIColor(red: 0x33/255, green: 0x33/255, blue: 1, alpha: 1), backgroundColor: nil, for: .info)
        DDTTYLogger.sharedInstance().setForegroundColor(UIColor(red: 0x44/255, green: 0x44/255, blue: 0x44/255, alpha: 1), backgroundColor: nil, for: .debug)
        DDTTYLogger.sharedInstance().setForegroundColor(UIColor(red: 0xAA/255, green: 0xAA/255, blue: 0xAA/255, alpha: 1), backgroundColor: nil, for: .verbose)
        DDTTYLogger.sharedInstance().colorsEnabled = true
    }
}
