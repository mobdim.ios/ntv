//
//  NTVAnketaNavigationController.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 07/04/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit

class NTVAnketaNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var shouldAutorotate : Bool {
        return true
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return [.portrait, .portraitUpsideDown]
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }


}
