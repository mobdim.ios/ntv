//
//  NTVAnketaViewController.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 07/04/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit
import Eureka
import RxSwift
import RxCocoa
import CSwiftV

final class CheckPermissionCell: Cell<Bool>, CellType {
    let checkImage = UIImageView()
    let labelTitle =  UILabel()
    var checked = false
    
    required internal init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setup() {
        super.setup()
        
        addSubview(checkImage)
        addSubview(labelTitle)
        
    }
    
    override func update() {
        super.update()
        
        
        checkImage.image = UIImage(named: "button_checkoff")
        
        checkImage.snp.remakeConstraints { make in
            make.centerY.equalTo(self)
            make.left.equalTo(20)
            make.width.equalTo(25)
            make.height.equalTo(20)
        }
        
        labelTitle.numberOfLines = 2
        labelTitle.textColor = UIColor.foregroundColor()
        labelTitle.snp.remakeConstraints { make in
            make.centerY.equalTo(checkImage)
            make.left.equalTo(checkImage.snp.right).offset(20)
            make.width.equalTo(240)
            make.height.equalTo(50)
        }
    }
}

class _CheckPermissionRow: Row<CheckPermissionCell> {
    
    required public init(tag: String?) {
        super.init(tag: tag)
        displayValueFor = nil
    }
}

final class CheckPermissionRow: _CheckPermissionRow, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
    }
}

class NTVAnketaViewController: FormViewController {

    let disposeBag = DisposeBag()
    
    var anketa = NTVAnketa()
    var regions: [[String]]?
    
    var check1 = false
    //var check2 = false
    let section = Section()
    
    var backButtonVisibled = false
    
    init(backButtonVisibled: Bool = true) {
        super.init(nibName: nil, bundle: nil)
        self.backButtonVisibled = backButtonVisibled
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        do {
            let filename = Bundle.main.path(forResource: "regions", ofType: "csv")
            let content = try String(contentsOfFile:filename!, encoding: String.Encoding.utf8)
            let csv = CSwiftV(with: content, separator: ",", headers: nil)
            regions = csv.rows
        } catch {
            print("error")
        }
//
        tableView?.separatorStyle = .none
        tableView?.backgroundColor = UIColor.backgroundColorDark()

        let topView = UIView()
        topView.backgroundColor = UIColor.backgroundColorDark(alpha: 0.5)
        view.addSubview(topView)
        topView.snp.makeConstraints { make in
            make.left.right.top.equalTo(self.view)
            make.height.equalTo(64)
        }
        
        if self.backButtonVisibled {
            let backButton = UIButton.backButton()
            topView.addSubview(backButton)
            backButton.snp.remakeConstraints{ make in
                make.top.equalTo(30)
                make.left.equalTo(10)
                make.width.equalTo(100)
                make.height.equalTo(20)
            }
            backButton.rx.tap.subscribeNext{ _ in
                self.clickClose()
            }.addDisposableTo(self.disposeBag)
        }
        
        let exitButton = UIButton(type: .system)
        topView.addSubview(exitButton)
        exitButton.isHidden = true
        exitButton.setTitle("Выйти", for: UIControlState())
        let color = UIColor(red: 111/255, green: 186/255, blue: 1, alpha: 1)
        exitButton.setTitleColor(color, for: UIControlState())
        exitButton.contentHorizontalAlignment = .right
        exitButton.snp.remakeConstraints { make in
            make.top.equalTo(30)
            make.right.equalTo(topView).offset(-10)
            make.width.equalTo(100)
            make.height.equalTo(20)
        }
        exitButton.rx.tap.subscribeNext {
            let udef = UserDefaults.standard
            udef.removeObject(forKey: "sid")
            udef.synchronize()
            
            (UIApplication.shared.delegate as! AppDelegate).showLogin()
        }.addDisposableTo(self.disposeBag)
        
        var header = HeaderFooterView<UIView>(HeaderFooterProvider.class)
        header.onSetupView = { (view, _) -> () in
            view.backgroundColor = UIColor.backgroundColor()
            
            let headerView = NTVHeaderTitleView(frame: CGRect.zero, topTitle: "Регистрация", downTitle: "в стрингер")
            view.addSubview(headerView)
            headerView.snp.makeConstraints { make in
                make.left.top.right.equalTo(view)
                make.height.equalTo(NTVHeaderTitleView.height)
            }
            
            
        }
        header.height = { NTVHeaderTitleView.height + 20}
        section.header = header
        
        let setCellTextBorder = { (textField: UIView) -> Void in
            let bottomBorder = CALayer()
            bottomBorder.frame = CGRect(x: 0.0, y: textField.frame.size.height - 1, width: textField.frame.size.width, height: 1.0);
            bottomBorder.backgroundColor = UIColor.backgroundColor().lighter().cgColor
            textField.layer.addSublayer(bottomBorder)
        }
        
        section <<< TextRow("lastname") {
            $0.title = "Фамилия"
            //$0.cell.textField.attributedPlaceholder = NSAttributedString(string: "Фамилия",
            //    attributes:[NSForegroundColorAttributeName: UIColor.foregroundColor().darker()])
            $0.cell.textField.rx.text.subscribeNext{ value in
                self.anketa.lastName = value
            }.addDisposableTo(self.disposeBag)
            
        }.cellUpdate { (cell, row) in
            cell.backgroundColor = UIColor.backgroundColor()
            cell.textField.textColor = UIColor.white
            cell.textField.textAlignment = .right
            cell.textLabel?.textColor = UIColor.foregroundColor().darker()
            cell.height = { 80 }
            
            cell.layer.addSublayer(CALayer.makeBottomLine())
            //setCellTextBorder(cell.textField)
        }
        
        section <<< TextRow("firstname") {
            $0.title = "Имя"
            //$0.cell.textField.attributedPlaceholder = NSAttributedString(string: "Имя",
            //    attributes:[NSForegroundColorAttributeName: UIColor.foregroundColor().darker()])
            $0.cell.textField.rx.text.subscribeNext{ value in
                self.anketa.firstName = value
            }.addDisposableTo(self.disposeBag)
        }.cellUpdate { (cell, row) in
            cell.backgroundColor = UIColor.backgroundColor()
            cell.textField.textColor = UIColor.white
            cell.textField.textAlignment = .right
            cell.textLabel?.textColor = UIColor.foregroundColor().darker()
            cell.height = { 80 }
            
            cell.layer.addSublayer(CALayer.makeBottomLine())
            //setCellTextBorder(cell.textField)
        }
        section <<< TextRow("middlename") {
            $0.title = "Отчество"
            //$0.cell.textField.attributedPlaceholder = NSAttributedString(string: "Отчество",
            //    attributes:[NSForegroundColorAttributeName: UIColor.foregroundColor().darker()])
            $0.cell.textField.rx.text.subscribeNext{ value in
                self.anketa.middleName = value
            }.addDisposableTo(self.disposeBag)
        }.cellUpdate { (cell, row) in
            cell.textField.textAlignment = .right
            cell.backgroundColor = UIColor.backgroundColor()
            cell.textField.textColor = UIColor.white
            cell.textLabel?.textColor = UIColor.foregroundColor().darker()
            cell.height = { 80 }
            
            cell.layer.addSublayer(CALayer.makeBottomLine())
            //setCellTextBorder(cell.textField)
        }
        section <<< DateRow("birthday") {
            $0.title = "Дата рождения"
        }.cellUpdate { (cell, row) in
            cell.backgroundColor = UIColor.backgroundColor()
            //cell.textField.textColor = UIColor.whiteColor()
            cell.height = { 80 }
            cell.detailTextLabel?.textColor = UIColor.white
            cell.textLabel?.textColor = UIColor.foregroundColor().darker()
            
//            let width = self.view.frame.width
//            let line_width: CGFloat = 200
//            let x: CGFloat = width - line_width - 5
//            let bottomBorder = CALayer()
//            bottomBorder.frame = CGRectMake(x, 70, line_width, 1.0);
//            bottomBorder.backgroundColor = UIColor.backgroundColor().lighter().CGColor
//            cell.layer.addSublayer(bottomBorder)
            cell.layer.addSublayer(CALayer.makeBottomLine())
            
//            cell.datePicker.backgroundColor = UIColor.backgroundColor()
//            //cell.datePicker.tintColor = UIColor.whiteColor()
//            cell.datePicker.setValue(UIColor.greenColor(), forKeyPath: "textColor")
//            cell.datePicker.setValue(0.8, forKeyPath: "alpha")
        }.onChange{ row in
            
            if let date = row.value {
                self.anketa.birthDay = date.toString()
            }
        }
        
        if let r = regions {
            section <<< PickerInlineRow<String>("region") {
                $0.title = "Регион"
                $0.options = []
                for (_, region) in r.enumerated() {
                    $0.options.append(region[1])
                }
                
            }.cellUpdate { (cell, row) in
                cell.backgroundColor = UIColor.backgroundColor()
                cell.height = { 80 }
                cell.detailTextLabel?.textColor = UIColor.white
                cell.textLabel?.textColor = UIColor.foregroundColor().darker()
                if let region_id = self.anketa.region_id {
                    let value = self.regions!.filter { $0[0] == region_id.stringValue }[0][1]
                    row.cell.detailTextLabel?.text = value
                }
                
                //setCellTextBorder(cell.detailTextLabel!)
                
                cell.layer.addSublayer(CALayer.makeBottomLine())
            }.onChange{ row in
                if let region = row.value {
                    let key = r.filter { $0[1] == region }[0][0]
                    self.anketa.region_id = NSNumber(value: Int(key)! as Int)
                }
                
            }
        }
        
        section <<< EmailRow("email") {
            $0.title = "E-mail"
//            $0.cell.textField.attributedPlaceholder = NSAttributedString(string: "E-mail",
//                attributes:[NSForegroundColorAttributeName: UIColor.foregroundColor().darker()])
            $0.cell.textField.rx.text.subscribeNext{ value in
                self.anketa.email = value
                }.addDisposableTo(self.disposeBag)
        }.cellUpdate { (cell, row) in
            cell.textField.textAlignment = .right
            cell.backgroundColor = UIColor.backgroundColor()
            cell.textField.textColor = UIColor.white
            cell.textLabel?.textColor = UIColor.foregroundColor().darker()
            cell.height = { 80 }
            //setCellTextBorder(cell.textField)
            
            cell.layer.addSublayer(CALayer.makeBottomLine())
        }
        
        section <<< PhoneRow("phone") {
            $0.title = "Телефон"
//            $0.cell.textField.attributedPlaceholder = NSAttributedString(string: "Телефон",
//                attributes:[NSForegroundColorAttributeName: UIColor.foregroundColor().darker()])
            $0.cell.textField.rx.text.subscribeNext{ value in
                self.anketa.phone = value
                }.addDisposableTo(self.disposeBag)
        }.cellUpdate { (cell, row) in
            cell.textField.textAlignment = .right
            cell.backgroundColor = UIColor.backgroundColor()
            cell.textField.textColor = UIColor.white
            cell.textLabel?.textColor = UIColor.foregroundColor().darker()
            cell.height = { 80 }
            //setCellTextBorder(cell.textField)
            cell.layer.addSublayer(CALayer.makeBottomLine())
        }
        
//        section <<< CheckPermissionRow("check1") { _ in
//            
//        }.cellUpdate { (cell, row) in
//            cell.labelTitle.text = "На передачу\nвидеоматериалов согласен"
//            cell.backgroundColor = UIColor.backgroundColor()
//            cell.height = { 80 }
//        }.onCellSelection { (cell, row) in
//            cell.checked = !cell.checked
//            cell.checkImage.image = UIImage(named: cell.checked ? "button_checkon" : "button_checkoff")
//            cell.selected = false
//            self.check1 = cell.checked
//        }
        
        section <<< CheckPermissionRow("check1") { _ in
            
        }.cellUpdate { (cell, row) in
            cell.labelTitle.text = "С правилами на обработку\nданных согласен"
            cell.backgroundColor = UIColor.backgroundColor()
            cell.height = { 80 }
            
            let license = UserDefaults.license()
            if license {
                cell.checked = license
            } else {
                cell.checked = false
            }
            cell.checkImage.image = UIImage(named: cell.checked ? "button_checkon" : "button_checkoff")
            self.check1 = cell.checked
        }.onCellSelection { (cell, row) in
            cell.isSelected = false
            //let licenseControler = NTVLicenseViewController.makeNavigationController()
            //self.presentViewController(licenseControler, animated: true, completion: nil)
        }
        
        section <<< ButtonRow("upload_button") {
            $0.title = "Сохранить"
            $0.cell.height = { 80 }
        }.cellUpdate { (cell, row) in
            cell.backgroundColor = UIColor.backgroundColor()
            
            cell.textLabel?.textColor = UIColor.buttonTitleColor()
            cell.textLabel?.textAlignment = .left
            cell.textLabel?.font = UIFont.systemFont(ofSize: 20)
            
        }.onCellSelection{ (cell, row) in
            if self.isUploadEnabled() {
                NTVWebServiceManager.sharedManager.writeAnketa(self.anketa).subscribe(
                    onNext: { result in
                        print(result)
                        UserDefaults.set_anketa()
                        self.clickClose()
                    }, onError: { error in
                        self.showError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            }
        }

        section <<< ButtonRow("logout_button") {
            $0.title = "Разлогиниться"
            $0.cell.height = { 80 }
            }.cellUpdate { (cell, row) in
                cell.backgroundColor = UIColor.backgroundColor()
                
                cell.textLabel?.textColor = UIColor.buttonDeleteTitleColor()
                cell.textLabel?.textAlignment = .left
                cell.textLabel?.font = UIFont.systemFont(ofSize: 20)
                
            }.onCellSelection{ (cell, row) in
                
                let udef = UserDefaults.standard
                udef.removeObject(forKey: "sid")
                udef.synchronize()
                
                let storage = HTTPCookieStorage.shared
                if let cookies = storage.cookies {
                    for cookie in cookies {
                        storage.deleteCookie(cookie)
                    }
                    UserDefaults.standard.synchronize()
                }
                
                (UIApplication.shared.delegate as! AppDelegate).showLogin()
        }

        
        form +++ section
        
        getUserInfo()
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func clickClose() {
        self.navigationController?.dismiss(animated: true, completion: {
            var empty = false
            repeat {
                if (self.anketa.firstName == nil || self.anketa.firstName!.isEmpty)
                    && (self.anketa.middleName == nil || self.anketa.middleName!.isEmpty)
                    && (self.anketa.lastName == nil || self.anketa.lastName!.isEmpty) {
                    empty = true
                    break
                }
                if self.anketa.phone == nil || self.anketa.phone!.isEmpty {
                    empty = true
                    break
                }
            } while false
        
            let appdelegate = UIApplication.shared.delegate as? AppDelegate
            if empty {
                UserDefaults.set_need_fmessage()
                NotificationCenter.default
                    .post(name: Notification.Name(rawValue: notification_set_first_message), object: nil)
                //appdelegate?.startDelayed()
            } else {
                UserDefaults.set_need_fmessage(false)
                appdelegate?.stopDelayed()
            }
            if !UserDefaults.not_fmessage() {
                NotificationCenter.default.post(name: Notification.Name(rawValue: notification_set_first_message), object: nil)
                UserDefaults.set_not_fmessage()
            }
        })
    }
    
    func isUploadEnabled() -> Bool {
//        let check1 = form.rowByTag("check1")
//        let enabled: Bool = (form.rowByTag("check1") as! CheckPermissionRow).cell.checked && (form.rowByTag("check2") as! CheckPermissionRow).cell.checked
        let enabled = check1// && check2
        return enabled
    }
    
    func getUserInfo() {
        NTVWebServiceManager.sharedManager.readAnketa().subscribe(
        onNext: { result in
            self.anketa = result as! NTVAnketa
            
            let lastNameRow = self.form.rowBy(tag: "lastname") as! TextRow
            lastNameRow.value = self.anketa.lastName
            
            let firstNameRow = self.form.rowBy(tag: "firstname") as! TextRow
            firstNameRow.value = self.anketa.firstName
            
            let middleNameRow = self.form.rowBy(tag: "middlename") as! TextRow
            middleNameRow.value = self.anketa.middleName
            
            let emailRow = self.form.rowBy(tag: "email") as! EmailRow
            emailRow.value = self.anketa.email
            
            let phoneNameRow = self.form.rowBy(tag: "phone") as! PhoneRow
            phoneNameRow.value = self.anketa.phone
            
            if let date = self.anketa.birthDay , date != "01.01.0001" {
                let birthdayRow = self.form.rowBy(tag: "birthday") as! DateRow
                birthdayRow.value = Date.toDate(date)
                birthdayRow.cell.detailTextLabel?.text = date
            }
            
            if let region_id = self.anketa.region_id {
                let value = self.regions!.filter { $0[0] == region_id.stringValue }[0][1]
                let regionRow = self.form.rowBy(tag: "region") as! PickerInlineRow<String>
                regionRow.cell.detailTextLabel?.text = value
            }
            
            self.section.reload()
            
        }, onError: { error in
            
        }
        ).addDisposableTo(self.disposeBag)
    }

}
