//
//  NTVView.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 21/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit

class NTVView: UIView {

    var didLayout:(() -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func prepare_view() {
        self.backgroundColor = UIColor.red
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let didLayout = self.didLayout {
            didLayout()
        }
    }
    
}
