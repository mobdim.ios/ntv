//
//  NTVSaveVideoViewController.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 30/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit
import MediaPlayer
import SnapKit
import RxSwift
import RxCocoa
import Eureka
import MRProgress

class NTVSaveVideoViewController: FormViewController, UITextViewDelegate {

    let disposeBag = DisposeBag()
    
    var pushed: Bool = false
    var index: Int = -1
    let labelName = UILabel()
    let labelDescription = UILabel()
    let textName = UITextField()
    let textDescription = UITextField()
    let buttonSend = UIButton()
    let viewMovieContainer = UIView()
    var movie = NTVMovieModel() {
        didSet {
            self.textName.text = movie.name
            self.textDescription.text = movie.descript
            
//            let fullPath: String = NTVLocalFilesManager.sharedManager!.fullPathByFile(movie.fullFileNameStorage!)!
//            let fileURL: NSURL = NSURL.fileURLWithPath(fullPath)
//            print("Play local file: \(fileURL)")
//            self.moviePlayerController = MPMoviePlayerController(contentURL: fileURL)
//            if let player = self.moviePlayerController {
//                player.view.frame = self.viewMovieContainer.bounds
//                player.movieSourceType = .File
//                player.shouldAutoplay = true
//                player.scalingMode = .AspectFit
//                player.movieSourceType = .File
//                player.prepareToPlay()
//                
//                self.viewMovieContainer.addSubview(player.view)
//        
//                NSNotificationCenter.defaultCenter().rx.notification(MPMoviePlayerPlaybackDidFinishNotification).subscribeNext { (notification) in
//                    self.moviePlayerController?.contentURL = fileURL
//                    self.moviePlayerController?.shouldAutoplay = false
//                    self.moviePlayerController?.prepareToPlay()
//                }.addDisposableTo(self.disposeBag)
//            }

            let nameRow = form.rowBy(tag: "name") as! TextRow
            nameRow.value = movie.name
            
            let descriptionRow = form.rowBy(tag: "description") as! TextAreaRow
            descriptionRow.value = movie.descript
            
            movie.thumbnailImageWithCompletion { (image, _) in
                if let thimage = self.thumbImage {
                    thimage.image = image
                }
            }

        }
    }
    
    var moviePlayerController: MPMoviePlayerController?
    let buttonDelete = UIButton()
    
    var thumbImage: UIImageView?
    
    override func loadView() {
        super.loadView()
        view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor.white
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //self.moviePlayerController!.stop()
        super.viewDidDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(clickClose))
        
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 50.0 / 255.0, green: 50.0 / 255.0, blue: 50.0 / 255.0, alpha: 1.0)
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.labelName.text = NSLocalizedString("name", comment: "Save")
        self.labelDescription.text = NSLocalizedString("description", comment: "Save")
        self.buttonSend.setTitle(NSLocalizedString("send", comment: "Save"), for: UIControlState())
        
        tableView?.separatorStyle = .none
        tableView?.backgroundColor = UIColor.backgroundColorDark()
        
        let topView = UIView()
        topView.backgroundColor = UIColor.backgroundColorDark(alpha: 0.5)
        view.addSubview(topView)
        topView.snp.makeConstraints { make in
            make.left.right.top.equalTo(self.view)
            make.height.equalTo(64)
        }
        
        let backButton = UIButton.backButton()
        topView.addSubview(backButton)
        backButton.snp.remakeConstraints { make in
            make.top.equalTo(30)
            make.left.equalTo(10)
            make.width.equalTo(100)
            make.height.equalTo(20)
        }
        backButton.rx.tap.subscribeNext{ _ in
            self.clickClose()
        }.addDisposableTo(self.disposeBag)
        
        self.viewMovieContainer.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 200)
        
        createContent()
                
        if self.index != -1 {
            NTVLocalFilesManager.sharedManager.refreshLocal()
            self.movie = NTVLocalFilesManager.sharedManager.movies[self.index]
        }
        self.textName.background = UIImage(named: "textfield_background.png")
        self.textDescription.background = UIImage(named: "textfield_background.png")
        self.title = NSLocalizedString("titleSave", comment: "Save")
        
        self.buttonDelete.rx.tap.asObservable().subscribeNext {
            NTVLocalFilesManager.sharedManager.removeLocalMovieAtIndex(self.index)
            self.navigationController!.dismiss(animated: true, completion: nil)
        }.addDisposableTo(self.disposeBag)
    }
    
    func clickClose(updated: Bool = true) {
        if updated {
            updateVideo()
        }
        
        if self.pushed {
            self.navigationController!.popViewController(animated: true)
        } else {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    fileprivate func createContent() {
        
        let section = Section()
        var header = HeaderFooterView<UIView>(HeaderFooterProvider.class)
        header.onSetupView = { (view, _) -> () in
            view.backgroundColor = UIColor.backgroundColor()
            
            let headerView = NTVHeaderTitleView(frame: CGRect.zero, topTitle: "Сохранённое", downTitle: "видео")
            view.addSubview(headerView)
            headerView.snp.makeConstraints { make in
                make.left.top.right.equalTo(view)
                make.height.equalTo(NTVHeaderTitleView.height)
            }
            headerView.countLabel.text = "\(NTVLocalFilesManager.sharedManager.movies.count)"
            
        }
        header.height = { NTVHeaderTitleView.height + 20}
        section.header = header
        
        form +++ section
        
        let video = Section()
//        {
//            var header = HeaderFooterView<UIView>(HeaderFooterProvider.Class)
//            header.onSetupView = { (view, section, form) -> () in
//                view.backgroundColor = UIColor.backgroundColor()
//                
//                view.addSubview(self.viewMovieContainer)
//                
//            }
//            header.height = { return 200 }
//            $0.header = header
//        }
        
        video <<< VideoRow() { row in
            row.cell.height = { 260 }
            row.cell.selectionStyle = .none
            self.thumbImage = row.cell.thumbImage
        }.onCellSelection { (cell, row) in
            
            let fullPath: String = NTVLocalFilesManager.sharedManager.fullPathByFile(self.movie.fullFileNameStorage!)!
            let fileURL: URL = URL(fileURLWithPath: fullPath)
            print("Play local file: \(fileURL)")

            let moviePlayerViewController = MPMoviePlayerViewController(contentURL: fileURL)
            moviePlayerViewController!.moviePlayer.controlStyle = .fullscreen
            moviePlayerViewController!.moviePlayer.shouldAutoplay = true
            moviePlayerViewController!.moviePlayer.setFullscreen(true, animated: true)
            moviePlayerViewController!.modalTransitionStyle = .crossDissolve
            moviePlayerViewController!.moviePlayer.scalingMode = .none
            moviePlayerViewController!.moviePlayer.movieSourceType = .file
            NotificationCenter.default
                .rx.notification(NSNotification.Name.MPMoviePlayerPlaybackStateDidChange, object: moviePlayerViewController)
                .subscribeNext { (notification) in
                    let mpViewController = notification.object as! MPMoviePlayerViewController
                    if mpViewController.moviePlayer.loadState == .playable && (mpViewController.moviePlayer.playbackState != .playing) {
                        mpViewController.moviePlayer.play()
                    }
                }.addDisposableTo(self.disposeBag)
            NotificationCenter.default
                .rx.notification(NSNotification.Name.MPMoviePlayerPlaybackDidFinish, object: moviePlayerViewController)
                .subscribeNext { (notification) in
                    self.dismissMoviePlayerViewControllerAnimated()
                }.addDisposableTo(self.disposeBag)
            self.presentMoviePlayerViewControllerAnimated(moviePlayerViewController)
            
        }.cellUpdate { (cell, row) in
            cell.backgroundColor = UIColor.backgroundColorDark()
        }
        
        video <<< TextRow("name") {
            $0.cell.textField.attributedPlaceholder = NSAttributedString(string:"Название",
                attributes:[NSForegroundColorAttributeName: UIColor.foregroundColor().darker()])
            $0.cell.textField.rx.text.subscribeNext{ value in
                self.movie.name = value
            }.addDisposableTo(self.disposeBag)
        }.cellUpdate { (cell, row) in
            cell.backgroundColor = UIColor.backgroundColor()
            cell.textField.textColor = UIColor.white
            cell.height = { 80 }
        }
        
        video <<< TextAreaRow("description") {
//            $0.cell.textView.attributedPlaceholder = NSAttributedString(string:"Описание",
//                attributes:[NSForegroundColorAttributeName: UIColor.foregroundColor().darker()])
            $0.cell.textView.rx.text.subscribeNext{ value in
                self.movie.descript = value
            }.addDisposableTo(self.disposeBag)
            $0.cell.textView.delegate = self
        }.cellUpdate { (cell, row) in
            if cell.textView.text.isEmpty {
                cell.textView.text = "Описание"
                cell.textView.textColor = UIColor.foregroundColor().darker()
            } else {
                cell.textView.textColor = UIColor.white
            }
            cell.textView.backgroundColor = UIColor.clear
            cell.backgroundColor = UIColor.backgroundColor()
            cell.height = { 80 }
        }
        
        video <<< ButtonRow() {
            $0.cell.height = { 80 }
        }.cellUpdate { [unowned self] (cell, row) in
            cell.textLabel?.text = self.movie.uploaded ? "Сохранить" : "Отправить"
            cell.backgroundColor = UIColor.backgroundColor()
            cell.height = { 80 }
            cell.textLabel?.textColor = UIColor.buttonTitleColor()
            cell.textLabel?.textAlignment = .left
            cell.textLabel?.font = UIFont.systemFont(ofSize: 20)
            cell.accessoryView = UIImageView(image: UIImage(named: "button_rightarrow"))
        }.onCellSelection { [unowned self] (cell, row) in
            if self.movie.uploaded {
                self.updateVideo()
            } else {
                NTVWebServiceManager.sharedManager.canUploadMovie(self.movie.name!, description: self.movie.descript!)
                    .subscribe(
                        onNext: { [unowned self] response in
                            let result = response as! NTVNetworkResult
                            self.movie.id = result.id!.int32Value
                            
                            MRProgressOverlayView.showOverlayAdded(to: self.navigationController?.view, animated: true)
                            NTVWebServiceManager.sharedManager.uploadMovie(self.movie).subscribe(
                                onNext: { [unowned self] _ in
                                    self.movie.uploaded = true
                                    self.updateVideo()
                                    MRProgressOverlayView.dismissOverlay(for: self.navigationController?.view, animated: true)
                                    self.navigationController?.popViewController(animated: true)
                                }, onError: { [unowned self] _ in
                                    MRProgressOverlayView.dismissOverlay(for: self.navigationController?.view, animated: true)
                                }
                            ).addDisposableTo(self.disposeBag)
                        }, onError: { error in
                            print("\(error)")
                        }
                    ).addDisposableTo(self.disposeBag)
            }
        }
        
        video <<< ButtonRow() {
            $0.title = "Удалить"
            $0.cell.height = { 80 }
        }.cellUpdate { (cell, row) in
            cell.backgroundColor = UIColor.backgroundColor()
            cell.height = { 80 }
            cell.textLabel?.textColor = UIColor.buttonDeleteTitleColor()
            cell.textLabel?.textAlignment = .left
            cell.textLabel?.font = UIFont.systemFont(ofSize: 20)
        }.onCellSelection{ [unowned self] (cell, row) in
            NTVLocalFilesManager.sharedManager.removeLocalMovieAtIndex(self.index)
            self.clickClose(updated: false)
        }
        
        form +++ video
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
    }
    
    fileprivate func updateVideo() {
         NTVLocalFilesManager.sharedManager.updateLocalMove(self.movie, atIndex: self.index)
    }
    
    // MARK: - Placeholder TextView
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.foregroundColor().darker() {
            textView.text = nil
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Описание"
            textView.textColor = UIColor.foregroundColor().darker()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:NSString = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with:text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {
            
            textView.text = "Описание"
            textView.textColor = UIColor.foregroundColor().darker()
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            
            return false
        }
            
            // Else if the text view's placeholder is showing and the
            // length of the replacement string is greater than 0, clear
            // the text view and set its color to black to prepare for
            // the user's entry
        else if textView.textColor == UIColor.foregroundColor().darker() && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.white
        }
        
        return true
    }
    
}
