//
//  TestTableView.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 15/04/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit
import SnapKit

class BottomView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class TestTableView: UIView, UITableViewDataSource, UITableViewDelegate {

    let tableView = UITableView()
    let h: CGFloat = 900
    
    var bottom_view: BottomView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        initControls()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func initControls() {
        tableView.dataSource = self
        tableView.delegate = self
        
        self.tableView.alwaysBounceVertical = false
        self.tableView.bounces = false
        
        addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.left.right.top.bottom.equalTo(self)
        }
    }
 
    // MARK: - UITableView
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let w = UIScreen.main.bounds.width
        let v = UIView(frame: CGRect(x: 0, y: 0, width: w, height: h))
        v.backgroundColor = UIColor.blue
        
        let l = UILabel()
        v.addSubview(l)
        l.textColor = UIColor.white
        l.text = "Название"
        l.snp.makeConstraints { make in
            make.top.equalTo(200)
            make.left.equalTo(50)
            make.width.equalTo(200)
            make.height.equalTo(30)
        }
        
        let b = BottomView()
        b.backgroundColor = UIColor.green
        v.addSubview(b)
        b.snp.makeConstraints { make in
            make.left.bottom.right.equalTo(v)
            make.height.equalTo(200)
        }
        
        self.bottom_view = b
        
        return v
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return h
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    // MARK: - Layout
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}
