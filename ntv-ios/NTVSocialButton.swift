//
//  NTVSocialButton.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 25/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit
import SnapKit

typealias TapButton = ((Void) -> Void)

enum SocialButtons {
    case odk
    case vk
    case fb
    case tw
    case gp
}

class NTVSocialButton: UIView {

    var tap: TapButton?
    let image = UIImageView()
    
    init(social: SocialButtons) {
        super.init(frame: CGRect.zero)
        var imageName: String = ""
        switch social {
        case .odk:
            imageName = "button_odk"
        case .vk:
            imageName = "button_vk"
        case .fb:
            imageName = "button_fb"
        case .tw:
            imageName = "button_tw"
        case .gp:
            imageName = "button_gp"
        }
        
        image.isUserInteractionEnabled = false
        image.image = UIImage(named: imageName)
        image.highlightedImage = UIImage(named: imageName + "_hl")
        self.addSubview(image)
        image.snp.makeConstraints { make in
            make.center.equalTo(self)
            make.width.height.equalTo(36)
        }
        
        self.backgroundColor = UIColor.backgroundColor().lighter()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Touches
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchDown()
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchUp()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchUp()
    }
    
    fileprivate func touchDown() {
        image.isHighlighted = true
    }
    
    fileprivate func touchUp() {
        image.isHighlighted = false
        if let t = tap {
            t()
        }
    }
    
    
    
}
