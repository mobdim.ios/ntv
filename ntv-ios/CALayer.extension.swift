//
//  CALayer.extension.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 20/04/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit

extension CALayer {
    static func makeBottomLine() -> CALayer {
        let width = UIScreen.main.bounds.width
        let line_width: CGFloat = 200
        let x: CGFloat = width - line_width - 5
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: x, y: 70, width: line_width, height: 1.0);
        bottomBorder.backgroundColor = UIColor.backgroundColor().lighter().cgColor
        return bottomBorder
    }
}
