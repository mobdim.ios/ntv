//
//  UIDevice.extension.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 05/05/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit

extension UIDevice {
    static var interfaceOrientation: UIInterfaceOrientationMask {
        get {
            let device_orientation = UIDevice.current.orientation
            
            var interface_orientation = UIInterfaceOrientationMask.portrait
            switch device_orientation {
            case .portrait:
                interface_orientation = UIInterfaceOrientationMask.portrait
            case .portraitUpsideDown:
                interface_orientation = UIInterfaceOrientationMask.portraitUpsideDown
            case .landscapeLeft:
                interface_orientation = UIInterfaceOrientationMask.landscapeRight
            case .landscapeRight:
                interface_orientation = UIInterfaceOrientationMask.landscapeLeft
            default:
                ()
            }
            
            return interface_orientation
        }
    }
}
