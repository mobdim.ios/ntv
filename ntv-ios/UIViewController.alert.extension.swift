//
//  UIViewController.alert.extension.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 01/04/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit

enum ErrorText: String {
    case title_error = "Ошибка"
    case title_ok = "OK"
    case title_critical_error = "Критическая ошибка"
}

extension UIViewController {
    
    func showError(_ error: Error) {
        showError(nserror: error as NSError)
    }
    
    func showError(nserror error: NSError) {
        let alert = UIAlertController(title: ErrorText.title_error.rawValue, message: error.localizedDescription, preferredStyle: .alert)
        let okAction = UIAlertAction(title: ErrorText.title_ok.rawValue, style: .default, handler: nil)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showCriticalError() {
        print(ErrorText.title_critical_error.rawValue)
        
        let alert = UIAlertController(title:ErrorText.title_critical_error.rawValue, message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: ErrorText.title_ok.rawValue, style: .default, handler: nil)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showFirstMessage(_ handler: ((UIAlertAction) -> Void)? = nil) {
        let message = "Уважаемый пользователь, для выплаты вам вознаграждения, в случае нашей заинтересованности в вашем видео, пожалуйста заполните правильно анкету о себе, чтобы мы могли связаться с вами."

        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: ErrorText.title_ok.rawValue, style: .default, handler: handler)
        alert.addAction(okAction)
        
        var vc: UIViewController? = nil
        if let _ = self.presentedViewController {
            vc = self.presentedViewController!
        }
        
        if let _ = self.navigationController , vc == nil {
            vc = self.navigationController!
        }
        
        if vc == nil {
            vc = self
        }
        
        vc!.present(alert, animated: true, completion: nil)
    }
}

