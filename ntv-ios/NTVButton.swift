//
//  NTVButton.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 24/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit

class NTVButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setTitleColor(UIColor.buttonTitleColor(), for: UIControlState())
        self.setTitleColor(UIColor.white, for: .highlighted)
        self.titleLabel?.numberOfLines = 2
        self.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        self.contentHorizontalAlignment = .left
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
