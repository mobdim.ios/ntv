//
//  NTVLoginNavigationController.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 05/04/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit

class NTVLoginNavigationController: UINavigationController {

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        var needShowLicense = false
        
        let udef = UserDefaults.standard
        let license = udef.object(forKey: "license")
        
        if license == nil {
            needShowLicense = true
        }
        
        if let l = license , (l as AnyObject).boolValue == false {
            needShowLicense = true
        }
        
        if needShowLicense {
            let licenseControler = NTVLicenseViewController.makeNavigationController()
            self.present(licenseControler, animated: true, completion: nil)
        }

    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return [.portrait, .portraitUpsideDown]
    }
}
