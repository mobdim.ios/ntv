//
//  NTVLicenseViewController.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 04/05/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import UIKit

class NTVLicenseNavigationController: UINavigationController {
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
}

class NTVLicenseViewController: UIViewController {

    static func makeNavigationController() -> NTVLicenseNavigationController {
        let anketaViewController = NTVLicenseViewController()
        let navigationController = NTVLicenseNavigationController(rootViewController: anketaViewController)
        
        return navigationController
    }
    
    override func loadView() {
        super.loadView()
        
        view = NTVLicenseView(frame: UIScreen.main.bounds)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController!.navigationBar.barTintColor = UIColor.backgroundColorDark()
        navigationController!.navigationBar.tintColor = UIColor.white
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.text = "Лицензионное\nсоглашение"
        self.navigationItem.titleView = label
        
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 15)]
        let cancelButton = UIBarButtonItem(title: "Отменить", style: .plain, target: self, action: #selector(cancel))
        let applyButton = UIBarButtonItem(title: "Соглашаюсь", style: .plain, target: self, action: #selector(apply))
        cancelButton.setTitleTextAttributes(attributes, for: UIControlState())
        applyButton.setTitleTextAttributes(attributes, for: UIControlState())
        
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem = applyButton
    }
    
    func cancel() {
        exit(0)
    }
    
    func dismiss() {
        self.navigationController!.dismiss(animated: true, completion: nil)
    }
    
    func apply() {
        UserDefaults.set_license()
        dismiss()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
