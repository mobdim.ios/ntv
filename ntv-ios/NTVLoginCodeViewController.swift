//
//  NTVLoginCodeViewController.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 30/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import UIKit
import Eureka
import SnapKit
import RxSwift
import RxCocoa

class NTVLoginCodeViewController: FormViewController {
    
    let disposeBag = DisposeBag()
    
    var waitEmailCode = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.modalTransitionStyle = .flipHorizontal
        
        self.tableView?.backgroundColor = UIColor.backgroundColor()
        
        form +++ Section() {
            var header = HeaderFooterView<UIView>(HeaderFooterProvider.class)
            header.onSetupView = { (view, _) -> () in
                view.backgroundColor = UIColor.backgroundColor()
                
                let headerView = NTVHeaderTitleView(frame: CGRect.zero, topTitle: "Регистрация", downTitle: "в стрингер")
                view.addSubview(headerView)
                headerView.snp.makeConstraints { make in
                    make.left.top.right.equalTo(view)
                    make.height.equalTo(NTVHeaderTitleView.height)
                }
                
                let textCodeField = NTVTextField(frame: CGRect(x: 0, y: 0, width: 0, height: 40))
                textCodeField.becomeFirstResponder()
                textCodeField.keyboardType = .numberPad
                textCodeField.textColor = UIColor.white
                textCodeField.attributedPlaceholder = NSAttributedString(string:"Введите код",
                    attributes:[NSForegroundColorAttributeName: UIColor.foregroundColor().darker()])
                view.addSubview(textCodeField)
                textCodeField.snp.makeConstraints { make in
                    make.top.equalTo(headerView.snp.bottom).offset(20)
                    make.left.equalTo(40)
                    make.right.equalTo(-20)
                    make.height.equalTo(40)
                }
                
                let buttonLogin = NTVButton()
                buttonLogin.setTitle("Войти", for: UIControlState())
                view.addSubview(buttonLogin)
                buttonLogin.snp.makeConstraints { make in
                    make.top.equalTo(textCodeField.snp.bottom).offset(30)
                    make.left.equalTo(40)
                    make.right.equalTo(-40)
                    make.height.equalTo(40)
                }
                buttonLogin.rx.tap.asObservable().subscribeNext {
                    let push_id = (UIApplication.shared.delegate as! AppDelegate).push_id
                    NTVWebServiceManager.sharedManager.login(textCodeField.text!, push_id: push_id, os: "ios").subscribe(
                        onNext: { response in
                            (UIApplication.shared.delegate as! AppDelegate).showCamera()
                        }, onError: { error in
                            self.showError(error as NSError)
                        }
                        ).addDisposableTo(self.disposeBag)
                }.addDisposableTo(self.disposeBag)
            }
            header.height = { return 300 }
            $0.header = header
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
        return .portrait
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
