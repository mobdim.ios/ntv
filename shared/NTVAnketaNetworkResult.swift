//
//  NTVAnketaNetworkResult.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 28/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Foundation
import ObjectMapper

class NTVAnketaNetworkResult: NTVNetworkResult {
    
    var anketa = NTVAnketa()
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        var result = NSDictionary()
        result <- map["result"]
        
        anketa.firstName = result["firstname"] is NSNull ? nil : result["firstname"] as? String
        anketa.lastName = result["lastname"] is NSNull ? nil : result["lastname"] as? String
        anketa.middleName = result["middlename"] is NSNull ? nil : result["middlename"] as? String
        anketa.phone = result["phone"] is NSNull ? nil : result["phone"] as? String
        anketa.email = result["email"] is NSNull ? nil : result["email"] as? String
        anketa.birthDay = result["birthday"] is NSNull ? nil : result["birthday"] as? String
        anketa.region = result["region"] is NSNull ? nil : result["region"] as? String
        anketa.region_id = result["region_id"] is NSNull ? nil : result["region_id"] as? NSNumber
    }
}
