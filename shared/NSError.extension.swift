//
//  NSError.extension.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 28/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Foundation

extension NSError {
    
    enum NTVError: String {
        case error_no
        case error_upload
        case error_vk_validate
        case error_unknown
    }
    
    convenience init(domain: Any, error: NTVError) {
        
        self.init(domain: String(describing: domain),
                  code: -1,
                  userInfo: [NSLocalizedDescriptionKey: String(describing: error)])
        
    }
    
    convenience init(domain: Any, networkResult result: NTVNetworkResult) {
        var error_code = -1
        if result.error_code != nil {
            error_code = result.error_code!.intValue
        }
        var error_message = result.error_message
        if result.error_description != nil {
            error_message = result.error_description!
        }
        self.init(domain: String(describing: domain),
                  code: error_code,
                  userInfo: [NSLocalizedDescriptionKey: error_message])
    }
    
    convenience init(domain: Any, errorCode: Int, errorMessage: String) {
        self.init(domain: String(describing: domain),
                  code: errorCode,
                  userInfo: [NSLocalizedDescriptionKey: errorMessage])
    }

}
