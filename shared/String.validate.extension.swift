//
//  String.validate.extension.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 26/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Foundation

extension String {
    func isEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isPhone() -> Bool {
        let set = CharacterSet(charactersIn: "+0123456789")
        
        if let _ = self.rangeOfCharacter(from: set) {
            return true
        }
        return false
    }
}
