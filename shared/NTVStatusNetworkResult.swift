//
//  NTVStatusNetworkResult.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 14/09/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Foundation
import ObjectMapper

class NTVStatusNetworkResult: NTVNetworkResult {
    
    var statuses = [Int: String]()
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        var result = [[String: AnyObject]]()
        result <- map["result"]
        guard result.count > 0 else {
            return
        }
        
        for dict: [String: AnyObject] in result {
            let id: Int = dict["id"] as! Int
            let status: String = dict["status"] as! String
            statuses[id] = status
        }        
    }
}
