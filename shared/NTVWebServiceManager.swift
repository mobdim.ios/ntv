//
//  NTVWebServiceManager.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 28/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import RxCocoa
import ObjectMapper

typealias NetworkParameters = [String: AnyObject]

struct NTVAnketa {
    var firstName: String?
    var middleName: String?
    var lastName: String?
    var birthDay: String?
    var region: String?
    var email: String?
    var phone: String?
    var city: String?
    var nationality: String?
    var address: String?
    var occupation_id: NSInteger = -1
    var region_id: NSNumber?
    
    func toTitleButton() -> String {
        return "\(firstName ?? "") \(lastName ?? "")"
    }
    
    func toParameters() -> NetworkParameters {
        var parameters = NetworkParameters()
        
        if let fn = self.firstName {
            parameters["firstname"] = fn as AnyObject?
        }
        if let mn = self.middleName {
            parameters["middlename"] = mn as AnyObject?
        }
        if let ln = self.lastName {
            parameters["lastname"] = ln as AnyObject?
        }
        if let bd = self.birthDay {
            parameters["birthday"] = bd as AnyObject?
        }
//        if let reg = self.region {
//            parameters["region"] = reg
//        }
        if let em = self.email {
            parameters["email"] = em as AnyObject?
        }
        if let ph = self.phone {
            parameters["phone"] = ph as AnyObject?
        }
        if let c = self.city {
            parameters["city"] = c as AnyObject?
        }
        if let n = self.nationality {
            parameters["nationality"] = n as AnyObject?
        }
        if let addr = self.address {
            parameters["address"] = addr as AnyObject?
        }
        if occupation_id != -1 {
            parameters["occupation_id"] = "\(occupation_id)" as AnyObject?
        }
        
        if let r_id = region_id {
            parameters["region"] = r_id.stringValue as AnyObject?
        }
        
        
        return parameters
    }
}

class NTVWebServiceManager: NSObject {
    let disposeBag = DisposeBag()
    //let kUrlWebService = "http://stringerapi.emer.link/ws/"
    let kUrlWebService = "http://mob.ntv.ru/ws/"
    
    static let sharedManager = NTVWebServiceManager()
    //fileprivate var manager: Manager!
    
    override init() {
        super.init()
        //self.manager = Alamofire.Manager.sharedInstance
    }
    
    var login: String = ""
}

// MARK: - Ништяки

extension NTVWebServiceManager {
    
    func _request(_ method: Alamofire.HTTPMethod, url: String, parameters: NetworkParameters? = nil, autoSendError: Bool = true) -> Observable<Any> {
        return Observable.create { observer in
            
            print("==> request: \(url)")
            
            
            Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default)//JSONEncoding.default)
            //Alamofire.request(method, url, parameters: parameters, encoding: .JSON)
                .responseJSON(completionHandler: { response in
                    print("<== response: \(response)")
                    
                    if let err = response.result.error {
                        observer.onError(err)
                        return
                    }
                    
                    if let json = response.result.value {
                        let d = json as! NSDictionary
                        if let token = d["access_token"] {
                            observer.onNext(token)
                            observer.onCompleted()
                        } else {
                            if let result = Mapper<NTVNetworkResult>().map(JSON: json as! [String : Any]) {
                                result.json = json as? NSDictionary
                                if result.isNoError() {
                                    observer.onNext(result)
                                    observer.onCompleted()
                                } else {
                                    let error = NSError(domain: NTVWebServiceManager.self, networkResult: result)
                                    print("\(error)")
                                    observer.onError(error)
                                }
                            } else {
                                observer.onError(NSError(domain: NTVWebServiceManager.self(), error: .error_unknown))
                            }
                        }
                    } else {
                        observer.onError(NSError(domain: NTVWebServiceManager.self(), error: .error_unknown))
                    }
                })
            
//            requestJSON(method, url, parameters: parameters)
//                .observeOn(MainScheduler.instance)
//                .subscribe (
//                    onNext: { json in
//                        
//                        print("<== response: \(json)")
//                        
//                        if let result = Mapper<NTVNetworkResult>().map(json.1) {
//                            result.response = json.0
//                            
//                            result.json = json.1 as? NSDictionary
//                            
//                            if result.isNoError() {
//                                observer.onNext(result)
//                                observer.onCompleted()
//                            } else {
//                                let error = NSError(domain: NTVWebServiceManager.self, networkResult: result)
//                                DDLogError("\(error)")
//                                observer.onError(error)
//                            }
//                        }
//                    },
//                    onError: { error in
//                        DDLogError("\(error)")
//                        observer.onError(error)
//                    }
//                ).addDisposableTo(self.disposeBag)
            
            return Disposables.create()
        }
    }
    
    func get(_ url: String, parameters: NetworkParameters? = nil, autoSendError: Bool = true) -> Observable<Any> {
        return self._request(.get, url: url, parameters: parameters, autoSendError: autoSendError)
    }
    
    func post(_ url: String, parameters: NetworkParameters? = nil, autoSendError: Bool = true) -> Observable<Any> {
        return self._request(.post, url: url, parameters: parameters, autoSendError: autoSendError)
    }
}

func saveSID(_ sid: String) {
    let udef = UserDefaults.standard
    udef.set(sid, forKey: "sid")
    udef.synchronize()
}

// MARK: - Запросы
extension NTVWebServiceManager {
    
    func autoLogin(_ sid:String) -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("Автологин...")
            
            self.post(self.kUrlWebService + "auto-login/", parameters: [
                "sid":sid as AnyObject])
                .subscribe(
                    onNext: { result in
                        observer.onNext(result as! NTVNetworkResult)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }
    }

    
    
    /**
     Запрос кода
     
     - parameter phoneEmail: телефонный номер или email
     
     - returns: Observable
     */
    func requestCode(_ phoneEmail:String) -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("Запрос кода...")
            
            self.login = phoneEmail
            
            self.post(self.kUrlWebService + "code/", parameters: [
                "login":phoneEmail as AnyObject])
                .subscribe(
                    onNext: { result in
                        observer.onNext(result as! NTVNetworkResult)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }        
    }
    
    /**
     Запись анкеты
     
     - parameter anketa: анкета
     
     - returns: Observable
     */
    func writeAnketa(_ anketa: NTVAnketa) -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("Запись анкеты...")
            print("\(anketa.toParameters())")
            
            self.post(self.kUrlWebService + "register/", parameters: anketa.toParameters())
                .subscribe(
                    onNext: { result in
                        observer.onNext(result as! NTVNetworkResult)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            return Disposables.create()
        }
    }
    
    /**
     Чтение анкеты
     
     - returns: Observable `NTVAnketaNetworkResult`
     - seealso: NTVAnketaNetworkResult
     */
    func readAnketa() -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("Чтение анкеты...")
            
            self.post(self.kUrlWebService + "user-info/")
                .subscribe(
                    onNext: { response in
                        let result = Mapper<NTVAnketaNetworkResult>().map(JSON: (response as! NTVNetworkResult).json as! [String : Any])
                        observer.onNext(result!.anketa)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }
    }
    
    func authLogin(_ sid: String) -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("Авто логин...")
            
            self.post(self.kUrlWebService + "auto-login/", parameters: ["sid": sid as AnyObject])
                .subscribe(
                    onNext: { reponse in
                        observer.onNext(reponse as! NTVAutoLoginNetworkResult)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }
    }
    
    func login(_ code: String, push_id: String = "", os: String = "") -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("Логин...")
            
            self.post(self.kUrlWebService + "login/",
                    parameters: ["login": self.login as AnyObject,
                                 "code": code as AnyObject,
                                 "push_id": push_id as AnyObject,
                                 "os": os as AnyObject])
                .subscribe(
                    onNext: { response in
                        let result = Mapper<NTVAutoLoginNetworkResult>().map(JSON: (response as! NTVNetworkResult).json as! [String : Any])
                        saveSID((response as! NTVNetworkResult).sid!)
                        observer.onNext(result)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }
    }

    func canUploadMovie(_ name: String = "", description: String = "", latitude: Float = 0, longitude: Float = 0) -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("можно ли аплоадить...")
            
            self.post(self.kUrlWebService + "can-upload-movie/", parameters: ["name": name as AnyObject, "description": description as AnyObject])
                .subscribe(
                    onNext: { reponse in
                        observer.onNext(reponse)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }
    }

    func uploadMovie(_ movie: NTVMovieModel) -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("аплоадим...")
            
            if movie.fullFileNameStorage == nil {
                let error = NSError(domain: "NTVWebServiceManager", error: .error_upload)
                observer.onError(error)
                return Disposables.create()
            }
            
            let fullPath = NTVLocalFilesManager.sharedManager.fullPathByFile(movie.fullFileNameStorage!)!
            let filePath = URL(fileURLWithPath: fullPath)
            
            let file: NSDictionary = ["filename": movie.filename!,
                        "fullname": fullPath,
                        "contentType": "video/mp4",
                        "key": "movie"
            ]
            
            let id = String(movie.id)
            
            var postValues = [String: AnyObject]()
            postValues["id"] = id as AnyObject?
            postValues["movie"] = file

            print(postValues)
            
            
            let url = self.kUrlWebService + "upload-movie/?id="+id
            
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    multipartFormData.append(filePath,
                                             withName: "movie",
                                             fileName: movie.filename!,
                                             mimeType: "video/mp4")
                },
                to: url, method:.post, encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        print("success")
                        //upload.uploadProgress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                        upload.uploadProgress { progress in
                            print(progress.fractionCompleted)
                        }
                        upload.response(completionHandler: { response in
                            do {
                                let jsonDict = try JSONSerialization.jsonObject(with: response.data!, options: []) as! [String: AnyObject]
                                print(jsonDict)
                                if let error_code = jsonDict["error_code"] as? Int, let error_message = jsonDict["error_message"] as? String, error_code != 0 {
                                    let error = NSError(domain: String(describing: NTVWebServiceManager.self), errorCode: error_code, errorMessage: error_message)
                                    observer.onError(error)
                                } else {
                                    observer.onNext(jsonDict)
                                    observer.onCompleted()
                                }
                            } catch {
                                let error = NSError(domain: String(describing: NTVWebServiceManager.self), error: .error_upload)
                                observer.onError(error)
                            }
                        })
//                        upload.response { (request, response, data, error) in
//                            print(error)
//                            print(response)
//                            do {
//                                let jsonDict = try JSONSerialization.jsonObject(with: data!, options: [])
//                                print(jsonDict)
//                                if let error_code = jsonDict["error_code"] as? Int, let error_message = jsonDict["error_message"] as? String {
//                                    let error = NSError(domain: String(NTVWebServiceManager), errorCode: error_code, errorMessage: error_message)
//                                    observer.onError(error)
//                                } else {
//                                    observer.onNext(jsonDict)
//                                    observer.onCompleted()
//                                }
//                            } catch {
//                                let error = NSError(domain: String(NTVWebServiceManager), error: .error_upload)
//                                observer.onError(error)
//                            }
//                        }
                    case .failure(let encodingError):
                        print("fail")
                        print(encodingError)
                        let error = NSError(domain: String(describing: NTVWebServiceManager.self), error: .error_upload)
                        observer.onError(error)
                    }
                }
            )
            
//                Alamofire.upload(request, file: filePath)
//                    .progress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
//                        print(bytesWritten)
//                    }
//                    .response { (request, response, data, error) in
//                        print(error)
//                        print(response)
//                        do {
//                            let jsonDict = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
//                            print(jsonDict)
//                        } catch {
//                            
//                        }
                
            return Disposables.create()
        }
    }
    
    func getFiles(_ sid: String) -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("список загруженных файлов...")
            
            self.post(self.kUrlWebService + "movies/", parameters: ["sid": sid as AnyObject])
                .subscribe(
                    onNext: { response in
                        let result = Mapper<NTVStatusNetworkResult>().map(JSON: (response as! NTVNetworkResult).json as! [String : Any])
                        observer.onNext(result!.statuses)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }
    }
    
}

// MARK: - Social

extension NTVWebServiceManager {
    
    func loginViaFacebook(_ token: String) -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("facebook")
            
            self.post(self.kUrlWebService + "fb-login/", parameters: ["accessToken": token as AnyObject])
                .subscribe(
                    onNext: { reponse in
                        let result = reponse as! NTVNetworkResult
                        saveSID(result.sid!)
                        observer.onNext(result.sid)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }
    }
    
    func validateLoginVK(_ code: String) -> Observable<Any> {
        return Observable.create { [unowned self] observer in
            print("vkontakte")
            
            let vkAppID = Bundle.main.object(forInfoDictionaryKey: "VKAppKey")
            let vkClientSecret = Bundle.main.object(forInfoDictionaryKey: "VKSecretKey")
            let vkRedirectUri = Bundle.main.object(forInfoDictionaryKey: "VKRedirectUri")
            
            self.post("https://oauth.vk.com/access_token", parameters: ["client_id": vkAppID! as AnyObject,
                                                                        "client_secret": vkClientSecret! as AnyObject,
                                                                        "redirect_uri": vkRedirectUri! as AnyObject,
                                                                        "code": code as AnyObject])
                .subscribe(
                    onNext: { reponse in
                        
                        if reponse is String {
                            observer.onNext(reponse as! String)
                            observer.onCompleted()
                        } else {
                            let error = NSError(domain: String(describing: NTVWebServiceManager.self), error: .error_vk_validate)
                            observer.onError(error)
                        }
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }
    }
    
    func loginViaVkontakte(_ token: String) -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("vkontakte")
            
            self.post(self.kUrlWebService + "vk-login/", parameters: ["token": token as AnyObject])
                .subscribe(
                    onNext: { reponse in
                        let result = reponse as! NTVNetworkResult
                        saveSID(result.sid!)
                        observer.onNext(result.sid)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }
    }

    func loginViaOdnoklassniki(_ token: String) -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("ok")
            
            self.post(self.kUrlWebService + "ok-login/", parameters: ["access_token": token as AnyObject])
                .subscribe(
                    onNext: { reponse in
                        let result = reponse as! NTVNetworkResult
                        saveSID(result.sid!)
                        observer.onNext(result.sid)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }
    }

    
    func loginViaTwitter(_ token: String, secret: String) -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("tw")
            
            self.post(self.kUrlWebService + "twitter-login/", parameters: ["access_token": token as AnyObject,
                                                                           "secret": secret as AnyObject])
            .subscribe(
                    onNext: { reponse in
                        let result = reponse as! NTVNetworkResult
                        saveSID(result.sid!)
                        observer.onNext(result.sid)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }
    }
    
    func loginViaGPP(_ token: String) -> Observable<Any> {
        
        return Observable.create { [unowned self] observer in
            print("gpp: \(token)")
            
            self.post(self.kUrlWebService + "gplus-login/", parameters: ["access_token": token as AnyObject])
                .subscribe(
                    onNext: { reponse in
                        let result = reponse as! NTVNetworkResult
                        saveSID(result.sid!)
                        observer.onNext(result.sid)
                        observer.onCompleted()
                    }, onError: { error in
                        observer.onError(error)
                    }
                ).addDisposableTo(self.disposeBag)
            
            
            return Disposables.create()
        }
    }

}
