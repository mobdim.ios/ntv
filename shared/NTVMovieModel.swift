//
//  NTVMovieModel.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 29/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Foundation

#if os(iOS)
    import UIKit
    import AVFoundation
#endif

enum keys: String {
    case id
    case name
    case description
    case channelId
    case authUserId
    case authHash
    case filename
    case fullFileNameStorage
    case url
    case convertId
    case fileId
    case uploaded
    case fullfilename
    case date
}

class NTVMovieModel: NSObject, NSCoding, NSCopying {
    
    var id: Int32 = -1
    var name: String?
    var descript: String?
    var channelId: Int32 = -1
    var channelName: String?
    var channelSubscribe: Bool = false
    var channelSrc: String?
    var authUserId: Int32 = -1
    var authHash: String?
    var filename: String?
    var fullFileNameStorage: String?
    var url: String?
    var convertId: Int32 = -1
    var fileId: Int32 = -1
    var fullfilename: URL?
    var date: Date?
    var uploaded: Bool = false
    var isSelected: Bool = false
    var status = ""
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        
        id = aDecoder.decodeCInt(forKey: keys.id.rawValue)
        name = aDecoder.decodeObject(forKey: keys.name.rawValue) as? String
        descript = aDecoder.decodeObject(forKey: keys.description.rawValue) as? String
        channelId = aDecoder.decodeCInt(forKey: keys.channelId.rawValue)
        authUserId = aDecoder.decodeCInt(forKey: keys.authUserId.rawValue)
        authHash = aDecoder.decodeObject(forKey: keys.authHash.rawValue) as? String
        filename = aDecoder.decodeObject(forKey: keys.filename.rawValue) as? String
        fullFileNameStorage = aDecoder.decodeObject(forKey: keys.fullFileNameStorage.rawValue) as? String
        url = aDecoder.decodeObject(forKey: keys.url.rawValue) as? String
        convertId = aDecoder.decodeCInt(forKey: keys.convertId.rawValue)
        fileId = aDecoder.decodeCInt(forKey: keys.fileId.rawValue)
        if aDecoder.containsValue(forKey: keys.uploaded.rawValue) {
            uploaded = aDecoder.decodeBool(forKey: keys.uploaded.rawValue)
        }
        fullfilename = aDecoder.decodeObject(forKey: keys.fullfilename.rawValue) as? URL
        date = aDecoder.decodeObject(forKey: keys.date.rawValue) as? Date
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encodeCInt(id, forKey: keys.id.rawValue)
        aCoder.encode(name, forKey: keys.name.rawValue)
        aCoder.encode(descript, forKey: keys.description.rawValue)
        aCoder.encodeCInt(channelId, forKey: keys.channelId.rawValue)
        aCoder.encodeCInt(authUserId, forKey: keys.authUserId.rawValue)
        aCoder.encode(authHash, forKey: keys.authHash.rawValue)
        aCoder.encode(filename, forKey: keys.filename.rawValue)
        aCoder.encode(fullFileNameStorage, forKey: keys.fullFileNameStorage.rawValue)
        aCoder.encode(url, forKey: keys.url.rawValue)
        aCoder.encodeCInt(convertId, forKey: keys.convertId.rawValue)
        aCoder.encodeCInt(fileId, forKey: keys.fileId.rawValue)
        aCoder.encode(uploaded, forKey: keys.uploaded.rawValue)
        aCoder.encode(fullfilename, forKey: keys.fullfilename.rawValue)
        aCoder.encode(date, forKey: keys.date.rawValue)
    }
    
    func copy(with zone: NSZone?) -> Any {
        let copy = NTVMovieModel()
        copy.id = self.id
        copy.name = self.name
        copy.descript = self.descript
        copy.channelId = self.channelId
        copy.authUserId = self.authUserId
        copy.authHash = self.authHash
        copy.filename = self.filename
        copy.fullFileNameStorage = self.fullFileNameStorage
        copy.url = self.url
        copy.convertId = self.convertId
        copy.fileId = self.fileId
        copy.uploaded = self.uploaded
        copy.fullfilename = self.fullfilename
        copy.date = self.date
        
        return copy
    }
    
#if os(iOS)
    func thumbnailImageWithCompletion(_ completion:@escaping (_ image: UIImage?, _ duration: TimeInterval) -> Void) {
        guard let filename = fullFileNameStorage else {
            print("thumbnailImageWithCompletion: filename is nil!!!")
            return
        }
        
        let fullPath = NTVLocalFilesManager.sharedManager.fullPathByFile(filename)
        let fileURL = URL(fileURLWithPath: fullPath!)
        let asset = AVURLAsset(url: fileURL, options: nil)
        let videoDuration = asset.duration
        let videoDurationSeconds = CMTimeGetSeconds(videoDuration)
        
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        let thumbTime = CMTimeMakeWithSeconds(0,30)
        
        
        //(CMTime, CGImage?, CMTime, AVAssetImageGeneratorResult, NSError?)
        //(CMTime, CGImage?, CMTime, AVAssetImageGeneratorResult, Error?)
        let handler: AVAssetImageGeneratorCompletionHandler = { (requestedTime: CMTime, im: CGImage?, actualTime: CMTime, result :AVAssetImageGeneratorResult, error: Error?) in
            
            if let cgimage = im {
                if result == AVAssetImageGeneratorResult.succeeded {
                    let thumbnail = UIImage(cgImage: cgimage)
                    DispatchQueue.main.async(execute: {
                        completion(thumbnail, videoDurationSeconds)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        print("thumbnailImageWithCompletion != Succeeded")
                        completion(nil, -1)
                    })
                }
            } else {
                DispatchQueue.main.async(execute: {
                    completion(nil, -1)
                })
            }
        }
        
        let maxSize = CGSize.zero;//CGSize(width: w, height: h)
        generator.maximumSize = maxSize
        let tVal = NSValue(time: thumbTime)
        generator.generateCGImagesAsynchronously(forTimes: [tVal], completionHandler: handler)
    }
#endif
}
