//
//  NTVNetworkResult.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 28/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Foundation
import ObjectMapper

class NTVNetworkResult: Mappable {
    
    var id: NSNumber?
    var error_message: String = ""
    var error_description: String? = nil
    var error_code: NSNumber?
    var sid: String?
    var response: HTTPURLResponse?
    var json: NSDictionary?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        error_message <- map["error_message"]
        error_code <- map["error_code"]
        error_description <- map["error_description"]
        sid <- map["sid"]
    }
    
    func isNoError() -> Bool {
        return error_code == 0
    }
    
}
