//
//  NTVLocalFilesManager.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 28/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Foundation

class NTVLocalFilesManager: NSObject {
    static let sharedManager = NTVLocalFilesManager()
    
    let keyLocalMovies = "keyLocalMovies"
    let kLocalFolderName = "videofiles"
    
    var movies = [NTVMovieModel]()
    
    #if os(OSX)
    var moviesPath: URL? = nil
    fileprivate let componentPathMovies = "NTV/Movies"
    fileprivate let componentPathMoviesFoRemove = "NTV"
    var moviesPathForRemove: URL? = nil
    #endif
    
    override init() {
        super.init()
    #if os(OSX)
        do {
            self.moviesPath = try FileManager.default.url(for: .moviesDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent(componentPathMovies)
            
            if let mp = self.moviesPath {
                try FileManager.default.createDirectory(at: mp, withIntermediateDirectories: true, attributes: nil)
            }
            
            self.moviesPathForRemove = try FileManager.default.url(for: .moviesDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent(componentPathMoviesFoRemove)
        } catch {
            print("file filemanager!!!")
        }
    #elseif os(iOS)
        
    #endif
        self.refreshLocal()
    }
    
    #if os(OSX)
    func newMovieFile() -> URL? {
        if let mp = self.moviesPath {
            let url = mp.appendingPathComponent(Date().toString("YYYY.MM.dd.HH:mm:ss") + ".mov")
            print("Создание нового файла \(url)")
            return url
        }
        
        return nil
    }
    #endif
    
    func refreshLocal() {
        
        movies.removeAll()
        print("Обновление локальных файлов.")
        
        let local = UserDefaults.standard.array(forKey: keyLocalMovies) as? [Data]
        
        guard let _ = local else {
            return
        }
        
        for data: Data in local! {
            let movie: NTVMovieModel = NSKeyedUnarchiver.unarchiveObject(with: data) as! NTVMovieModel
            movies.append(movie)
            print("\(movie)")
        }
    }
    
    func addLocalMovie(_ movie: NTVMovieModel) {
        var local = UserDefaults.standard.array(forKey: keyLocalMovies)
        
        if local == nil {
            local = [Data]()
        }
        
        let encodedObject = NSKeyedArchiver.archivedData(withRootObject: movie)
        local!.append(encodedObject)
        UserDefaults.standard.set(local, forKey: keyLocalMovies)
        
        let result = UserDefaults.standard.synchronize()
        
        print("addLocalMovie \(result)")
        print("add: \(movie.fullFileNameStorage)")
    }
    
    func updateLocalMove(_ movie: NTVMovieModel, atIndex index:Int) {
        let local = UserDefaults.standard.array(forKey: keyLocalMovies)
        
        guard let _ = local else {
            return
        }
        
        if local!.count == 0 {
            print("local count is 0!")
            return
        }
        
        let encodedObject = NSKeyedArchiver.archivedData(withRootObject: movie)

        let mulocal = NSMutableArray(array:local!)
        
        mulocal.replaceObject(at: index, with: encodedObject)
    
        UserDefaults.standard.set(mulocal, forKey: keyLocalMovies)
        
        let result = UserDefaults.standard.synchronize()
    
        print("updateLocalMovie : \(result)")
        print("update: \(movie.fullFileNameStorage)")
    }
    
    func removeLocalMovieAtIndex(_ index: Int) {
        
        var local = UserDefaults.standard.array(forKey: keyLocalMovies)
        
        if local == nil {
            local = [Data]()
        }
        
        if index > local!.count {
            print("index < mulocal.count")
            return
        }
        
        let data = local![index] as! Data
        
        let movie = NSKeyedUnarchiver.unarchiveObject(with: data) as! NTVMovieModel
        
        guard let fullFileNameStorage = movie.fullFileNameStorage else {
            print("fullFileNameStorage = nil!")
            return
        }
        
        guard let fullPath = fullPathByFile(fullFileNameStorage) else {
            print("fullPath = nil!")
            return
        }
        
        do {
            try FileManager.default.removeItem(atPath: fullPath)
        } catch {
//            let alert = UIAlertController(title: nil, message: "Remove file error!", preferredStyle: .Alert)
//            let okAction = UIAlertAction(title: NSLocalizedString("ok", comment:""), style: .Cancel, handler: nil
//            )
//            alert.addAction(okAction)
            
            print("Remove file error!")
            
            //return
        }
        
        local!.remove(at: index)
        UserDefaults.standard.set(local, forKey: keyLocalMovies)
        UserDefaults.standard.synchronize()
    }
    
    func removeAllFiles() {
        #if os(OSX)
        
//        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
//        let documentsDirectory = paths.first!
//        let dataPath = (documentsDirectory as NSString).stringByAppendingPathComponent(kLocalFolderName)
        do {
            try FileManager.default.removeItem(at: self.moviesPathForRemove!)
        } catch {
            print("Ошибка удаления файлов")
            return
        }
        UserDefaults.standard.removeObject(forKey: keyLocalMovies)
        UserDefaults.standard.synchronize()
        exit(0)
        #endif
    }
        
    func fullPathByFile(_ filename: String) -> String? {
        var fn: String
        if (filename as NSString).range(of: kLocalFolderName).location == NSNotFound {
            fn = filename
            #if os(OSX)
            return fn
            #endif
        } else {
            fn = (filename as NSString).lastPathComponent
        }

        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths.first!
    
        let dataPath = (documentsDirectory as NSString).appendingPathComponent(kLocalFolderName)

        
        var fullPath: String? = nil
        do {
            if !FileManager.default.fileExists(atPath: dataPath) {
                try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
            }
        } catch {
            return fullPath
        }
        
        fullPath = "\(dataPath)/\(fn)"
        
    
        return fullPath;
    }
}
