//
//  NTVVKCodeNetworkResult.swift
//  ntv
//
//  Created by Dmitriy Zadoroghnyy on 16/07/16.
//  Copyright © 2016 Dmitriy Zadoroghnyy. All rights reserved.
//

import Foundation
import ObjectMapper


class NTVVKCodeNetworkResult: Mappable {
    
    var access_token: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        access_token <- map["access_token"]
    }
}
