//
//  NTVAutoLoginNetworkResult.swift
//  ntv
//
//  Created by Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный on 28/03/16.
//  Copyright © 2016 Ведущий программист мобильных платформ Системный Дом "Деловые Партнеры" Дмитрий Задорожный. All rights reserved.
//

import Foundation
import ObjectMapper

class NTVAutoLoginNetworkResult: NTVNetworkResult {
    var result: Int = -1
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        result <- map["result"]
    }
}
